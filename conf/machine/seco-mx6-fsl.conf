#@TYPE: Machine
#@NAME: SECO North i.MX6
#@SOC: i.MX6
#@DESCRIPTION: Machine configuration for SECO North i.MX6 platforms based on FSL layers

require include/seco-mx6.inc
require conf/machine/include/arm/armv7a/tune-cortexa9.inc

SOC_FAMILY = "imx:mx6:mx6q"
MACHINEOVERRIDES =. "mx6:mx6q:mx6dl:"

SANTVEND_PRESHUTDOWN_SCRIPTS = " \
    shutdown-pwr-check \
    restore-wakeup-ok \
"

MACHINE_ESSENTIAL_EXTRA_RDEPENDS += " \
    linux-firmware-imx-sdma-imx6q \
    kernel-module-imx6-spreadspectrum \
"

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append = " \
    ${SANTVEND_PRESHUTDOWN_SCRIPTS} \
"

KERNEL_DEVICETREE ?= " \
    seconorth/imx6dl-santaro.dtb \
    seconorth/imx6dl-santino-lt.dtb \
    seconorth/imx6dl-santino.dtb \
    seconorth/imx6dl-santoka.dtb \
    seconorth/imx6q-santaro.dtb \
    seconorth/imx6q-santoka.dtb \
    seconorth/imx6qp-santoka.dtb \
    seconorth/imx6q-santvend.dtb  \
"

RAM_START_ADDRESS = "0x10000000"

# SECO North specific offset, that has historic reasons but
# doesn't make sense
KERNEL_EXTRA_ARGS += "LOADADDR=0x10010000"
