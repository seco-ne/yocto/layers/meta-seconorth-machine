# The IMX_DEFAULT_BSP can only be nxp or mainline. When building mainline, the vendor specific
# components are not included in the build (like the hardware acceleration features).

IMX_DEFAULT_BSP = "nxp"

# @TODO: ???
MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE = "1"

require conf/machine/include/imx-base.inc 

# The imx-base class adds WIC image types, but then the imx-boot build fails.
# Therefore, we reset the filesystem type to tar.gz.
SOC_DEFAULT_IMAGE_FSTYPES = "tar.gz" 

require seconorth-machine.inc

# @TODO: ???
MACHINEOVERRIDES =. "imx-boot-container:"

# @NOTE: The cortexa53-crypto tunning is part of the tune-cortexa53 include.
require conf/machine/include/arm/armv8a/tune-cortexa53.inc

# @NOTE: The arch-arm64 is included implicit via the tune-cortexa53 include.
# require conf/machine/include/arm/arch-arm64.inc

EXTRA_IMAGEDEPENDS:append = " \
    imx-boot \
"

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:remove = " \
    kernel-module-gfplatdetect \
"

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append = " \
    firmware-nxp-wifi-nxp8997-pcie \
    kernel-module-8723ds \
    kernel-module-nxp-wlan \
    linux-firmware-imx-sdma-imx7d \
    linux-firmware-imx-vpu \
    linux-firmware-pcie8997 \
    rtl8723ds \
    rtl8723ds-bt-uart-driver \
"

# Spreadspectrum is a Python script and we don't want Python in Flash-N-Go.
# @TODO: Create alternative, maybe a shell script?
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append:seconorth = " \
    spreadspectrum \
"

# Do we need to set these variables?
UBOOT_SUFFIX = "bin"
UBOOT_MAKE_TARGET = ""
SPL_BINARY = "spl/u-boot-spl.bin"
BOOT_SPACE = "65536"
UBOOT_CONFIG ??= "sd_no_console sd"

PREFERRED_PROVIDER_virtual/kernel = "linux-imx"
PREFERRED_VERSION:linux-imx = "5.15.32"

PREFERRED_PROVIDER:u-boot = "u-boot-seco-imx"
PREFERRED_PROVIDER_virtual/bootloader = "u-boot-seco-imx"
PREFERRED_VERSION:u-boot = "2020.04"

PREFERRED_VERSION:gstreamer1.0 = "1.18.5.imx"
PREFERRED_VERSION:gstreamer1.0-plugins-base = "1.18.5.imx"
PREFERRED_VERSION:gstreamer1.0-plugins-good = "1.18.5.imx"
PREFERRED_VERSION:gstreamer1.0-plugins-bad = "1.18.5.imx"

# uImage and zImage build targets are not supported on ARM64.
# If compressed kernel images are to be used, the bootloader must unpack them.
# @TODO: Is this still the case?
KERNEL_IMAGETYPE = "Image"

# Seems not to get set for i.MX8. So far, no idea how this is going to work... PIC?
LOADADDR = ""

MACHINE_INSTALL_SCRIPTS:append = " fng-install-uboot.sh "
