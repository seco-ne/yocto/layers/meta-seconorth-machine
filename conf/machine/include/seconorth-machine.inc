require conf/machine/include/soc-family.inc

PREFERRED_PROVIDER_virtual/kernel ??= "linux-seconorth"
PREFERRED_VERSION_linux-seconorth ??= "5.15.29"
KERNEL_IMAGETYPE = "uImage"

KBUILD_BUILD_USER := "support"
KBUILD_BUILD_HOST := "seco.com"

# The MACHINEOVERRIDES_EXTENDER_FILTER_OUT is the filter that removes
# the old machine overrides for the NXP SoCs (e.g. mx6 or mx8m).
# Since we use these overrides frequently and the new overrides are
# quite long (mx8m-generic-bsp, etc.) we keep them for now.
# For more info see meta-freescale/conf/machine/include/imx-base.inc.
MACHINEOVERRIDES_EXTENDER_FILTER_OUT = " "

MACHINEOVERRIDES =. "seco-arm:seco-nxp:"

MACHINE_EXTRA_RRECOMMENDS:append = " \
    gfxml2dto \
    kernel-modules \
    packagegroup-seconorth-touchdriver \
"

# "bb.utils.contains" works with exact strings matches.
# Use inline python to add package for all edgehog DISTROs.
EXTRA_IMAGEDEPENDS:append = "\
    ${@bb.utils.contains('DISTRO', 'seconorth-fngsystem', 'uuu-scripts', '', d)} \
    ${@'uuu-scripts' if 'edgehog' in d.getVar('DISTRO') else ''} \
    ${@'uuu-install-wic' if 'edgehog' in d.getVar('DISTRO') else ''} \
"

MACHINE_EXTRA_RRECOMMENDS_EXTERN ??= ""

MACHINE_EXTRA_RRECOMMENDS_EXTERN:append:seco-arm = " \
    ${@bb.utils.contains_any('DISTRO', ' \
        seconorth-fngsystem \
        seconorth-wayland \
    ', '', 'yocto-install-services', d)} \
"

# Add seco-hmi-postinstallation for Edgehog builds.
MACHINE_EXTRA_RRECOMMENDS:append = " \
    ${MACHINE_EXTRA_RRECOMMENDS_EXTERN} \
    imx-memtool \
"

MACHINE_ESSENTIAL_EXTRA_RDEPENDS:append = " \
    kernel-devicetree \
    kernel-image \
    seco-sharedconf \
    seco-sharedconf-network \
    seco-sharedconf-showversion \
    packagegroup-bootscript \
    ${MACHINE_INSTALL_SCRIPTS} \
"

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append = " \
    bt-hci-init \
    kernel-module-gfplatdetect \
    kernel-module-rtc-pcf8563-guf \
"

KERNEL_MODULE_PROBECONF += "evbug"
module_conf_evbug = "blacklist evbug"

SERIAL_CONSOLES = "115200;ttymxc0 115200;ttyGS0"
KERNEL_MODULE_AUTOLOAD += "g_serial"

MACHINE_FEATURES = " \
    alsa \
    emmc \
    ext2 \
    rtc \
    serial \
    touchscreen \
    usbgadget \
    usbhost \
    vfat \
"

MACHINE_INSTALL_SCRIPTS = "fng-install.sh"
MACHINE_INSTALL_SCRIPTS:fng = "fngsystem-self-update.sh fngsystem-self-init.sh"

IMAGE_BOOT_FILES:append = " \
    ${@' '.join([ dtb.split('/')[-1] for dtb in d.getVar('KERNEL_DEVICETREE').split() ] )} \
    boot/* \
"

# Edgehog boot script takes kernel from bootfs_X partitions.
IMAGE_BOOT_FILES:append:seco-arm = " \
    ${KERNEL_IMAGETYPE} \
"

# Take the first devicetree from KERNEL_DEVICETREE as DEFAULT_DEVICETREE
# currently used in boot.cfg for imx8m machines
DEFAULT_DEVICETREE  = "${@ d.getVar('KERNEL_DEVICETREE').split()[0].split('/')[-1].strip() }"
