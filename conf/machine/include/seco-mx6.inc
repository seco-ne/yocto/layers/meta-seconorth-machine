# The IMX_DEFAULT_BSP can only be nxp or mainline. When building mainline, the vendor specific
# components are not included in the build (like the hardware acceleration features).

IMX_DEFAULT_BSP = "nxp"

MACHINE_HAS_VIVANTE_KERNEL_DRIVER_SUPPORT:mx6ull = "0"
MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE = "1"

require conf/machine/include/imx-base.inc

# The imx-base class adds WIC image types, but then the imx-boot build fails.
# Therefore, we reset the filesystem type to tar.gz.
SOC_DEFAULT_IMAGE_FSTYPES = "tar.gz"

require seconorth-machine.inc

PREFERRED_PROVIDER_virtual/kernel = "linux-imx"
PREFERRED_VERSION:linux-imx = "5.15.32"

MACHINE_GSTREAMER_1_0_PLUGIN:mx6dl-nxp-bsp = "imx-gst1.0-plugin"
MACHINE_GSTREAMER_1_0_PLUGIN:mx6q-nxp-bsp = "imx-gst1.0-plugin"

PREFERRED_VERSION_gstreamer1.0 = "1.18.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-base = "1.18.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good = "1.18.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-bad = "1.18.5.imx"

