SUMMARY = "Driver for the Bluetooth part of the Realtek 8723DS"
HOMEPAGE = "https://github.com/Poco-Ye/rtl8723DS-BT-uart-driver"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = " \
    git://github.com/Poco-Ye/rtl8723DS-BT-uart-driver.git;protocol=https;branch=master \
    file://0001-rkt_hciattach-Fix-Makefile-for-the-Yocto-build.patch \
"

PV = "1.0+git${SRCPV}"
SRCREV = "793362d55797668cc0dcd242b095c5361838a4ae"

S = "${WORKDIR}/git"

do_configure() {
    cd rtk_hciattach
    oe_runmake clean
}

do_compile:prepend() {
    cd rtk_hciattach
}

do_install() {
    mkdir -p ${D}${bindir}
    install -m755 ${S}/rtk_hciattach/rtk_hciattach ${D}${bindir}/
    mkdir -p ${D}/lib/firmware/rtlbt/
    install -m755 ${S}/8723D/rtl8723d_config ${D}/lib/firmware/rtlbt/
    install -m755 ${S}/8723D/rtl8723d_fw ${D}/lib/firmware/rtlbt/
}

FILES:${PN} += " \
    /lib/firmware/rtlbt/rtl8723d_config \
    /lib/firmware/rtlbt/rtl8723d_fw \
"
