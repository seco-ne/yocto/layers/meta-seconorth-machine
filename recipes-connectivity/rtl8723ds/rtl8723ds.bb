SUMMARY = "Driver for Realtek 8723DS wireless devices"
HOMEPAGE = "https://www.realtek.com"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

inherit module
do_compile[lockfiles] = "${TMPDIR}/kernel-scripts.lock"

SRC_URI = " \
    git://github.com/lwfinger/rtl8723ds.git;protocol=https;branch=master \
    file://0001-Disable-DEBUG-and-__DATE__-__TIME__-usage.patch \
    file://0001-Enable-adaptivity-for-regulation-qualification.patch \
"

SRCREV = "76fb80685be9920a1d5ac7003102dcdfb76daa6b"

S = "${WORKDIR}/git"

EXTRA_OEMAKE = "KSRC=${STAGING_KERNEL_DIR} KVER=${KERNEL_VERSION}"

do_install() {
    install -d ${D}/lib/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless
    install -m 0644 ${S}/8723ds.ko ${D}${base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/net/wireless
}

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
RRECOMMENDS:${PN} = "kernel-module-${PN}"

do_configure[depends] += "virtual/kernel:do_compile_kernelmodules"
