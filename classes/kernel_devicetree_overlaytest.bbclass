# Support to apply a given set of overlays directly after the build
# to detect errors before actually using the overlays on the device
# 
# The overlays to test are described in the 'OVERLAY_COMBINATIONS' variable
# containing several lines in the format
# dtb:dtbo
# where dtb and the dtbo may contain '*' or other patterns (interpreted by 'find').
# each overlay found for dtbo is applied to each dtb found with the dtb pattern.
# Results are not stored

DEPENDS += "dtc-native"

OVERLAY_COMBINATIONS ?= ""

do_test_overlays() {
    ret=0
    dtb_base_path="${B}/arch/${ARCH}/boot/dts/"
    for c in ${OVERLAY_COMBINATIONS};
    do
        dtb_pattern="${c%:*}"
        dtbo_pattern="${c#*:}"

        # Resolve to full path, let 'find' expand wildcards
        dtbs="$(find "$dtb_base_path" -path *${dtb_pattern} )"
        if [ -z "${dtbs}" ];then
            echo "Failed to find devicetrees for pattern: ${dtb_pattern}"
            ret=1
        fi

        dtbos="$(find "$dtb_base_path" -path *${dtbo_pattern} )"
        if [ -z "${dtbos}" ];then
            echo "Failed to find overlays for pattern: ${dtbo_pattern}"
            ret=1
        fi

        for dtb in ${dtbs}
        do
            for dtbo in ${dtbos}
            do
                if ! fdtoverlay -v -i "$dtb" "$dtbo" -o /dev/null
                then
                    echo "Failed to apply overlay $dtbo to devicetree $dtb"
                    ret=1
                fi

            done
        done
    done
    return $ret
}

addtask test_overlays after do_compile before do_populate_sysroot
