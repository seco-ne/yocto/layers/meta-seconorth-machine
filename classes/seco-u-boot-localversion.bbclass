# Seco U-Boot LOCALVERSION extension
# adapted from meta-freescale/classes/fsl-u-boot-localversion.bbclass
#
# This allow to easy reuse of code between different U-Boot recipes
#
# The following options are supported:
#
#  SCMVERSION        Puts the Git hash in U-Boot local version
#  LOCALVERSION      Value used in LOCALVERSION (default to '+fslc')
#
# Copyright 2014 (C) O.S. Systems Software LTDA.

SCMVERSION ??= "y"
LOCALVERSION ??= "+fslc"
SCMVERSION_BEFORE_LOCALVERSION ??= "n"

UBOOT_LOCALVERSION = "${LOCALVERSION}"

do_compile:prepend() {
    if [ "${SCMVERSION}" = "y" ]; then
        # Add GIT revision to the local version
        head=`cd ${S} ; git rev-parse --verify --short HEAD 2> /dev/null`
        if [ "${SCMVERSION_BEFORE_LOCALVERSION}" = "y" ]; then
            printf "%s%s%s" +g $head "${UBOOT_LOCALVERSION}" > ${S}/.scmversion
            printf "%s%s%s" +g $head "${UBOOT_LOCALVERSION}" > ${B}/.scmversion
        else
            printf "%s%s%s" "${UBOOT_LOCALVERSION}" +g $head > ${S}/.scmversion
            printf "%s%s%s" "${UBOOT_LOCALVERSION}" +g $head > ${B}/.scmversion
        fi
    else
        printf "%s" "${UBOOT_LOCALVERSION}" > ${S}/.scmversion
        printf "%s" "${UBOOT_LOCALVERSION}" > ${B}/.scmversion
    fi
}
