FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://0001-iMX8M-Add-additional-devicetrees-in-dtbs-to-the-u-bo.patch"

do_compile:prepend() {
    for dtb in ${UBOOT_ADDITIONAL_DTB_NAMES};do
        cp -v ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/${dtb} ${BOOT_STAGING}
    done
}

do_compile:append() {
    for config in ${UBOOT_CONFIG}; do
        # Integrate fw_env.config and u-boot-initial-env
        cp "${DEPLOY_DIR_IMAGE}/u-boot-initial-env-${MACHINE}-${config}.bin" "${S}"
        cp "${DEPLOY_DIR_IMAGE}/fw_env-${MACHINE}-${config}.config" "${S}"

        cd ${S}

        for target in ${IMXBOOT_TARGETS}; do
            # Store an md5sum to check the files on target before installation
            md5sum ${BOOT_NAME}-${MACHINE}-${config}.bin-${target} \
                   u-boot-initial-env-${MACHINE}-${config}.bin \
                   fw_env-${MACHINE}-${config}.config \
                   > ${BOOT_NAME}-${MACHINE}-${config}.bin-${target}.md5
        done
        cd -
    done
}

do_deploy:append() {
    for config in ${UBOOT_CONFIG}; do
        for target in ${IMXBOOT_TARGETS}; do
            # Create a tar from the bootfiles, compressed gz, dereference links (copy the file not the link into the tar)
            cd ${S}
            tar chzf ${DEPLOYDIR}/${BOOT_NAME}-${MACHINE}-${config}.bin-${target}.tar.gz \
                     ${BOOT_NAME}-${MACHINE}-${config}.bin-${target} \
                     ${BOOT_NAME}-${MACHINE}-${config}.bin-${target}.md5 \
                     u-boot-initial-env-${MACHINE}-${config}.bin \
                     fw_env-${MACHINE}-${config}.config

            cd -
            cd ${DEPLOYDIR}
            # The creation of the link name is a little hacky, as it assumes the config to begin with sd, but it works
            ln -sf ${BOOT_NAME}-${MACHINE}-${config}.bin-${target}.tar.gz ${BOOT_NAME}${config#sd}.tar.gz
            cd -
        done
    done
}

# Copy of the original compile function, as UBOOT_ADDITIONAL_DTB_NAMES had to
# be added to the make call
do_compile:mx8m() {
    bbnote 8MQ/8MM/8MN/8MP boot binary build
    for ddr_firmware in ${DDR_FIRMWARE_NAME}; do
        bbnote "Copy ddr_firmware: ${ddr_firmware} from ${DEPLOY_DIR_IMAGE} -> ${BOOT_STAGING} "
        cp ${DEPLOY_DIR_IMAGE}/${ddr_firmware}               ${BOOT_STAGING}
    done
    cp ${DEPLOY_DIR_IMAGE}/signed_dp_imx8m.bin               ${BOOT_STAGING}
    cp ${DEPLOY_DIR_IMAGE}/signed_hdmi_imx8m.bin             ${BOOT_STAGING}
    cp ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/${UBOOT_DTB_NAME}   ${BOOT_STAGING}
    if [ "x${UBOOT_SIGN_ENABLE}" = "x1" ] ; then
        # Use DTB binary patched with signature node
        cp ${DEPLOY_DIR_IMAGE}/${UBOOT_DTB_BINARY} ${BOOT_STAGING}/${UBOOT_DTB_NAME}
    fi
    cp ${DEPLOY_DIR_IMAGE}/${ATF_MACHINE_NAME}               ${BOOT_STAGING}/bl31.bin

    # mkimage for i.MX8
    # Copy TEE binary to SoC target folder to mkimage
    if ${DEPLOY_OPTEE}; then
        cp ${DEPLOY_DIR_IMAGE}/tee.bin ${BOOT_STAGING}
    fi

    for config in ${UBOOT_CONFIG}; do
        cp ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${config} \
                                                             ${BOOT_STAGING}/u-boot-spl.bin
        cp ${DEPLOY_DIR_IMAGE}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${config} \
                                                             ${BOOT_STAGING}/u-boot-nodtb.bin
        cp ${DEPLOY_DIR_IMAGE}/u-boot-${MACHINE}.bin-${config} ${BOOT_STAGING}/u-boot.bin
        for target in ${IMXBOOT_TARGETS}; do
            if [ "$target" = "flash_linux_m4_no_v2x" ]; then
               # Special target build for i.MX 8DXL with V2X off
               bbnote "building ${IMX_BOOT_SOC_TARGET} - ${REV_OPTION} V2X=NO ${target}"
               make SOC=${IMX_BOOT_SOC_TARGET} ${REV_OPTION} V2X=NO dtbs=${UBOOT_DTB_NAME} flash_linux_m4
            else
               bbnote "building ${IMX_BOOT_SOC_TARGET} - ${REV_OPTION} ${target}"
               make SOC=${IMX_BOOT_SOC_TARGET} ${REV_OPTION} dtbs="${UBOOT_DTB_NAME} ${UBOOT_ADDITIONAL_DTB_NAMES}" ${target}
            fi
            if [ -e "${BOOT_STAGING}/flash.bin" ]; then
                cp ${BOOT_STAGING}/flash.bin ${S}/${BOOT_NAME}-${MACHINE}-${config}.bin-${target}
            fi
        done
    done
}

do_compile() {
    bberror "SOC specific do_compile override function is not found. Please create one."
}

do_install () {
    install -d ${D}/boot
    for config in ${UBOOT_CONFIG}; do
        for target in ${IMXBOOT_TARGETS}; do
            install -m 0644 ${S}/${BOOT_NAME}-${MACHINE}-${config}.bin-${target} ${D}/boot/
        done
    done
}

deploy_mx8m() {
    install -d ${DEPLOYDIR}/${BOOT_TOOLS}
    for config in ${UBOOT_CONFIG}; do
        install -m 0644 ${DEPLOY_DIR_IMAGE}/u-boot-spl.bin-${MACHINE}-${config} \
                                                             ${DEPLOYDIR}/${BOOT_TOOLS}
    done
    for ddr_firmware in ${DDR_FIRMWARE_NAME}; do
        install -m 0644 ${DEPLOY_DIR_IMAGE}/${ddr_firmware}  ${DEPLOYDIR}/${BOOT_TOOLS}
    done
    install -m 0644 ${BOOT_STAGING}/signed_dp_imx8m.bin      ${DEPLOYDIR}/${BOOT_TOOLS}
    install -m 0644 ${BOOT_STAGING}/signed_hdmi_imx8m.bin    ${DEPLOYDIR}/${BOOT_TOOLS}
    install -m 0755 ${BOOT_STAGING}/${TOOLS_NAME}            ${DEPLOYDIR}/${BOOT_TOOLS}
    install -m 0755 ${BOOT_STAGING}/mkimage_fit_atf.sh       ${DEPLOYDIR}/${BOOT_TOOLS}

}

do_deploy() {
    deploy_${SOC_FAMILY}
    # copy the sc fw, dcd and uboot to deploy path
    for config in ${UBOOT_CONFIG}; do
        install -m 0644 ${DEPLOY_DIR_IMAGE}/u-boot-${MACHINE}.bin-${config}        ${DEPLOYDIR}/${BOOT_TOOLS}
    done
    # copy tee.bin to deploy path
    if ${DEPLOY_OPTEE}; then
       install -m 0644 ${DEPLOY_DIR_IMAGE}/tee.bin ${DEPLOYDIR}/${BOOT_TOOLS}
    fi

    # copy makefile (soc.mak) for reference
    install -m 0644 ${BOOT_STAGING}/soc.mak                  ${DEPLOYDIR}/${BOOT_TOOLS}
    # copy the generated boot image to deploy path
    for target in ${IMXBOOT_TARGETS}; do
        # Use first "target" as IMAGE_IMXBOOT_TARGET
        if [ "$IMAGE_IMXBOOT_TARGET" = "" ]; then
            IMAGE_IMXBOOT_TARGET="$target"
            echo "Set boot target as $IMAGE_IMXBOOT_TARGET"
        fi
        for config in ${UBOOT_CONFIG}; do
            install -m 0644 ${S}/${BOOT_NAME}-${MACHINE}-${config}.bin-${target} ${DEPLOYDIR}
        done
    done

    ln -sf ${BOOT_NAME}-${MACHINE}-${config}.bin-${IMAGE_IMXBOOT_TARGET}    ${DEPLOYDIR}/${BOOT_NAME}
}
