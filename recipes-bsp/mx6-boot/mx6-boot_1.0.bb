SUMMARY = "SECO Northern Europe Flash-N-Go Boot with U-Boot"
DESCRIPTION = "This package provides a combined boot binary for SECO Northern \
Europe various iMX6 boards."
HOMEPAGE = "https://www.seco.com"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

COMPATIBLE_MACHINE = "(seco-mx6|seco-mx6-fsl|seco-mx6ull)"

SRC_URI = ""

require recipes-bsp/fng-boot/fngboot-common.inc
require mx6-boot.inc

do_compile[depends] = "u-boot-seco-imx:do_deploy fngboot:do_deploy"

S = "${WORKDIR}"

MX6_BOOT ?= "${@(d.getVar('MX6_BOOT_NAME')).split(".")[0]}"
FILES:${PN} = " \
	${datadir}/${MX6_BOOT_ALT_NAME}* \
	${datadir}/${MX6_BOOT}*.tar.gz \
"

do_compile() {
	tar -xpf ${DEPLOY_DIR_IMAGE}/${FNGBOOT_ARCHIVE}
	md5sum -c ${FNGBOOT_BIN_SHORT}.md5
	objcopy -I binary -O binary --pad-to ${FNGBOOT_PAD_SIZE} ${FNGBOOT_BIN_SHORT} ${FNGBOOT_BIN_SHORT}.padded
	cat ${FNGBOOT_BIN_SHORT}.padded ${DEPLOY_DIR_IMAGE}/u-boot.bin > ${MX6_BOOT_NAME}
	md5sum ${MX6_BOOT_NAME} > ${MX6_BOOT_NAME}.md5
	tar -czf ${MX6_BOOT_ARCHIVE} ${MX6_BOOT_NAME} ${MX6_BOOT_NAME}.md5

	if [ -n "${UBOOT_CONFIG}" ]; then
		for config in ${UBOOT_CONFIG}; do
			cat ${FNGBOOT_BIN_SHORT}.padded ${DEPLOY_DIR_IMAGE}/u-boot.bin-${config} > ${MX6_BOOT_NAME}-${config}
			md5sum ${MX6_BOOT_NAME}-${config} > ${MX6_BOOT_NAME}-${config}.md5
			tar -czf ${MX6_BOOT}-${config}.tar.gz ${MX6_BOOT_NAME}-${config} ${MX6_BOOT_NAME}-${config}.md5
		done
	fi
}

do_install() {
	install -d ${D}${datadir}
	install -m 0644 ${S}/${MX6_BOOT_ARCHIVE} ${D}${datadir}/
	install -m 0644 ${S}/${MX6_BOOT_NAME} ${D}${datadir}/${MX6_BOOT_ALT_NAME}

	if [ -n "${UBOOT_CONFIG}" ]; then
		for config in ${UBOOT_CONFIG}; do
			install -m 0644 ${S}/${MX6_BOOT}-${config}.tar.gz ${D}${datadir}/
			install -m 0644 ${S}/${MX6_BOOT_NAME}-${config} ${D}${datadir}/${MX6_BOOT_ALT_NAME}-${config}
		done
	fi
}

inherit deploy

addtask deploy after do_compile

do_deploy() {
	install -m 0644 ${MX6_BOOT_ARCHIVE} ${DEPLOYDIR}/
	install -m 0644 ${MX6_BOOT_NAME} ${DEPLOYDIR}/${MX6_BOOT_ALT_NAME}

	if [ -n "${UBOOT_CONFIG}" ]; then
		for config in ${UBOOT_CONFIG}; do
			install -m 0644 ${MX6_BOOT}-${config}.tar.gz ${DEPLOYDIR}/
			install -m 0644 ${S}/${MX6_BOOT_NAME}-${config} ${DEPLOYDIR}/${MX6_BOOT_ALT_NAME}-${config}
		done
	fi
}
