MX6_BOOT_NAME = "mx6-boot.bin"
MX6_BOOT_ARCHIVE = "mx6-boot.tar.gz"

MX6_BOOT_ALT_NAME ??= "u-boot.imx"

# U-Boot offset on real devices (= 128KB / 0x20000)
FNGBOOT_UBOOT_OFFSET = "131072"

# Flash-N-Go Boot offset; U-Boot will be placed at aboves offset:
# 0x20000 (128KB) - 0x400 (512*2; see fngboot-common.inc) = 0xffc00
FNGBOOT_PAD_SIZE = "0x1fc00"
