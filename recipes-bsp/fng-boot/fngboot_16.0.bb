require fngboot.inc

BOARD_NAME="Santaro"
FNGBOOT_VERSION="v16.0r3966"
FNGBOOT_DATE="12-04-2024"
FNGBOOT_BIN="GF_iMX6x-FNGBoot-${FNGBOOT_VERSION}_${FNGBOOT_DATE}.bin"

# Take FnG-Boot from public download server
SRC_URI = "http://support.garz-fricke.com/projects/${BOARD_NAME}/Flash-N-Go/FNGBoot/FNGBoot-${FNGBOOT_VERSION}/${FNGBOOT_BIN}"

SRC_URI[md5sum] = "7ad67b5a97aa04e5c1e53153546c8963"
