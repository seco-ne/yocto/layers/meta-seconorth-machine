PACKAGE_ARCH = "${MACHINE_ARCH}"

FNGBOOT_BIN_SHORT = "fngboot.bin"
FNGBOOT_ARCHIVE = "fngboot.tar.gz"

# Default Flash-N-Go Boot offset
FNGBOOT_BS = "512"
FNGBOOT_SEEK = "2"
