SUMMARY = "SECO Northern Europe Flash-N-Go Boot"
HOMEPAGE = "https://www.seco.com"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

require fngboot-common.inc

S = "${WORKDIR}"

FILES:${PN} = "${datadir}/${FNGBOOT_ARCHIVE}"

do_compile() {
	echo "${@ d.getVarFlag('SRC_URI', 'md5sum', True)}  ${FNGBOOT_BIN_SHORT}" > ${FNGBOOT_BIN_SHORT}.md5
	cp ${FNGBOOT_BIN} ${FNGBOOT_BIN_SHORT}
	tar -czf ${FNGBOOT_ARCHIVE} ${FNGBOOT_BIN_SHORT} ${FNGBOOT_BIN_SHORT}.md5
}

do_install() {
	install -d ${D}${datadir}
	install -m 0644 ${S}/${FNGBOOT_ARCHIVE} ${D}${datadir}/
}

inherit deploy

addtask deploy after do_compile

do_deploy() {
	install -m 0644 ${FNGBOOT_ARCHIVE} ${DEPLOYDIR}/${FNGBOOT_ARCHIVE}
	install -m 0644 ${FNGBOOT_BIN_SHORT} ${DEPLOYDIR}/${FNGBOOT_BIN_SHORT}
	install -m 0644 ${FNGBOOT_BIN_SHORT}.md5 ${DEPLOYDIR}/${FNGBOOT_BIN_SHORT}.md5
}
