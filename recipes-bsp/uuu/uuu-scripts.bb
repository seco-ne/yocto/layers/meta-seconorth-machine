SUMMARY = "SECO Northern Europe uuu scripts for initial commissioning"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = " \
	file://uuu.lst;subdir=${BP} \
	file://uuu-seco-mx6.lst;subdir=${BP} \
	file://uuu-seco-mx6ull.lst;subdir=${BP} \
"
inherit deploy kernel-artifact-names
DEPENDS = "uuu-native"

do_deploy() {
    bberror "MACHINE specific do_deploy override function is not found. Make sure MACHINE is suppoerted."
}

do_deploy:mx8() {
	if test -n "${KERNEL_DEVICETREE}"; then
		for DTB in ${KERNEL_DEVICETREE}; do
			ext="${DTB##*.}"
			if [ $ext = 'dtb' ];then
				DTB_BASE_NAME=$(basename ${DTB} .dtb)
				# Generate .sh files based on different DTB files
				cat > uuu-${DTB_BASE_NAME}.sh <<EOF
#!/bin/sh
UUU=\$(which uuu 2>/dev/null)
if [ -z "\$UUU" ];then
    UUU="\$(dirname "\$0")/uuu"
fi
\$UUU -d -b uuu.lst imx-boot ${KERNEL_IMAGETYPE}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT} ${DTB_BASE_NAME}.dtb fngsystem-image-${MACHINE}.cpio.gz
EOF

				# Generate Windows .bat files based on different DTB files
				cat > uuu-${DTB_BASE_NAME}.bat <<EOF
.\uuu.exe -d -b uuu.lst imx-boot ${KERNEL_IMAGETYPE}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT} ${DTB_BASE_NAME}.dtb fngsystem-image-${MACHINE}.cpio.gz
EOF
			fi
		done
	fi

	install -d ${DEPLOYDIR}/
	install -m 0755 ${S}/uuu.lst ${DEPLOYDIR}/uuu.lst
	install -m 0755 ${S}/uuu-*.sh ${DEPLOYDIR}/
	install -m 0755 ${S}/uuu-*.bat ${DEPLOYDIR}/
}

KERNEL_ADDR:mx6    = "0x10010000"
KERNEL_ADDR:mx6ull = "0x80800000"

INITRD_ADDR:mx6    = "0x13C00000"
INITRD_ADDR:mx6ull = "0x82000000"

do_deploy:mx6() {
	if test -n "${KERNEL_DEVICETREE}"; then
		for DTB in ${KERNEL_DEVICETREE}; do
			ext="${DTB##*.}"
			if [ $ext = 'dtb' ];then
				DTB_BASE_NAME=$(basename ${DTB} .dtb)
				# Generate .sh files based on different DTB files
				cat > uuu-${DTB_BASE_NAME}.sh <<EOF
#!/bin/sh
FNGSYSTEM_SIZE=\$(stat -c "%s" "\$(realpath fngsystem-image-${MACHINE}.cpio.gz)")
echo "exec -b ${KERNEL_ADDR} -r ${INITRD_ADDR} -s \$FNGSYSTEM_SIZE \"console=ttymxc0,115200 root=/dev/ram0 rootfstype=ramfs rdinit=/sbin/init cma=0 fngsystem=mfg\"" > fngboot-mx6.cfg

cat ${KERNEL_IMAGETYPE}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT} ${DTB_BASE_NAME}.dtb > linuximage

UUU=\$(which uuu 2>/dev/null)
if [ -z "\$UUU" ];then
    UUU="\$(dirname "\$0")/uuu"
fi
\$UUU -d -b uuu-${MACHINE}.lst fngboot.bin fngboot-mx6.cfg linuximage fngsystem-image-${MACHINE}.cpio.gz
EOF

				# Generate Windows .bat files based on different DTB files
				cat > uuu-${DTB_BASE_NAME}.bat <<EOF
SET "filename=fngsystem-image-${MACHINE}.cpio.gz"
FOR %%A IN (%filename%) DO ECHO exec -b 0x10010000 -r 0x13C00000 -s %%~zA > fngboot-mx6.cfg
ECHO "console=ttymxc0,115200 root=/dev/ram0 rootfstype=ramfs rdinit=/sbin/init cma=0 fngsystem=mfg" >> fngboot-mx6.cfg

COPY /B ${KERNEL_IMAGETYPE}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT} + ${DTB_BASE_NAME}.dtb linuximage

.\uuu.exe -d -b uuu-${MACHINE}.lst fngboot.bin fngboot-mx6.cfg linuximage fngsystem-image-${MACHINE}.cpio.gz
EOF
			fi
		done
	fi

	install -d ${DEPLOYDIR}/
	install -m 0755 ${S}/uuu-${MACHINE}.lst ${DEPLOYDIR}/uuu-${MACHINE}.lst
	install -m 0755 ${S}/uuu-*.sh ${DEPLOYDIR}/
	install -m 0755 ${S}/uuu-*.bat ${DEPLOYDIR}/
}

addtask do_deploy after do_install

# We do not install anything to the root file system
ALLOW_EMPTY:${PN} = "1"
