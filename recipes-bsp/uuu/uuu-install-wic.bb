SUMMARY = "SECO Northern Europe uuu scripts for initial commissioning"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = " \
	file://flash-mx6-wic.bat;subdir=${BP} \
	file://flash-mx6-wic.sh;subdir=${BP} \
	file://fngboot-uboot.cfg;subdir=${BP} \
	file://uuu-mx6-install-wic.lst;subdir=${BP} \
"

inherit nopackages deploy

do_patch[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"

do_deploy[depends] += "\
    uuu-native:do_deploy \
"

UBOOT_LOADADDR = "0x17800000"

do_deploy() {
    bberror "MACHINE specific do_deploy override function is not found. Make sure MACHINE is suppoerted."
}

do_deploy:mx6() {
	install -d ${DEPLOYDIR}/

	install -m 0755 ${S}/flash-mx6-wic.bat ${DEPLOYDIR}/flash-mx6-wic.bat
	install -m 0755 ${S}/flash-mx6-wic.sh ${DEPLOYDIR}/flash-mx6-wic.sh

	install -m 0644 ${S}/fngboot-uboot.cfg ${DEPLOYDIR}/fngboot-uboot.cfg
	sed -i -e 's#@UBOOT_LOADADDR@#${UBOOT_LOADADDR}#g' ${DEPLOYDIR}/fngboot-uboot.cfg

	install -m 0644 ${S}/uuu-mx6-install-wic.lst ${DEPLOYDIR}/uuu-mx6-install-wic.lst
	sed -i -e 's#@UBOOT_LOADADDR@#${UBOOT_LOADADDR}#g' ${DEPLOYDIR}/uuu-mx6-install-wic.lst
}

addtask do_deploy after do_install
