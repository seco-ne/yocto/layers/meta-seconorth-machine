SUMMARY = "UUU (Universal Update Utility)"
DESCRIPTION = "UUU is Freescale/NXP I.MX Chip image deploy tools."
HOMEPAGE = "https://github.com/nxp-imx/mfgtools"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=38ec0c18112e9a92cffc4951661e85a5"

SRC_URI = "\
	https://github.com/nxp-imx/mfgtools/releases/download/uuu_${PV}/uuu;subdir=${BP};name=linuxbin \
	https://github.com/nxp-imx/mfgtools/releases/download/uuu_${PV}/uuu.exe;subdir=${BP};name=windowsbin \
	https://raw.githubusercontent.com/nxp-imx/mfgtools/master/LICENSE;subdir=${BP};name=license \
"

SRC_URI[linuxbin.sha256sum] = "8124d7720e8d9784045de6210a7435752a2826e4902a75d2a0f1410e539227e5"
SRC_URI[windowsbin.sha256sum] = "7537fde74b8f11c2cba1f226780cc9158ddd5001e3a0df3812135354ef5c86e4"
SRC_URI[license.sha256sum] = "cc8d47f7b9260f6669ecd41c24554c552f17581d81ee8fc602c6d23edb8bf495"

inherit bin_package
inherit deploy

# To prevent unexpected behaviour, native must be included last.
inherit native

INSANE_SKIP:${PN} += "already-stripped"

do_deploy() {
	install -d ${DEPLOYDIR}/
	install -m 0755 ${S}/uuu ${DEPLOYDIR}/uuu
	install -m 0755 ${S}/uuu.exe ${DEPLOYDIR}/uuu.exe
}
addtask do_deploy after do_install

# We do not install anything to the root file system
ALLOW_EMPTY:${PN} = "1"
