# Copyright (C) 2012-2016 O.S. Systems Software LTDA.
# Copyright (C) 2013-2016 Freescale Semiconductor
# Copyright (C) 2017-2022 NXP

SUMMARY = "i.MX Register Manipulation Tool"
DESCRIPTION = "Tool to manipulate (read/write) the registers of \
               i.MX SoCs (taken from the imx-test package)"
SECTION = "base"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-or-later;md5=fed54355545ffd980b814dab4a3b312c"

PE = "1"
PV = "7.0+${SRCPV}"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = " \
    git://github.com/nxp-imx/imx-test.git;protocol=https;branch=${SRCBRANCH} \
    file://0001-Add-compile-install-and-clean-to-memtool.patch \
"

SRCBRANCH = "lf-5.15.32_2.0.0"
SRCREV = "c640c7e8456b0516851e76adb2acce6b3866b1fb"

S = "${WORKDIR}/git/test/memtool"

INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

PARALLEL_MAKE = "-j 1"
EXTRA_OEMAKE += "${PACKAGECONFIG_CONFARGS}"

do_compile() {
    CFLAGS="${TOOLCHAIN_OPTIONS}"
    oe_runmake V=1 VERBOSE='' \
               CROSS_COMPILE=${TARGET_PREFIX} \
               INC="-I${S}" \
               CC="${CC} -L${STAGING_LIBDIR} ${LDFLAGS}" \
               SDKTARGETSYSROOT=${STAGING_DIR_HOST} \
               LINUXPATH=${STAGING_KERNEL_DIR} \
               KBUILD_OUTPUT=${STAGING_KERNEL_BUILDDIR}
}

do_install() {
    oe_runmake DESTDIR=${D}/${sbindir} \
               install
}

FILES:${PN} += "${sbindir}"
