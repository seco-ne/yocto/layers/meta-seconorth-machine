SUMMARY = "SECO Northern Europe install and postinstallation scripts"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit autotools
inherit deploy
# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

RDEPENDS:${PN} = " \
         kernel-image \
         kernel-devicetree \
"

SRCREV = "${AUTOREV}"
SRC_URI = " \
    git://git.seco.com/seco-ne/tools/yocto-install.git;protocol=https;branch=main;nobranch=1 \
"
S="${WORKDIR}/git"

#=================================================================
# Use the current MACHINE and DEFAULT_ROOTFS in the configure step
export MACHINE
export DEFAULT_ROOTFS = "seconorth-image-${MACHINE}.tar.gz"
#=================================================================

# TODO should also go to the Makefile I guess
do_compile:append:fng() {
    # Exchange the bootpartitions for Flash-N-Go System
    sed -i ${S}/prepare-image/prepare-kernel.sh -e 's|^BOOTPARTITIONS=".*|BOOTPARTITIONS="/dev/disk/by-label/Flash-N-Go /dev/mmcblk0p1 /dev/mmcblk1p1"|'
}

# Sort out some unneeded files. The yocto-install package contains
# all install scripts but we do not need all of them in one config

# TODO this is a bit ugly, may be we should change the scripts
# to use them in all machines or leave this to the Makefile
# of the package
DELETE_FILES = " prepare-rtc.sh prepare-dt.sh prepare-xml2dto.sh "
DELETE_FILES:remove:mx6 = "  prepare-dt.sh prepare-xml2dto.sh "
DELETE_FILES:remove:mx8mm = "  prepare-rtc.sh "

do_install:append() {

        echo ${MACHINE_INSTALL_SCRIPTS}
        cd ${D}${datadir}/${PN}/

        for script in *.sh;
        do
                # Check if script is in MACHINE_INSTALL_SCRIPTS
                # Delete it otherwise
                keep="false"
                for needed_script in ${MACHINE_INSTALL_SCRIPTS};
                do
                        if [ "$script" = "$needed_script" ];then
                                keep="true"
                                break
                        fi
                done

                if $keep;then
                        echo "Install script: $script"
                else
                        echo "Delete $script"
                        rm "$script"
                fi
        done

        for file in ${DELETE_FILES};
        do
             for found in $(find ${D} -name "*${file%.sh}*")
                do
                        echo "Deleting $found"
                        rm $found
                done
        done
}

# the 'combine_kernel' configures a postinstall task to combine kernel and
# devicetree
do_install:append:mx6() {
        install -d ${D}/boot
        touch ${D}/boot/combine_kernel
}

# This configures the default devicetree used by the postinstall step
# to configure the loaded devicetree
do_install:append:mx8() {
        install -d ${D}/boot
        echo ${DEFAULT_DEVICETREE} >> ${D}/boot/devicetree
}

do_deploy() {
        install -d ${DEPLOYDIR}/boot
        for i in ${D}/${datadir}/${PN}/*.sh;
        do
                install -m 0755 $i ${DEPLOYDIR}
        done
}

# combine_kernel is needed for Edgehog WIC and for bootfs_image for rauc
do_deploy:append:mx6() {
    touch ${DEPLOYDIR}/boot/combine_kernel
}

# devicetree is needed for bootfs_image for rauc
do_deploy:append:mx8() {
    cp ${D}/boot/devicetree ${DEPLOYDIR}/boot/devicetree
}

addtask do_deploy after do_install

PACKAGES_MACHINE = ""
# use mx6q override as Nallino is currently not supported
# TODO: currently the u-boot build for seco-mx6-fsl does not
# work, so we replace the mx6q overide with the seco-mx6.
# This should be reverted when the u-boot build is fixed.
PACKAGES_MACHINE:seco-mx6 += " \
     ${PN}-mx6-boot \
     "

PACKAGES_MACHINE:seco-mx6ull += " \
     ${PN}-mx6-boot \
     "

PACKAGES_MACHINE:mx8m += " \
     ${PN}-u-boot \
     "

PACKAGES += " \
     ${PN}-fngsystem \
     ${PN}-ontarget \
     ${PACKAGES_MACHINE} \
     ${PN}-services \
     "

FILES:${PN} = "${datadir}/${PN}/fng-install.sh"
FILES:${PN}-fngsystem = "${datadir}/${PN}/fngsystem*.sh"
FILES:${PN}-u-boot = "${datadir}/${PN}/fng-install-uboot.sh"
FILES:${PN}-mx6-boot = "${datadir}/${PN}/fng-install-mx6-boot.sh"
FILES:${PN}-ontarget = "${sysconfdir}/** ${libexecdir}/** ${sbindir}/**"
FILES:${PN}-ontarget:append:mx6 = " boot/combine_kernel "
FILES:${PN}-ontarget:append:mx8 = " boot/devicetree "
FILES:${PN}-services = "${systemd_system_unitdir}/**"

RPROVIDES:${PN} += " fng-install.sh "

RPROVIDES:${PN}-fngsystem += " fngsystem-self-update.sh fngsystem-self-init.sh "
RPROVIDES:${PN}-mx6-boot:seco-mx6 += " fng-install-mx6-boot.sh "
RPROVIDES:${PN}-mx6-boot:seco-mx6ull += " fng-install-mx6-boot.sh "
RPROVIDES:${PN}-u-boot:mx8m += " fng-install-uboot.sh "

RDEPENDS:${PN} = "${PN}-ontarget kernel-image kernel-devicetree"
RDEPENDS:${PN}-fngsystem = "${PN}-ontarget"
RDEPENDS:${PN}-u-boot:mx8m = "${PN}-ontarget imx-boot"
RDEPENDS:${PN}-mx6-boot:seco-mx6 = "${PN}-ontarget mx6-boot"
RDEPENDS:${PN}-mx6-boot:seco-mx6ull = "${PN}-ontarget mx6-boot"
RDEPENDS:${PN}-services = "${PN}-ontarget gfxml2dto"

DEPLOY_DEPENDS = ""
DEPLOY_DEPENDS:seco-mx6 = "mx6-boot:do_deploy"
DEPLOY_DEPENDS:seco-mx6ull = "mx6-boot:do_deploy"
DEPLOY_DEPENDS:mx8m = "imx-boot:do_deploy"
do_deploy[depends] += " ${DEPLOY_DEPENDS} "

# i2c-tools is for rtc detection
# phytool for ethernet phy detection
# libgpiod-tools for ethernet phy detection
# sharedconf for common functions
# netpbm and seco-pnmtologo for logo.dat creation
RDEPENDS:${PN}-ontarget = " \
    i2c-tools \
    libgpiod-tools \
    netpbm \
    phytool \
    seco-pnmtologo \
    seco-sharedconf \
    seco-sharedconf-network \
"

inherit systemd

SYSTEMD_SERVICE:${PN}-services = "seco-hmi-postinstallation.service"
SYSTEMD_PACKAGES = "${PN}-services"
