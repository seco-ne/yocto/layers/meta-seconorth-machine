SUMMARY = "Pre-shutdown Power Check service"
DESCRIPTION = "This service checks whether a board is supplied \
by battery or by PSU during the shutdown routine. \
And when the main power (PSU) is absent, the shutdow-pwr-check service \
reboots a board instead of performing a powerdown."

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-or-later;md5=fed54355545ffd980b814dab4a3b312c"

SRC_URI = "\
	file://shutdown-pwr-check.service \
	file://shutdown-pwr-check.sh \
"

RDEPENDS:${PN} = "bash libgpiod libgpiod-tools"

inherit systemd

do_install() {
	install -D -m 0644 ${WORKDIR}/shutdown-pwr-check.service ${D}${systemd_system_unitdir}/shutdown-pwr-check.service
	install -D -m 0755 ${WORKDIR}/shutdown-pwr-check.sh ${D}${systemd_system_unitdir}/shutdown-pwr-check.sh
}

SYSTEMD_SERVICE:${PN} = "shutdown-pwr-check.service"
SYSTEMD_AUTO_ENABLE = "enable"

FILES:${PN} = " \
    ${systemd_system_unitdir}/** \
"
