#!/bin/bash

if ! systemctl list-jobs | grep -q -E 'poweroff.target.*start'; then
        exit 0;
fi

PFO12V=$(gpiofind pfo_12v)
if [ -n "${PFO12V}" ]; then
        if [ "$(gpioget $PFO12V)" == "1" ]; then
                systemctl reboot
        fi
fi
