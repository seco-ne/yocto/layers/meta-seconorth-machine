SUMMARY = "Restore WakeUp_OK State service"
DESCRIPTION = "This service runs before poweroff call and restores \
the state of WakeUp_OK signal by setting a corresponding 'wakeup_ok' \
GPIO high. This makes a board to start-up automatically when the main \
power (PSU) will be provided again in future."

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-or-later;md5=fed54355545ffd980b814dab4a3b312c"

SRC_URI = "file://restore-wakeup-ok.service"

RDEPENDS:${PN} = "bash libgpiod libgpiod-tools"

inherit systemd

do_install() {
	install -D -m 0644 ${WORKDIR}/restore-wakeup-ok.service ${D}${systemd_system_unitdir}/restore-wakeup-ok.service
}

# Override default systemd_postinst to avoid the service restart after the installation
systemd_postinst() {
if systemctl >/dev/null 2>/dev/null; then
	OPTS=""

	if [ -n "$D" ]; then
		OPTS="--root=$D"
	fi

	if [ "${SYSTEMD_AUTO_ENABLE}" = "enable" ]; then
		for service in ${SYSTEMD_SERVICE_ESCAPED}; do
			systemctl ${OPTS} enable "$service"
		done
	fi

	if [ -z "$D" ]; then
		systemctl daemon-reload
		systemctl preset ${SYSTEMD_SERVICE_ESCAPED}
	fi
fi
}

SYSTEMD_SERVICE:${PN} = "restore-wakeup-ok.service"
SYSTEMD_AUTO_ENABLE = "enable"

FILES:${PN} = "${systemd_system_unitdir}/**"
