SUMMARY = "SECO Northern Europe platform bootscript for FNGBoot"
HOMEPAGE = "https://north.seco.com"
DESCRIPTION = "The packages contains bootscripts for seco hmi devices, both for uboot and fngboot"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit deploy

DEPENDS += "u-boot-mkimage-native"

BOOT_SCRIPTS = "boot.cfg boot.scr features.scr"
BOOT_SCRIPTS_ALT = "boot-alt.cfg boot-alt.scr features-alt.scr"
BOOT_SCRIPTS:mx6 = "boot.cfg"
BOOT_SCRIPTS_ALT:mx6 = "boot-alt.cfg"

PROVIDES += "${BOOT_SCRIPTS} ${BOOT_SCRIPTS_ALT}"
RPROVIDES:${PN} += "${BOOT_SCRIPTS}"
RPROVIDES:${PN}-alt += "${BOOT_SCRIPTS_ALT}"

SRC_URI = " \
    file://boot.cfg \
    file://boot-alt.cfg \
    file://boot.scr.cfg \
    file://features.scr.cfg \
"

PACKAGES += "${PN}-alt"

S = "${WORKDIR}"

RAMDISK_OFFSET = "0x2000000"

do_configure() {
    # Set the default devicetree in boot.cfg and boot-alt.cfg
    sed -i boot.cfg -e "s|devicetree=.*|devicetree=${DEFAULT_DEVICETREE}|"
    sed -i boot-alt.cfg -e "s|devicetree=.*|devicetree=${DEFAULT_DEVICETREE}|"

    # Insert the correct load address for the target platform in boot-alt.cfg
    RAMDISK_LOAD_ADDR="$( printf "0x%08X" "$( awk "BEGIN {print ${RAM_START_ADDRESS} + ${RAMDISK_OFFSET}}")" )"
    sed -i boot-alt.cfg -e "s|load -b 0x[0-9abcdefABCDEF]\+|load -b ${RAMDISK_LOAD_ADDR}|"

    # Create a boot-alt.scr.cfg from boot.scr.cfg
    sed boot.scr.cfg \
        -e "s|boot\.cfg|boot-alt.cfg|" \
        -e "s|features\.scr|features-alt.scr|" \
        > boot-alt.scr.cfg
    # duplicate features.scr also, as one file can only be part of one package
    cp features.scr.cfg features-alt.scr.cfg
}

do_compile() {

    for f in *.scr.cfg;
    do
        echo "Create image from $f"
        mkimage -A arm -T script -O linux -d ${WORKDIR}/${f} ${WORKDIR}/${f%.cfg}
    done
}

do_install() {
    install -d ${D}/boot

    for f in  ${BOOT_SCRIPTS} ${BOOT_SCRIPTS_ALT};
    do
        install -m 0644 ${S}/${f} ${D}/boot/
    done
}

do_deploy() {
    install -d ${DEPLOYDIR}/boot
    for f in ${BOOT_SCRIPTS};
    do
        install -m 0644 ${S}/${f} ${DEPLOYDIR}/boot/
    done
}

do_deploy:fng() {
    install -d ${DEPLOYDIR}/boot
    for f in ${BOOT_SCRIPTS_ALT};
    do
        install -m 0644 ${S}/${f} ${DEPLOYDIR}/boot/
    done
}

FILES:${PN} = " ${@' '.join([ "boot/" + f for f in d.getVar('BOOT_SCRIPTS').split() ])} "
FILES:${PN}-alt = " ${@' '.join([ "boot/" + f for f in d.getVar('BOOT_SCRIPTS_ALT').split() ])} "


addtask do_deploy after do_install
do_deploy[recrdeptask] += "do_deploy"
do_deploy[cleandirs] = "${DEPLOYDIR}"
