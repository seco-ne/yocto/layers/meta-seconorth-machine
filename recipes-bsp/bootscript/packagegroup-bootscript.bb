SUMMARY = "SECO Northern Europe platform bootscript"
HOMEPAGE = "https://north.seco.com"

inherit packagegroup
inherit deploy

PACKAGES = " \
    ${PN} \
"

RDEPENDS:${PN} = "boot.cfg"
RDEPENDS:${PN}:fng = "boot-alt.cfg"

do_deploy() {
    echo Just to path through the deps
}
addtask do_deploy
do_deploy[recrdeptask] += "do_deploy"
