DESCRIPTION = "i.MX U-Boot suppporting SECO boards"

require recipes-bsp/u-boot/u-boot.inc
require u-boot-seco-imx-common_${PV}.inc

PROVIDES += "u-boot"

inherit uuu_bootloader_tag

UUU_BOOTLOADER            = ""
UUU_BOOTLOADER:mx6-nxp-bsp        = "${UBOOT_BINARY}"
UUU_BOOTLOADER:mx7-nxp-bsp        = "${UBOOT_BINARY}"
UUU_BOOTLOADER_TAGGED     = ""
UUU_BOOTLOADER_TAGGED:mx6-nxp-bsp = "u-boot-tagged.${UBOOT_SUFFIX}"
UUU_BOOTLOADER_TAGGED:mx7-nxp-bsp = "u-boot-tagged.${UBOOT_SUFFIX}"

do_deploy:append:mx8m-nxp-bsp() {
    # Deploy u-boot-nodtb.bin and fsl-imx8m*-XX.dtb for mkimage to generate boot binary
    if [ -n "${UBOOT_CONFIG}" ]
    then
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    install -d ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/arch/arm/dts/${UBOOT_DTB_NAME}  ${DEPLOYDIR}/${BOOT_TOOLS}
                    install -m 0777 ${B}/${config}/u-boot-nodtb.bin  ${DEPLOYDIR}/${BOOT_TOOLS}/u-boot-nodtb.bin-${MACHINE}-${type}
                fi
            done
            unset  j
        done
        unset  i
    fi
}

# Get include file from poky layer
FILESEXTRAPATHS:append := "${BSPDIR}/sources/poky/meta/recipes-bsp/u-boot/files:"

DEPENDS += "u-boot-tools-native"

# This is set in poky u-boot.inc to ${PN}-initial-env, which
# names the file u-boot-seco-imx-initial-env,
# but fw_printenv expects then name u-boot-initial-env
UBOOT_INITIAL_ENV = "u-boot-initial-env"
PACKAGES += "${PN}-fw-env"
FILES:${PN}-env:remove = "/etc/fw_env.config"
FILES:${PN}-fw-env = "/etc/fw_env*config"

# Create an uboot-initial-env.bin that can be dd'd to the env offset
# and then correctly read by u-boot and the libuboot-tools
do_compile:append () {
    if [ -n "${UBOOT_CONFIG}" -o -n "${UBOOT_DELTA_CONFIG}" ]
    then
        unset i j k
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    # Generate the uboot-initial-env.bin
                    if [ -n "${UBOOT_INITIAL_ENV}" ]; then
                        # Generate u-boot-initial-conf.bin and fw_env.config snippet
                        eval $( sed -n "/^CONFIG_ENV/p"  ${B}/${config}/include/config/auto.conf )
                        # Set defaults if not found in auto.conf, needed for Tanaro 2019 u-boot
                        [ -n "$CONFIG_ENV_SIZE" ] || CONFIG_ENV_SIZE=0x2000
                        [ -n "$CONFIG_ENV_OFFSET" ] || CONFIG_ENV_OFFSET=0x400000

                        mkenvimage -s $CONFIG_ENV_SIZE -o ${B}/${config}/u-boot-initial-env.bin ${B}/${config}/u-boot-initial-env
                        echo "/dev/mmcblk0  $CONFIG_ENV_OFFSET $CONFIG_ENV_SIZE" > ${B}/${config}/fw_env.config

                        cp ${B}/${config}/u-boot-initial-env.bin ${B}/${config}/u-boot-initial-env-${type}.bin
                        cp ${B}/${config}/fw_env.config ${B}/${config}/fw_env-${type}.config
                    fi
                    unset k
                fi
            done
            unset  j
        done
        unset  i
    else
        # Generate the uboot-initial-env.bin
        if [ -n "${UBOOT_INITIAL_ENV}" ]; then
            # Generate u-boot-initial-conf.bin and fw_env.config snippet
            eval $( sed -n "/^CONFIG_ENV/p"  ${B}/${config}/include/config/auto.conf )
            mkenvimage -s $CONFIG_ENV_SIZE -o ${B}/${config}/u-boot-initial-env.bin
            echo "/dev/mmcblk0  $CONFIG_ENV_OFFSET $CONFIG_ENV_SIZE" > ${B}/${config}/fw_env.config
        fi
    fi
}

do_install:append () {
    if [ -n "${UBOOT_CONFIG}" ]
    then
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    # Install the uboot-initial-env.bin and fw_env.config
                    if [ -n "${UBOOT_INITIAL_ENV}" ]; then
                        install -D -m 644 ${B}/${config}/u-boot-initial-env-${type}.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin
                        ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}-${MACHINE}-${type}.bin
                        ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}-${type}.bin
                        ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}.bin

                        install -D -m 644 ${B}/${config}/fw_env-${type}.config ${D}/${sysconfdir}/fw_env-${MACHINE}-${type}-${PV}-${PR}.config
                        ln -sf fw_env-${MACHINE}-${type}-${PV}-${PR}.config ${D}/${sysconfdir}/fw_env-${MACHINE}-${type}.config
                        ln -sf fw_env-${MACHINE}-${type}-${PV}-${PR}.config ${D}/${sysconfdir}/fw_env-${type}.config
                        ln -sf fw_env-${MACHINE}-${type}-${PV}-${PR}.config ${D}/${sysconfdir}/fw_env.config
                    fi
                fi
            done
            unset  j
        done
        unset  i
    else
        # Install the uboot-initial-env.bin and fw_env.config
        if [ -n "${UBOOT_INITIAL_ENV}" ]; then
            install -D -m 644 ${B}/u-boot-initial-env.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}-${MACHINE}-${PV}-${PR}.bin
            ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${PV}-${PR}.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}-${MACHINE}.bin
            ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${PV}-${PR}.bin ${D}/${sysconfdir}/${UBOOT_INITIAL_ENV}.bin

            install -D -m 644 ${B}/fw_env.config ${D}/${sysconfdir}/fw_env-${MACHINE}-${PV}-${PR}.config
            ln -sf fw_env-${MACHINE}-${PV}-${PR}.config ${D}/${sysconfdir}/fw_env-${MACHINE}.config
            ln -sf fw_env-${MACHINE}-${PV}-${PR}.config ${D}/${sysconfdir}/fw_env.config
        fi
    fi
}

do_deploy:append () {
    if [ -n "${UBOOT_CONFIG}" ]
    then
        for config in ${UBOOT_MACHINE}; do
            i=$(expr $i + 1);
            for type in ${UBOOT_CONFIG}; do
                j=$(expr $j + 1);
                if [ $j -eq $i ]
                then
                    # Deploy the uboot-initial-env.bin and fw_env.config
                    if [ -n "${UBOOT_INITIAL_ENV}" ]; then
                        install -D -m 644 "${B}/${config}/u-boot-initial-env-${type}.bin" ${DEPLOYDIR}/${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin
                        cd ${DEPLOYDIR}
                        ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin ${UBOOT_INITIAL_ENV}-${MACHINE}-${type}.bin
                        ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${type}-${PV}-${PR}.bin ${UBOOT_INITIAL_ENV}-${type}.bin

                        install -D -m 644 "${B}/${config}/fw_env-${type}.config" ${DEPLOYDIR}/fw_env-${MACHINE}-${type}-${PV}-${PR}.config
                        cd ${DEPLOYDIR}
                        ln -sf fw_env-${MACHINE}-${type}-${PV}-${PR}.config fw_env-${MACHINE}-${type}.config
                        ln -sf fw_env-${MACHINE}-${type}-${PV}-${PR}.config fw_env-${type}.config
                    fi
                    # Deploy additional devicetrees, if more then one should integrated into the image
                    for dtb in ${UBOOT_ADDITIONAL_DTB_NAMES};do
                        install -m 0777 ${B}/${config}/arch/arm/dts/${dtb}  ${DEPLOYDIR}/${BOOT_TOOLS}
                    done
                fi
            done
            unset  j
        done
        unset  i
    else
        # Deploy the uboot-initial-env and fw_env.config
        if [ -n "${UBOOT_INITIAL_ENV}" ]; then
            install -D -m 644 ${B}/${config}/u-boot-initial-env.bin ${DEPLOYDIR}/${UBOOT_INITIAL_ENV}-${MACHINE}-${PV}-${PR}.bin
            cd ${DEPLOYDIR}
            ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${PV}-${PR}.bin ${UBOOT_INITIAL_ENV}-${MACHINE}.bin
            ln -sf ${UBOOT_INITIAL_ENV}-${MACHINE}-${PV}-${PR}.bin ${UBOOT_INITIAL_ENV}.bin

            install -D -m 644 ${B}/${config}/fw_env.config ${DEPLOYDIR}/fw_env-${MACHINE}-${PV}-${PR}.config
            cd ${DEPLOYDIR}
            ln -sf fw_env-${MACHINE}-${PV}-${PR}.config fw_env-${MACHINE}.config
            ln -sf fw_env-${MACHINE}-${PV}-${PR}.config fw_env.config
        fi
        # Deploy additional devicetrees, if more then one should integrated into the image
        for dtb in ${UBOOT_ADDITIONAL_DTB_NAMES};do
            install -m 0777 ${B}/${config}/arch/arm/dts/${dtb}  ${DEPLOYDIR}/${BOOT_TOOLS}
        done
    fi
}

# This is needed for Edgehog imx6 build.
# BOOTSCR_NAME is only defined in Edgehog layers.
do_deploy:append:seco-arm () {
    if test -f ${WORKDIR}/${BOOTSCR_NAME}
    then
        sed -i 's/bootz/bootm/g' ${WORKDIR}/${BOOTSCR_NAME}
    fi
}

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(seco-mx8mm|seco-mx8mp|seco-mx6|seco-mx6ull)"
