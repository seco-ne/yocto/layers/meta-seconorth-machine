LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://Licenses/gpl-2.0.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRCBRANCH = "guf_imx_v2020.04-trizeps8plus_2021_12_07"
SRC_URI = " \
    git://git.seco.com/seco-ne/3rd-party/kuk/uboot-imx-kuk.git;protocol=https;branch=${SRCBRANCH};nobranch=1 \
"
SRCREV = "${AUTOREV}"

LOCALVERSION ?= "-${DISTRO_VERSION}"
SCMVERSION_BEFORE_LOCALVERSION = "y"

DEPENDS += "flex-native bison-native bc-native dtc-native"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

inherit seco-u-boot-localversion

BOOT_TOOLS = "imx-boot-tools"
