SUMMARY = "Initialization of Bluetooth interface."
DESCRIPTION = "Bring-up hci0 interface and make it discoverable. \
For Tanaro boards it's also needed to call rtk_hciattach tool before \
hcitool."

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-or-later;md5=fed54355545ffd980b814dab4a3b312c"

SRC_URI = "\
	file://bluetooth-init.service \
	file://bluetooth-init.sh \
"

RDEPENDS:${PN} = "bluez5"

inherit systemd

do_install() {
	install -D -m 0644 ${WORKDIR}/bluetooth-init.service ${D}${systemd_system_unitdir}/bluetooth-init.service
	install -D -m 0755 ${WORKDIR}/bluetooth-init.sh ${D}${systemd_system_unitdir}/bluetooth-init.sh
}

SYSTEMD_SERVICE:${PN} = "bluetooth-init.service"
SYSTEMD_AUTO_ENABLE = "disable"

FILES:${PN} = "${systemd_system_unitdir}/**"
