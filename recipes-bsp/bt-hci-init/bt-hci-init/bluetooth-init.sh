#!/bin/sh -e

# Realtek RTL8723DS is installed on Tanaro boards and it requires an additional
# initialization via rtk_hciattach tool
if grep -q -i tanaro /sys/devices/soc0/machine; then
    if ! hciconfig hci0; then
        # try hciattach
        rtk_hciattach ttymxc3 rtk_h5
        sleep 1
        # and recheck again
        if ! hciconfig hci0; then
            echo Failed to bring up
            exit 1
        fi
    fi
fi

# The azurewave module on the trizeps module needs its own hciattach call
if grep -q -i trizeps /sys/devices/soc0/machine; then
    if ! hciconfig hci0; then
        # try hciattach
        hciattach /dev/ttymxc2 any 115200 flow
        sleep 1
        # and recheck again
        if ! hciconfig hci0; then
            echo Failed to bring up
            exit 1
        fi
    fi
fi

hciconfig hci0 up
hciconfig hci0 piscan

exit 0
