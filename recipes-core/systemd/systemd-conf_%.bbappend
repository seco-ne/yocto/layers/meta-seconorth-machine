FILESEXTRAPATHS:prepend := "${THISDIR}/systemd-conf:"

SRC_URI += " \
    file://wired.network-do-not-send-DHCP-release.patch \
    file://journald-add-kmsg-logs.patch \
"

S = "${WORKDIR}"
