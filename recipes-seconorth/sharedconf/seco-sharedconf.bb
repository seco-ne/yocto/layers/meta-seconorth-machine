SUMMARY = "SECO North shared configuration script for xml config files"
HOMEPAGE = "https://git.seco.com/tools/seco-sharedconf"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit systemd
inherit update-rc.d
inherit autotools

SRCREV = "${AUTOREV}"

SRC_URI = " \
    git://git.seco.com/seco-ne/tools/seco-sharedconf.git;protocol=https;branch=main;nobranch=1 \
"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S="${WORKDIR}/git"

PACKAGES += " ${PN}-network ${PN}-showversion "
RDEPENDS:${PN} = " xmlstarlet "
RDEPENDS:${PN}-network += " ${PN} ethtool "
RDEPENDS:${PN}-showversion += " ${PN} "

do_install:append() {

    # The package installs the systemd service to /usr/lib but yocto expects is to be in /lib
    mv ${D}/usr/lib ${D}/

    if ! ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
        # Remove systemd unit
        rm -rf ${D}${systemd_system_unitdir}/
    fi
    if ! ${@bb.utils.contains('DISTRO_FEATURES','sysvinit','true','false',d)}; then    
        # Remove sysvinit script
        rm -rf ${D}${sysconfdir}/init.d
    fi

}

# Note: We used to generate the network configuration upon the first boot.
# We now support an option for a read-only filesystem, where writing the
# configuration during the first boot doesn't work anymore. The network-conf
# script is still shipped with this recipe, but the execution has been moved
# to the post-install of the fng-install script (see fng-install/prepare-image).

FILES:${PN} = " \
    ${sbindir}/conf-reader.sh \
    ${sbindir}/seco-functions.sh \
    ${sbindir}/sys-versions.sh \
    ${sbindir}/gf-versions.sh \
    ${sbindir}/serial2mac.sh \
    ${sbindir}/mac2serial.sh \
    ${sbindir}/sconfig \
    ${sysconfdir}/fng-postinstall/sconfig-init \
    ${sysconfdir}/init.d/etc-shared-remount \
    ${sysconfdir}/shared \
    ${sysconfdir}/udev/** \
    ${systemd_system_unitdir}/** \
    /usr/share/sharedconf/* \
"

FILES:${PN}-network = " \
    ${sbindir}/network-conf.sh \
    ${sbindir}/lan95xx-init-eeprom.sh \
    ${sbindir}/lan95xx-set-mac.sh \
    ${sbindir}/lan95xx-set-serial.sh \
    ${sbindir}/mask2cidr.sh \
    ${sysconfdir}/init.d/generate-network-conf \
    ${sysconfdir}/lan9500a-eeprom.bin \
    ${sysconfdir}/lan9514-eeprom.bin \
"

INITSCRIPT_PACKAGES="${PN}-network ${PN}"

INITSCRIPT_NAME:${PN}="etc-shared-remount"
INITSCRIPT_PARAMS:${PN}="start 10 S ."

INITSCRIPT_NAME:${PN}-network="generate-network-conf"
INITSCRIPT_PARAMS:${PN}-network="start 80 S ."

SYSTEMD_SERVICE:${PN}-network = "generate-network-conf.service"
SYSTEMD_AUTO_ENABLE:fng ?= "enable"
SYSTEMD_AUTO_ENABLE ?= "disable"

FILES:${PN}-showversion = " \
    ${sbindir}/showversion \
    ${sysconfdir}/init.d/showversion \
"

# We need the init.d showversion entry, 
# don't delete it (systemd normally does it with this function)
python rm_sysvinit_initddir () {
    pass
}
