SUMMARY = "Tool to create a devicetree overlay from the SECO North xml device config file."
HOMEPAGE = "https://git.seco.com/seco-ne/tools/gfxml2dto"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "dtc"

SRCREV = "${AUTOREV}"
SRC_URI = "git://git.seco.com/seco-ne/tools/gfxml2dto.git;protocol=https;branch=main;nobranch=1"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

# NOTE: if this software is not capable of being built in a separate build directory
# from the source, you should replace autotools with autotools-brokensep in the
# inherit line
inherit autotools

# Specify any options you want to pass to the configure script using EXTRA_OECONF:
EXTRA_OECONF = ""

