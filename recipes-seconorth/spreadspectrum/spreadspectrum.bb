SUMMARY = "SECO Northern Europe platform spreadspectrum script."
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = "file://spreadspectrum.py"

RDEPENDS:${PN} = " python3 devmem2 imx-memtool"

COMPATIBLE_MACHINE = "mx8"

S = "${WORKDIR}"

do_install() {
	install -d ${D}/sbin
	install -m 0700 ${S}/spreadspectrum.py ${D}/sbin/spreadspectrum.py
}

FILES:${PN} = " \
	sbin/spreadspectrum.py \
	"
