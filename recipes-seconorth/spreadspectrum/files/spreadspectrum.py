#!/usr/bin/env python3
# 
#
import sys
import subprocess
import argparse
from shutil import which

VIDEO_PLL1=0x30360000
PLLBASE=VIDEO_PLL1

FDIV_CTL0=0x2C
FDIV_CTL1=0x30
SSCG_CTRL=0x34

verbose = 0

def has_tool(name):
    """ Check if tool 'name' is available """
    return which(name) is not None

def print_v1(*args, **kwargs):
    if verbose < 1:
        return
    print(*args, file=sys.stderr, **kwargs)

def print_v2(*args, **kwargs):
    if verbose < 2:
        return
    print(*args, file=sys.stderr, **kwargs)

def read_register_devmem2(phyaddr):
    cmd=['devmem2', "0x{:08x}".format(phyaddr)]
    print_v2(cmd)
    result=subprocess.run(cmd,capture_output=True)
    out=result.stdout.decode('UTF-8')
    resultline=out.splitlines()[-1]
    value=int(resultline.split(' ')[-1], 0)
    print_v2("0x{:08x}".format(value))
    return(value)

def read_register_memtool(phyaddr):
    cmd=['memtool', "0x{:08x}".format(phyaddr), '1']
    print_v2(cmd)
    result=subprocess.run(cmd,capture_output=True)
    print_v2(result.stdout)
    out=result.stdout.decode('UTF-8')
    resultline=out.splitlines()[-2]
    print_v2("Search result in: ",resultline)
    value=int(resultline.split(' ')[-1], 16)
    print_v2("0x{:08x}".format(value))
    return(value)

def read_register(phyaddr):
    if has_tool('memtool'):
        return read_register_memtool(phyaddr)
    if has_tool('devmem2'):
        return read_register_devmem2(phyaddr)
    raise Exception("Neither memtool nor devmem2 found.")

def write_register_devmem2(phyaddr, value):
    cmd=['devmem2', "0x{:08x}".format(phyaddr), 'w', "0x{:08x}".format(value) ]
    print_v2(cmd)
    result=subprocess.run(cmd,capture_output=True)
    out=result.stdout.decode('UTF-8')
    print_v2(out)
    lastline=out.splitlines()[-1]
    readbackvalue=int(lastline.split(' ')[-1], 0)
    if readbackvalue != value:
        raise Exception("Writing 0x{:08x} to 0x{:08x} failed.".format(value, phyaddr))

def write_register_memtool(phyaddr, value):
    cmd=['memtool', "{:08x}={:08x}".format(phyaddr, value) ]
    print_v2(cmd)
    result=subprocess.run(cmd,capture_output=True)
    out=result.stdout.decode('UTF-8')
    print_v2(out)
    readbackvalue = read_register_memtool(phyaddr)
    if readbackvalue != value:
        raise Exception("Writing 0x{:08x} to 0x{:08x} failed.".format(value, phyaddr))

def write_register(phyaddr, value):
    if has_tool('memtool'):
        return write_register_memtool(phyaddr, value)
    if has_tool('devmem2'):
        return write_register_devmem2(phyaddr, value)
    raise Exception("Neither memtool nor devmem2 found.")


def decode_ctl0(value):
    pll_post_div = (value >>  0 ) & 0x007
    pll_pre_div =  (value >>  4 ) & 0x03F
    pll_main_div = (value >> 12 ) & 0x3FF
    return pll_post_div,pll_pre_div,pll_main_div

def decode_sscg(value):
    sel_pf =        (value >>  0 ) & 0x003
    pll_mrat_ctl =  (value >>  4 ) & 0x03F
    pll_mfreq_ctl = (value >> 12 ) & 0x0FF
    sscg_en =       (value >> 31 ) & 0x001
    return sel_pf, pll_mrat_ctl, pll_mfreq_ctl, sscg_en

def encode_sscg(sel_pf, pll_mrat_ctl, pll_mfreq_ctl, sscg_en):
    s=sel_pf & 0x003
    if s != sel_pf:
        raise Exception("Value of sel_pf out of range ({})".format(sel_pf))
    print_v2("Set sel_pf: {}".format(s))

    pll_mrat_ctl = int(pll_mrat_ctl)
    r=pll_mrat_ctl & 0x03F
    if r != pll_mrat_ctl:
        raise Exception("Value of pll_mrat_ctl out of range ({})".format(pll_mrat_ctl))
    print_v2("Set pll_mrat_ctl: {}".format(r))

    pll_mfreq_ctl = int(pll_mfreq_ctl)
    f=pll_mfreq_ctl & 0x0FF
    if f != pll_mfreq_ctl:
        raise Exception("Value of pll_mfreq_ctl out of range ({})".format(pll_mfreq_ctl))
    print_v2("Set pll_mfreq_ctl: {}".format(f))

    e=sscg_en & 0x001
    if e != sscg_en:
        raise Exception("Value of sscg_en out of range ({})".format(sscg_en))
    print_v2("Set sscg_en: {}".format(e))

    value=s << 0 | r << 4 | f << 12 | e << 31
    print_v1("Set register to : 0x{:08x}".format(value))

    return value

def main(args):
    global verbose
    parser = argparse.ArgumentParser(description=__doc__, usage="%(prog)s [OPTIONS]")

    parser.add_argument(
        "mode",
        nargs='?',
        action="store",
        default='get',
        type=str,
        choices=['set','get','disable'],
        help=("Use set the specify a new spreadspectrum setting, disable to disalbe spreadspectrum clocking."),
    )
    parser.add_argument(
        "mfreq",
        nargs='?',
        action="store",
        default=0,
        type=float,
        help=("Modulation frequency used for ssc [HZ]."),
    )
    parser.add_argument(
        "mrate",
        nargs='?',
        action="store",
        default=0,
        type=float,
        help=("Modulation rate used for ssc [%%]."),
    )
    parser.add_argument(
        "-m",
        "--method",
        action="store",
        dest="method",
        default='center',
        type=str,
        choices=['down','up','center'],
        help=("Define if the where to spread the spectrum."),
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        dest="verbose",
        default=0,
        help=("Increase verbosity."),
    )
    options = parser.parse_args(args)
    verbose = options.verbose

    if ( options.mode == 'disable' or 
            ( options.mode == 'set' and ( options.mfreq == 0 or options.mrate == 0 ))):
        print("Disable Spread spectrum ")
        write_register(PLLBASE + SSCG_CTRL, encode_sscg( 0, 0, 0, 0))
        exit(0)

    pll_post_div,pll_pre_div,pll_main_div = decode_ctl0(read_register(PLLBASE + FDIV_CTL0))
    print_v1("Post Div: {}\nPre Div: {}\nMain Div: {}".format(
        pll_post_div,pll_pre_div,pll_main_div))

    m=pll_main_div
    p=pll_pre_div
    Fin=24 * pow(10,6)

    if options.mode == 'set':
        MF=options.mfreq
        print_v2("Target mfreq {}Hz".format(MF))
        MR=options.mrate / 100
        print_v2("Target mrate {}%".format(MR * 100))

        mfr=Fin/p/MF/pow(2,5)
        mrr=MR/(mfr/100/pow(2,6))
        if options.method == 'center':
            sel_pf = 0x2
        elif options.method == 'down':
            sel_pf = 0x0
        elif options.method == 'up':
            sel_pf = 0x1
        else:
            raise Exeption("Specify a valid value for --method")

        try:
            value=encode_sscg( sel_pf, mrr, mfr, 1)
        except Exception as e:
            print("Error: ", e.args)
            exit(1)
        write_register(PLLBASE + SSCG_CTRL, value )


    # Allways
    #if options.mode == 'get':
    sel_pf, pll_mrat_ctl, pll_mfreq_ctl, sscg_en = decode_sscg(read_register(PLLBASE + SSCG_CTRL))
    print_v1("Sel PF: {}\nPLL Mrat: {}\nPLL MFreq: {}\nSSCG_EN: {}".format(
    sel_pf, pll_mrat_ctl, pll_mfreq_ctl, sscg_en))

    if 0 == sscg_en:
        print("Spread spectrum disabled")
        exit(0)

    mfr=pll_mfreq_ctl
    mrr=pll_mrat_ctl
    
    MF=Fin/p/mfr/pow(2,5)
    MR=mfr*mrr/100/pow(2,6)
    if (sel_pf & 0x2) == 0x2:
        method = 'center'
    elif sel_pf == 0x0:
        method = 'down'
    elif sel_pf == 0x1:
        method = 'up'
    else:
        method = 'unknown'
            
    print("Modulation frequency: {:.2f}Hz".format(MF))
    print("Modulation range pk-pk: {}%".format(MR*100))
    print("Modulation mode: {}".format(method))


if __name__ == "__main__":
    main(sys.argv[1:])


