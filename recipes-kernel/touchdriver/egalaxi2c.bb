SUMMARY = "EETI I2C touch driver for touchscreens based on EXC3000 controller"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

require conf/seconorth-kernel-modules.inc

DEPENDS += " kernel-module-touchgpio "

SRCREV = "${AUTOREV}"
SRC_URI = " \
    git://git.seco.com/seco-ne/kernel/modules/egalaxi2c.git;protocol=https;branch=master;nobranch=1;destsuffix=src/ \
    file://Makefile \
"
