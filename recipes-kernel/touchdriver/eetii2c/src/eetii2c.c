/*
 *
 * Touch Screen I2C Driver for EETI Controller
 *
 * Copyright (C) 2000-2011  eGalax_eMPIA Technology Inc.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#define RELEASE_DATE "2013/15/04"

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <linux/kfifo.h>
#include <linux/version.h>
#include <linux/input.h>
#include <linux/irq.h>
#include <linux/timer.h>
#include <linux/proc_fs.h>
#include <linux/miscdevice.h>
#include <linux/slab.h>
#include <linux/poll.h>
#include "touch_gpio.h"
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,15)
#include <linux/pm.h>
#endif
#include "eetii2c_ts.h"

#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#endif

/* the devinit/devexit macros were removed in 3.8
   not sure why this wasn't a problem for our 3.10.17 kernel
   so I make an empty define for kernel older than 3.10.17 */
#if LINUX_VERSION_CODE > KERNEL_VERSION(3,10,17)
#define __devinit
#define __devexit
#endif

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
static struct early_suspend egalax_early_suspend;
#endif
#define MAX_EVENTS		600
#define MAX_I2C_LEN		10
#define FIFO_SIZE		8192 //(PAGE_SIZE*2)
#define MAX_SUPPORT_POINT	10
#define REPORTID_MOUSE		0x01
#define REPORTID_VENDOR		0x03
#define REPORTID_MTOUCH		0x04
#define MAX_RESOLUTION		2047

/// ioctl command ///
#define EGALAX_IOC_MAGIC	0x72
#define	EGALAX_IOCWAKEUP	_IO(EGALAX_IOC_MAGIC, 1)
#define EGALAX_IOC_MAXNR	1
#define EGALAX_ERROR_RETRIES 3

// running mode
#define MODE_STOP	0
#define MODE_WORKING	1
#define MODE_IDLE	2
#define MODE_SUSPEND	3

// controller type
#define TYPE_NONE		0
#define TYPE_7200		1
#define TYPE_7700		2
#define TYPE_UNKNOWN	3

#define VERSION_STRING_NAME_LENGTH	6

#ifndef VERSION
#define VERSION RELEASE_DATE
#endif


static const char* controller_names[TYPE_UNKNOWN+1] = {"NONE", "EXC7200", "EXC7700", "UNKNOWN"};

struct point_data {
	short Status;
	short X;
	short Y;
	short Z;
};

struct _egalax_i2c {
	struct workqueue_struct *ktouch_wq;
	struct work_struct work_irq;
	struct delayed_work delay_work_idle;
	struct delayed_work delay_work_loopback;
	struct i2c_client *client;
	unsigned char work_state;
	unsigned char skip_packet;
	unsigned char downCnt;
	unsigned char TPType;
	int error_cnt;
	int error_retries;
	bool controller_failed;
	bool idle_support_mode;
#ifdef CONFIG_HAS_EARLYSUSPEND
	bool earlysuspend_mode;
#endif
	char version_string[VERSION_STRING_NAME_LENGTH];

	struct touch_gpio * gpios;
	int irq_flags;
};

struct egalax_char_dev
{
	int OpenCnts;
	struct kfifo DataKFiFo;
	unsigned char *pFiFoBuf;
	spinlock_t FiFoLock;
	struct semaphore sem;
	wait_queue_head_t fifo_inq;
};

static struct _egalax_i2c *p_egalax_i2c_dev = NULL;	// allocated in egalax_i2c_probe
static struct egalax_char_dev *p_char_dev = NULL;	// allocated in init_module
static atomic_t egalax_char_available = ATOMIC_INIT(1);
static atomic_t wait_command_ack = ATOMIC_INIT(0);
static struct input_dev *input_dev = NULL;
static struct point_data PointBuf[MAX_SUPPORT_POINT];

#define DBG_MODULE	0x00000001
#define DBG_CDEV	0x00000002
#define DBG_PROC	0x00000004
#define DBG_POINT	0x00000008
#define DBG_INT		0x00000010
#define DBG_I2C		0x00000020
#define DBG_SUSP	0x00000040
#define DBG_INPUT	0x00000080
#define DBG_CONST	0x00000100
#define DBG_IDLE	0x00000200
#define DBG_WAKEUP	0x00000400
#define DBG_BUTTON	0x00000800
static unsigned int DbgLevel =
					DBG_MODULE|
//					DBG_CDEV  |
//					DBG_PROC  |
//					DBG_POINT |
//					DBG_INT	  |
//					DBG_I2C	  |
					DBG_SUSP  |
//					DBG_INPUT |
//					DBG_CONST |
//					DBG_IDLE  |
//					DBG_WAKEUP|
//					DBG_BUTTON|
					0 ;
module_param(DbgLevel, int, 0644);
MODULE_PARM_DESC(DbgLevel, "Debug level.");

#define PROC_FS_NAME	"eeti_dbg"
#define PROC_FS_MAX_LEN	8
static struct proc_dir_entry *dbgProcFile;

#define EGALAX_DBG(level, fmt, args...)  do{ if( (level&DbgLevel)>0 ) \
					printk( KERN_DEBUG "[egalax_i2c]: " fmt, ## args); } while(0)

#define IDLE_INTERVAL	7 // second
#define EGALAX_EDGE_INT(x) (x & (IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING))
#define EGALAX_INT_ACTIVE(x, y) ((y & (IRQF_TRIGGER_FALLING | IRQF_TRIGGER_LOW)) ? !x : x)

static bool single_touch_mode = false;
module_param(single_touch_mode, bool, 0644);
MODULE_PARM_DESC(single_touch_mode, "Simulated a single touch on a multi touch device.");
static bool enable_abs_rx_ry_rz = false;
module_param(enable_abs_rx_ry_rz, bool, 0644);
MODULE_PARM_DESC(enable_abs_rx_ry_rz, "Send a copy of the abs_mt events as abs_r[xyz].");


static int wakeup_controller(int irq)
{
	struct touch_gpio * gpios = p_egalax_i2c_dev->gpios;
	int ret=0;

	disable_irq(irq);

	touch_gpio_irq_direction_output(gpios, false);

	if( p_egalax_i2c_dev->TPType == TYPE_7700 )
		mdelay(5);
	else
		udelay(200);
	touch_gpio_irq_direction_input( gpios);

	enable_irq(irq);

	EGALAX_DBG(DBG_WAKEUP, " INT wakeup touch controller done\n");

	return ret;
}

static int egalax_cdev_open(struct inode *inode, struct file *filp)
{
	if( !atomic_dec_and_test(&egalax_char_available) )
	{
		atomic_inc(&egalax_char_available);
		return -EBUSY; // already open
	}

	p_char_dev->OpenCnts++;
	filp->private_data = p_char_dev;// Used by the read and write metheds

	if (p_egalax_i2c_dev->idle_support_mode)
	{
		// check and wakeup controller if necessary
		cancel_delayed_work_sync(&p_egalax_i2c_dev->delay_work_idle);
		if( p_egalax_i2c_dev->work_state == MODE_IDLE )
			wakeup_controller(p_egalax_i2c_dev->client->irq);
	}

	EGALAX_DBG(DBG_CDEV, " CDev open done!\n");
	try_module_get(THIS_MODULE);
	return 0;
}

static int egalax_cdev_release(struct inode *inode, struct file *filp)
{
	struct egalax_char_dev *cdev = filp->private_data;

	atomic_inc(&egalax_char_available); // release the device

	cdev->OpenCnts--;

	kfifo_reset( &cdev->DataKFiFo );

	if (p_egalax_i2c_dev->idle_support_mode)
		queue_delayed_work(p_egalax_i2c_dev->ktouch_wq, &p_egalax_i2c_dev->delay_work_idle, HZ*IDLE_INTERVAL);

	EGALAX_DBG(DBG_CDEV, " CDev release done!\n");
	module_put(THIS_MODULE);
	return 0;
}

#define MAX_READ_BUF_LEN	50
static char fifo_read_buf[MAX_READ_BUF_LEN];
static ssize_t egalax_cdev_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
	int read_cnt, ret, fifoLen;
	struct egalax_char_dev *cdev = file->private_data;

	if( down_interruptible(&cdev->sem) )
		return -ERESTARTSYS;

	fifoLen = kfifo_len(&cdev->DataKFiFo);

	while( fifoLen<1 ) // nothing to read
	{
		up(&cdev->sem); // release the lock
		if( file->f_flags & O_NONBLOCK )
			return -EAGAIN;

		if( wait_event_interruptible(cdev->fifo_inq, kfifo_len( &cdev->DataKFiFo )>0) )
		{
			return -ERESTARTSYS; // signal: tell the fs layer to handle it
		}

		if( down_interruptible(&cdev->sem) )
			return -ERESTARTSYS;
	}

	if(count > MAX_READ_BUF_LEN)
		count = MAX_READ_BUF_LEN;

	EGALAX_DBG(DBG_CDEV, " \"%s\" reading fifo data\n", current->comm);
	read_cnt = kfifo_out_locked(&cdev->DataKFiFo, fifo_read_buf, count, &cdev->FiFoLock);

	ret = copy_to_user(buf, fifo_read_buf, read_cnt)?-EFAULT:read_cnt;

	up(&cdev->sem);

	return ret;
}

static ssize_t egalax_cdev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
	struct egalax_char_dev *cdev = file->private_data;
	int ret=0;
	char *tmp;

	if( down_interruptible(&cdev->sem) )
		return -ERESTARTSYS;

	if (count > MAX_I2C_LEN)
		count = MAX_I2C_LEN;

	tmp = kzalloc(count, GFP_KERNEL);
	if(tmp==NULL)
	{
		up(&cdev->sem);
		return -ENOMEM;
	}

	if(copy_from_user(tmp, buf, count))
	{
		up(&cdev->sem);
		kfree(tmp);
		return -EFAULT;
	}

	ret = i2c_master_send(p_egalax_i2c_dev->client, tmp, count);

	up(&cdev->sem);
	EGALAX_DBG(DBG_CDEV, " I2C writing %zu bytes (%i).\n", count, ret);
	kfree(tmp);

	return ret;
}

static unsigned int egalax_cdev_poll(struct file *filp, struct poll_table_struct *wait)
{
	struct egalax_char_dev *cdev = filp->private_data;
	unsigned int mask = 0;
	int fifoLen;

	down(&cdev->sem);
	poll_wait(filp, &cdev->fifo_inq,  wait);

	fifoLen = kfifo_len(&cdev->DataKFiFo);

	if( fifoLen > 0 )
		mask |= POLLIN | POLLRDNORM;    /* readable */
	if( (FIFO_SIZE - fifoLen) > MAX_I2C_LEN )
		mask |= POLLOUT | POLLWRNORM;   /* writable */

	up(&cdev->sem);
	return mask;
}

static long egalax_cdev_ioctl(struct file *filp, unsigned int cmd, unsigned long args)
{
	int ret=0;

	if(_IOC_TYPE(cmd) != EGALAX_IOC_MAGIC)
		return -ENOTTY;
	if(_IOC_NR(cmd) > EGALAX_IOC_MAXNR)
		return -ENOTTY;

	if(_IOC_DIR(cmd) & _IOC_READ)
		ret = !access_ok((void __user*)args, _IOC_SIZE(cmd));
	else if(_IOC_DIR(cmd) & _IOC_WRITE)
		ret = !access_ok((void __user*)args, _IOC_SIZE(cmd));

	if(ret)
		return -EFAULT;

	EGALAX_DBG(DBG_CDEV, " Handle device ioctl command\n");
	switch (cmd)
	{
		case EGALAX_IOCWAKEUP:
			ret = wakeup_controller(p_egalax_i2c_dev->client->irq);
			break;
		default:
			ret = -ENOTTY;
			break;
	}

	return ret;
}

static int egalax_proc_show(struct seq_file* seqfilp, void *v)
{
	seq_printf(seqfilp, "EETI I2C for All Points.\nDebug Level: 0x%08X\nRelease Date: %s\nSECO Northern Europe Version: %s\n", DbgLevel, RELEASE_DATE, VERSION);
	return 0;
}

static int egalax_proc_open(struct inode *inode, struct file *filp)
{
	EGALAX_DBG(DBG_PROC, " \"%s\" call proc_open\n", current->comm);
	return single_open(filp, egalax_proc_show, NULL);
}

static ssize_t egalax_proc_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
	char procfs_buffer_size = 0;
	unsigned char procfs_buf[PROC_FS_MAX_LEN+1] = {0};

	EGALAX_DBG(DBG_PROC, " \"%s\" call proc_write\n", current->comm);

	procfs_buffer_size = count;
	if(procfs_buffer_size > PROC_FS_MAX_LEN )
		procfs_buffer_size = PROC_FS_MAX_LEN+1;

	if( copy_from_user(procfs_buf, buf, procfs_buffer_size) )
	{
		EGALAX_DBG(DBG_PROC, " proc_write faied at copy_from_user\n");
		return -EFAULT;
	}

	sscanf(procfs_buf, "%x", &DbgLevel);
	EGALAX_DBG(DBG_PROC, " Switch Debug Level to 0x%08X\n", DbgLevel);

	return procfs_buffer_size;
}

static void ProcessReport(unsigned char *buf, struct _egalax_i2c *p_egalax_i2c)
{
	int i, cnt_down=0, cnt_up=0;
	short X=0, Y=0, Z=0, ContactID=0, Status=0;
	bool bNeedReport = false;
	static bool touch_down;
	static int LastUpdateID = 0;

	Status = buf[1]&0x01;
	ContactID = (buf[1]&0x7C)>>2;
	X = ((buf[3]<<8) + buf[2])>>4;
	Y = ((buf[5]<<8) + buf[4])>>4;
	Z = ((buf[7]<<8) + buf[6])>>4;
	if( !(ContactID>=0 && ContactID<MAX_SUPPORT_POINT) )
	{
		EGALAX_DBG(DBG_POINT, " Get I2C Point data error [%02X][%02X][%02X][%02X][%02X][%02X]\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]);
		return;
	}

	PointBuf[ContactID].X = X;
	PointBuf[ContactID].Y = Y;
	PointBuf[ContactID].Z = Z;
	if(PointBuf[ContactID].Status!=Status)
	{
		if(Status)
			p_egalax_i2c->downCnt++;
		else if( PointBuf[ContactID].Status>0 )
			p_egalax_i2c->downCnt--;

		PointBuf[ContactID].Status = Status;
		bNeedReport = true;
	}

	EGALAX_DBG(DBG_POINT, " Get Point[%d] Update: Status=%d X=%d Y=%d Z=%d DownCnt=%d\n", ContactID, Status, X, Y, Z, p_egalax_i2c->downCnt);

	// Send point report
	if( bNeedReport || (ContactID <= LastUpdateID) )
	{

		if(PointBuf[0].Status > 0)
		{
			if( !touch_down )
			{
				touch_down = true;
				input_report_key(input_dev, BTN_TOUCH, 1);
			}
			input_report_abs(input_dev, ABS_X, PointBuf[0].X);
			input_report_abs(input_dev, ABS_Y, PointBuf[0].Y);
			input_report_abs(input_dev, ABS_PRESSURE, 1);

			if(enable_abs_rx_ry_rz)
			{
				input_report_abs(input_dev, ABS_RX, PointBuf[0].X);
				input_report_abs(input_dev, ABS_RY, PointBuf[0].Y);
				input_report_abs(input_dev, ABS_RZ, 1);
			}
		}else{
			if( touch_down )
			{
				input_report_abs (input_dev, ABS_PRESSURE, 0);
				input_report_key (input_dev, BTN_TOUCH, 0);
				if(enable_abs_rx_ry_rz)
					input_report_abs(input_dev, ABS_RZ, 0);
				touch_down = false;
			}
		}
		for(i=0; i<MAX_SUPPORT_POINT; i++)
		{
			if(PointBuf[i].Status >= 0)
			{
				if (!single_touch_mode)
				{
					if (PointBuf[i].Status)
					{
						input_report_abs(input_dev, ABS_MT_POSITION_X, PointBuf[i].X);
						input_report_abs(input_dev, ABS_MT_POSITION_Y, PointBuf[i].Y);
					} else {
						touch_down = false;
					}
					input_mt_sync(input_dev);
				}

				if(PointBuf[i].Status == 0)
				{
					PointBuf[i].Status--;
					cnt_up++;
				}
				else
				{
					cnt_down++;
				}
			}
		}

		input_sync(input_dev);
		EGALAX_DBG(DBG_POINT, " Input sync point data done! (Down:%d Up:%d)\n", cnt_down, cnt_up);
	}

	LastUpdateID = ContactID;
}

static struct input_dev * allocate_Input_Dev(struct i2c_client *client, struct _egalax_i2c *p_egalax_i2c_dev)
{
	int ret;
	struct input_dev *pInputDev=NULL;

	pInputDev = input_allocate_device();
	if(pInputDev == NULL)
	{
		EGALAX_DBG(DBG_MODULE, " Failed to allocate input device\n");
		return NULL;//-ENOMEM;
	}

	pInputDev->name = kstrdup ("eetii2c_ts", GFP_NOIO);
	pInputDev->phys = "I2C";
	pInputDev->id.bustype = BUS_I2C;
	pInputDev->id.vendor = 0x0EEF;
	pInputDev->id.product = 0x0020;
	pInputDev->id.version = 0x0001;
	pInputDev->dev.parent = &client->dev;
	input_set_drvdata (pInputDev, p_egalax_i2c_dev);

	set_bit(EV_SYN, pInputDev->evbit);
	set_bit(EV_ABS, pInputDev->evbit);
	set_bit(EV_KEY, pInputDev->evbit);
	set_bit(BTN_TOUCH, pInputDev->keybit);
	input_set_abs_params(pInputDev, ABS_PRESSURE, 0, 1, 0, 0);
	input_set_abs_params(pInputDev, ABS_X, 0, MAX_RESOLUTION, 0, 0);
	input_set_abs_params(pInputDev, ABS_Y, 0, MAX_RESOLUTION, 0, 0);
	if (!single_touch_mode)
	{
		input_set_abs_params(pInputDev, ABS_MT_POSITION_X, 0, MAX_RESOLUTION, 0, 0);
		input_set_abs_params(pInputDev, ABS_MT_POSITION_Y, 0, MAX_RESOLUTION, 0, 0);
	}
	if( enable_abs_rx_ry_rz)
	{
		input_set_abs_params(pInputDev, ABS_RX, 0, MAX_RESOLUTION, 0, 0);
		input_set_abs_params(pInputDev, ABS_RY, 0, MAX_RESOLUTION, 0, 0);
		input_set_abs_params(pInputDev, ABS_RZ, 0, MAX_RESOLUTION, 0, 0);
	}

	ret = input_register_device(pInputDev);
	if(ret)
	{
		EGALAX_DBG(DBG_MODULE, " Unable to register input device.\n");
		input_free_device(pInputDev);
		pInputDev = NULL;
	}

	return pInputDev;
}

static int egalax_i2c_measure(struct _egalax_i2c *egalax_i2c)
{
	struct i2c_client *client = egalax_i2c->client;
	u8 x_buf[MAX_I2C_LEN]={0};
	int count, loop=3, ret=0;
	static bool discard_answer = false;
	static bool name_detect = false;
	static int TPTypeBuffer = 0;

	EGALAX_DBG(DBG_INT, " egalax_i2c_measure\n");

	do{
		count = i2c_master_recv(client, x_buf, MAX_I2C_LEN);
	}while(count==-EAGAIN && --loop);

	if(count<=0)
	{
		static int cnt_err = 0; static int mod = 1000;
		cnt_err++;
		if( cnt_err < 10)
			printk(KERN_ERR "Error, while trying to receive i2c measure data (%i).\r\n", count);
		else
			if( cnt_err % mod == 0)
			{
				printk(KERN_ERR "Error, while trying to receive i2c measure data (%i) (%d times).\r\n", count, cnt_err);
				mod *= 10;
			}
		return -EIO;
	}
	else
	{
		EGALAX_DBG(DBG_I2C, " I2C read data with Len=%d\n", count);
	}

	switch(x_buf[0])
	{
		case REPORTID_MTOUCH:
			if( !egalax_i2c->skip_packet && count==MAX_I2C_LEN && egalax_i2c->work_state==MODE_WORKING)
			{
			#ifdef _IDLE_MODE_SUPPORT
				cancel_delayed_work_sync(&egalax_i2c->delay_work_idle);
			#endif

				ProcessReport(x_buf, egalax_i2c);
			}
			break;

		case REPORTID_VENDOR:
			EGALAX_DBG(DBG_I2C, " I2C get vendor command packet\n");

			if (discard_answer)
			{
				EGALAX_DBG(DBG_I2C, " Discard second answer\n");
				if (name_detect)
				{
					egalax_i2c->TPType = TPTypeBuffer;
					name_detect = false;
				}
				else
				{
					atomic_set(&wait_command_ack, 1);
					printk(KERN_INFO "[egalax_i2c]: detected device type = %d -> '%s', version: '%s'.\n", egalax_i2c->TPType,  controller_names[egalax_i2c->TPType], egalax_i2c->version_string);
				}
				discard_answer = false;
				break;
			}

			if ((x_buf[4] == 'D') && (egalax_i2c->version_string[0] == 0))
			{
				memcpy(egalax_i2c->version_string, &x_buf[5], MAX_I2C_LEN-5);
				egalax_i2c->version_string[VERSION_STRING_NAME_LENGTH-1] = '\0';
				discard_answer = true;
			}
			else if ((x_buf[4] == 'E') && (egalax_i2c->TPType == TYPE_NONE))
			{
				if(x_buf[5]=='V' && x_buf[6]=='e' && x_buf[7]=='n' && x_buf[8]=='u' && x_buf[9]=='s')
					TPTypeBuffer = TYPE_7700;
				else if(x_buf[5]=='P' && x_buf[6]=='C' && x_buf[7]=='A' && x_buf[8]=='P')
					TPTypeBuffer = TYPE_7200;
				else
					TPTypeBuffer = TYPE_UNKNOWN;
				name_detect = true;
				discard_answer = true;
			}

			if( p_char_dev->OpenCnts>0 ) // If someone reading now! put the data into the buffer!
			{
				loop=3;
				do {
					ret = wait_event_timeout(p_char_dev->fifo_inq, kfifo_avail(&p_char_dev->DataKFiFo)>=count, HZ);
				}while(ret<=0 && --loop);

				if(ret>0) // fifo size is ready
				{
					ret = kfifo_in_locked(&p_char_dev->DataKFiFo, x_buf, count, &p_char_dev->FiFoLock);

					wake_up_interruptible( &p_char_dev->fifo_inq );
				}
				else
				{
					EGALAX_DBG(DBG_CDEV, " [Warning] Can't write data because fifo size is overflow.\n");
				}
			}
			break;
		default:
			EGALAX_DBG(DBG_I2C, " I2C read error data with Len=%d header=%d\n", count, x_buf[0]);
			break;
	}

	return count;
}

static void egalax_i2c_wq_irq(struct work_struct *work)
{
	struct _egalax_i2c *egalax_i2c = container_of(work, struct _egalax_i2c, work_irq);
	struct i2c_client *client = egalax_i2c->client;

	EGALAX_DBG(DBG_INT, " egalax_i2c_wq run\n");

	if (egalax_i2c->idle_support_mode)
	{
		cancel_delayed_work_sync(&egalax_i2c->delay_work_idle);
		if(egalax_i2c->work_state == MODE_IDLE)
			egalax_i2c->work_state = MODE_WORKING;
	}

	//continue recv data
	while( touch_gpio_get_irq_state(p_egalax_i2c_dev->gpios))
	{
		egalax_i2c_measure(egalax_i2c);
		schedule();
	}

	if( egalax_i2c->skip_packet > 0 )
		egalax_i2c->skip_packet = 0;

	if (egalax_i2c->idle_support_mode)
	{
		if( p_char_dev->OpenCnts<=0 && egalax_i2c->work_state==MODE_WORKING )
			queue_delayed_work(egalax_i2c->ktouch_wq, &egalax_i2c->delay_work_idle, HZ*IDLE_INTERVAL);
	}

	if (!EGALAX_EDGE_INT(egalax_i2c->irq_flags))
		enable_irq(client->irq);

	EGALAX_DBG(DBG_INT, " egalax_i2c_wq leave\n");
}

static irqreturn_t egalax_i2c_interrupt(int irq, void *dev_id)
{
	struct _egalax_i2c *egalax_i2c = (struct _egalax_i2c *)dev_id;

	EGALAX_DBG(DBG_INT, " INT with irq:%d\n", irq);

	if (!EGALAX_EDGE_INT(egalax_i2c->irq_flags))
		disable_irq_nosync(irq);
	queue_work(egalax_i2c->ktouch_wq, &egalax_i2c->work_irq);

	return IRQ_HANDLED;
}

static void egalax_i2c_wq_idle(struct work_struct *work)
{
	struct _egalax_i2c *egalax_i2c = container_of(work, struct _egalax_i2c, delay_work_idle.work);
	unsigned char buf[] = {0x03, 0x06, 0x0A, 0x04, 0x36, 0x3F, 0x01, 0x00, 0, 0};
	int ret=0;

	if(egalax_i2c->work_state == MODE_WORKING)
	{
		ret = i2c_master_send(egalax_i2c->client, buf, MAX_I2C_LEN);
		if(ret==MAX_I2C_LEN)
		{
			egalax_i2c->work_state = MODE_IDLE;
			EGALAX_DBG(DBG_IDLE, " Set controller to idle mode\n");
		}
		else
		{
			printk(KERN_ERR "Error, while trying to send i2c data for idle mode ( %i).\r\n", ret);
		}
	}
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,15)
static int egalax_i2c_pm_suspend(struct device *dev)
{
	struct _egalax_i2c *egalax_i2c = dev_get_drvdata (dev);
#else
static int egalax_i2c_pm_suspend(struct i2c_client *client, pm_message_t mesg)
{
	struct _egalax_i2c *egalax_i2c = dev_get_drvdata (&client->dev);
#endif
	u8 cmdbuf[MAX_I2C_LEN]={0x03, 0x05, 0x0A, 0x03, 0x36, 0x3F, 0x02, 0, 0, 0};
	int ret=0;

	EGALAX_DBG(DBG_SUSP, " Enter suspend state:%d\n", p_egalax_i2c_dev->work_state);

	if(!p_egalax_i2c_dev)
		goto fail_suspend;

	cancel_delayed_work_sync(&p_egalax_i2c_dev->delay_work_loopback);

	if (egalax_i2c->idle_support_mode)
	{
		cancel_delayed_work_sync(&p_egalax_i2c_dev->delay_work_idle);

		if(p_egalax_i2c_dev->work_state == MODE_IDLE)
		{
			EGALAX_DBG((DBG_SUSP|DBG_IDLE), " Wakeup controller from idle mode before entering suspend\n");
			atomic_set(&wait_command_ack, 0);
			if( wakeup_controller(p_egalax_i2c_dev->client->irq)!=0 )
				goto fail_suspend2;

			while( !atomic_read(&wait_command_ack) )
				mdelay(2);

			EGALAX_DBG((DBG_SUSP|DBG_IDLE), " Device return to working\n");
		}
	}

	ret = i2c_master_send(p_egalax_i2c_dev->client, cmdbuf, MAX_I2C_LEN);
	if( MAX_I2C_LEN != ret)
	{
		printk(KERN_ERR "Error, while trying to send i2c data for suspend mode (%i).\r\n", ret);
		goto fail_suspend2;
	}

	disable_irq(p_egalax_i2c_dev->client->irq);

	p_egalax_i2c_dev->work_state = MODE_SUSPEND;

	EGALAX_DBG(DBG_SUSP, " egalax suspend done!!\n");
	return 0;

fail_suspend2:
	p_egalax_i2c_dev->work_state = MODE_SUSPEND;
fail_suspend:
	EGALAX_DBG(DBG_SUSP, " egalax suspend failed!!\n");
	return -1;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,15)
static int egalax_i2c_pm_resume(struct device *dev)
{
	struct _egalax_i2c *egalax_i2c = dev_get_drvdata (dev);
#else
static int egalax_i2c_pm_resume(struct i2c_client *client)
{
	struct _egalax_i2c *egalax_i2c = dev_get_drvdata (&client->dev);
#endif
	short i;
	u8 x_buf[MAX_I2C_LEN]={0};

	EGALAX_DBG(DBG_SUSP, " Enter resume state:%d\n", p_egalax_i2c_dev->work_state);

	if(!p_egalax_i2c_dev)
		goto fail_resume;

	// re-init parameter
	p_egalax_i2c_dev->downCnt=0;
	for(i=0; i<MAX_SUPPORT_POINT; i++)
	{
		PointBuf[i].Status = -1;
		PointBuf[i].X = PointBuf[i].Y = 0;
	}

	enable_irq(p_egalax_i2c_dev->client->irq);

	if( wakeup_controller(p_egalax_i2c_dev->client->irq)==0 )
		p_egalax_i2c_dev->work_state = MODE_WORKING;
	else
		goto fail_resume2;

	if (EGALAX_EDGE_INT(egalax_i2c->irq_flags))
	{
		int ret_value = 0;
		while((ret_value >= 0) && touch_gpio_get_irq_state( egalax_i2c->gpios))
		{
			ret_value = i2c_master_recv(p_egalax_i2c_dev->client, x_buf, MAX_I2C_LEN);
			if (ret_value < 0)
				printk(KERN_ERR "Error, while trying to receive i2c data while int down (%i).\r\n", ret_value);
		}
	}


	atomic_set(&wait_command_ack, 0);
	queue_delayed_work(p_egalax_i2c_dev->ktouch_wq, &p_egalax_i2c_dev->delay_work_loopback, HZ/2);

	if (egalax_i2c->idle_support_mode)
		queue_delayed_work(p_egalax_i2c_dev->ktouch_wq, &p_egalax_i2c_dev->delay_work_idle, HZ*IDLE_INTERVAL);


	EGALAX_DBG(DBG_SUSP, " egalax resume done!!\n");
	return 0;

fail_resume2:
	p_egalax_i2c_dev->work_state = MODE_WORKING;
fail_resume:
	EGALAX_DBG(DBG_SUSP, " egalax resume failed!!\n");
	return -1;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void egalax_i2c_early_suspend(struct early_suspend *handler)
{
	pm_message_t state;
	state.event = PM_EVENT_SUSPEND;

	EGALAX_DBG(DBG_SUSP, " %s\n", __func__);
	egalax_i2c_pm_suspend(p_egalax_i2c_dev->client, state);
}

static void egalax_i2c_early_resume(struct early_suspend *handler)
{
	EGALAX_DBG(DBG_SUSP, " %s\n", __func__);
	egalax_i2c_pm_resume(p_egalax_i2c_dev->client);
}
#endif

static void sendLoopback(struct i2c_client *client)
{
	struct _egalax_i2c *egalax_i2c = dev_get_drvdata (&client->dev);
	u8 cmdbuf[MAX_I2C_LEN]={0x03, 0x03, 0x0A, 0x01, 0x45, 0, 0, 0, 0, 0}; // Request controller name
	u8 cmdbuf2[MAX_I2C_LEN]={0x03, 0x03, 0x0A, 0x01, 0x44, 0, 0, 0, 0, 0}; // Request controller firmware revision
	//u8 cmdbuf3[MAX_I2C_LEN]={0x03, 0x03, 0x0A, 0x01, 0x41, 0, 0, 0, 0, 0}; // Request loop back packet
	int ret_value;

	EGALAX_DBG(DBG_MODULE, "Sending loopback command (%i).\r\n", p_egalax_i2c_dev->TPType);

	if(p_egalax_i2c_dev->TPType == TYPE_NONE)
		ret_value = i2c_master_send(client, cmdbuf, MAX_I2C_LEN);
	else
		ret_value = i2c_master_send(client, cmdbuf2, MAX_I2C_LEN);
	if (ret_value < 0)
	{
		egalax_i2c->error_cnt++;
		printk(KERN_ERR "[egalax_i2c]: Error, while trying to send loopback i2c data (%i, %i).\r\n", ret_value, egalax_i2c->error_cnt);
		if (egalax_i2c->error_cnt >= egalax_i2c->error_retries)
		{
			printk(KERN_ERR "[egalax_i2c]: Failed detecting controller.\r\n");
			atomic_set(&wait_command_ack, 1); // finishing loopback thread;
			egalax_i2c->controller_failed = true;
		}
	}
}

static void egalax_i2c_wq_loopback(struct work_struct *work)
{
	struct _egalax_i2c *egalax_i2c = container_of(work, struct _egalax_i2c, delay_work_loopback.work);

	EGALAX_DBG(DBG_MODULE, "egalax_i2c_wq_loopback (%i).\r\n", egalax_i2c->work_state);

	if(MODE_WORKING == egalax_i2c->work_state)
	{
		sendLoopback(egalax_i2c->client);

		if(!atomic_read(&wait_command_ack))
			queue_delayed_work(egalax_i2c->ktouch_wq, &egalax_i2c->delay_work_loopback, HZ);
		else
		{
			EGALAX_DBG(DBG_MODULE, " Close egalax_i2c_wq_loopback work\n");
			if (egalax_i2c->controller_failed)
			{
					EGALAX_DBG(DBG_MODULE, " Controller init failed, deinit irq and input system.\n");
					if(egalax_i2c->client->irq)
					{
						disable_irq(egalax_i2c->client->irq);
						free_irq(egalax_i2c->client->irq, egalax_i2c);
						egalax_i2c->client->irq = 0;
					}

					if(input_dev)
					{
						input_unregister_device(input_dev);
						input_dev = NULL;
					}
			}
		}
	}

	return;
}

#ifdef CONFIG_OF
static struct of_device_id egalax_i2c_dt_idtable[] = {
	{ .compatible = "eeti,eetii2c_ts", .data = 0, },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, egalax_i2c_dt_idtable);
#endif
static void remove_special_devices(void);
static void create_special_devices(void);

static int __devinit egalax_i2c_probe(struct i2c_client *client, const struct i2c_device_id *idp)
{

	int ret, i;
	struct eetii2c_ts_platform_data * pdata = (struct eetii2c_ts_platform_data *) client->dev.platform_data;
	struct device * dev = &client->dev;
	u8 x_buf[MAX_I2C_LEN]={0};


	EGALAX_DBG(DBG_MODULE, " Start probe\n");

	pr_info("[egalax_i2c]: Version %s\n", VERSION );

	p_egalax_i2c_dev = (struct _egalax_i2c *) devm_kzalloc(dev, sizeof(struct _egalax_i2c), GFP_KERNEL);
	if (!p_egalax_i2c_dev)
	{
		EGALAX_DBG(DBG_MODULE, "  Request memory failed\n");
		ret = -ENOMEM;
		goto fail1;
	}

#ifdef CONFIG_OF
	{
	struct device_node *np;
	np = client->dev.of_node;
	if( np){
		if( 0 == of_property_read_u32( np, "idle-support", &i))
			p_egalax_i2c_dev->idle_support_mode = (i == 1);

		if( 0 == of_property_read_u32_index( np, "interrupts", 1, &i))
			p_egalax_i2c_dev->irq_flags = i;

#ifdef CONFIG_HAS_EARLYSUSPEND
		if( 0 != of_property_read_u32( np, "early-syspend", &p_egalax_i2c_dev->earlysuspend_mode))
			p_egalax_i2c_dev->earlysuspend_mode = 0;
#endif
	}
	}
#endif

	p_egalax_i2c_dev->gpios = touch_gpio_read_from_dt(dev, reset_optional | wake_optional | irq_mandatory );
	if (IS_ERR(p_egalax_i2c_dev->gpios) && pdata )
	{
			p_egalax_i2c_dev->gpios = touch_gpio_set_from_pdata(dev, reset_optional | wake_optional | irq_mandatory,
					pdata->reset_pin, ACTIVE_HIGH, pdata->wake_pin, ACTIVE_HIGH,  pdata->gpio_int_number, ACTIVE_HIGH);

			client->irq = gpio_to_irq( pdata->gpio_int_number);
			p_egalax_i2c_dev->irq_flags = pdata->irq_flags;
			p_egalax_i2c_dev->idle_support_mode = pdata->idle_support_mode;
#ifdef CONFIG_HAS_EARLYSUSPEND
			p_egalax_i2c_dev->earlysuspend_mode = pdata->early_suspend_mode;
#endif
	}
	if (IS_ERR(p_egalax_i2c_dev->gpios)){
			dev_err(dev, "Probe gpios from dt and from platform data failed\n");
			return PTR_ERR(p_egalax_i2c_dev->gpios);
	}

	p_egalax_i2c_dev->error_retries = EGALAX_ERROR_RETRIES;

	/* this pulls wake high, enabling the controller */
	touch_gpio_wake_out(p_egalax_i2c_dev->gpios, true);


	if (pdata && pdata->guf_eetii2c_reset && ! pdata->guf_eetii2c_reset())
	{
		dev_err(&client->dev, "failed reseting touch controller.\n");
		ret = -ENODEV;
		goto fail3;
	}

	touch_gpio_reset_out(p_egalax_i2c_dev->gpios, true);
	msleep(100);
	touch_gpio_reset_out(p_egalax_i2c_dev->gpios, false);

	input_dev = allocate_Input_Dev(client, p_egalax_i2c_dev);
	if(input_dev==NULL)
	{
		EGALAX_DBG(DBG_MODULE, " allocate_Input_Dev failed\n");
		ret = -EINVAL;
		goto fail4;
	}
	EGALAX_DBG(DBG_MODULE, " Register input device done\n");

	create_special_devices();

	for(i=0; i<MAX_SUPPORT_POINT;i++)
	{
		PointBuf[i].Status = -1;
		PointBuf[i].X = PointBuf[i].Y = 0;
	}

	p_egalax_i2c_dev->client = client;

	p_egalax_i2c_dev->ktouch_wq = create_singlethread_workqueue("eetii2c_ts_wq");
	INIT_WORK(&p_egalax_i2c_dev->work_irq, egalax_i2c_wq_irq);
	if (p_egalax_i2c_dev->idle_support_mode)
	{
		INIT_DELAYED_WORK(&p_egalax_i2c_dev->delay_work_idle, egalax_i2c_wq_idle);
		queue_delayed_work(p_egalax_i2c_dev->ktouch_wq, &p_egalax_i2c_dev->delay_work_idle, HZ*IDLE_INTERVAL*2);
	}

	INIT_DELAYED_WORK(&p_egalax_i2c_dev->delay_work_loopback, egalax_i2c_wq_loopback);
	queue_delayed_work(p_egalax_i2c_dev->ktouch_wq, &p_egalax_i2c_dev->delay_work_loopback, HZ/2);

	i2c_set_clientdata(client, p_egalax_i2c_dev);

	p_egalax_i2c_dev->work_state = MODE_WORKING;
	p_egalax_i2c_dev->TPType = TYPE_NONE;

	if( touch_gpio_get_irq_state(p_egalax_i2c_dev->gpios))
		p_egalax_i2c_dev->skip_packet = 1;
	else
		p_egalax_i2c_dev->skip_packet = 0;

	ret = request_irq(client->irq, egalax_i2c_interrupt, p_egalax_i2c_dev->irq_flags, client->name, p_egalax_i2c_dev);
	if( ret )
	{
		EGALAX_DBG(DBG_MODULE, " Request irq(%d) failed\n", client->irq);
		goto fail5;
	}
	EGALAX_DBG(DBG_MODULE, " Request irq(%d)\n", client->irq );

	if (EGALAX_EDGE_INT(p_egalax_i2c_dev->irq_flags))
	{
		while( touch_gpio_get_irq_state(p_egalax_i2c_dev->gpios))
			i2c_master_recv(client, x_buf, MAX_I2C_LEN);
	}

#ifdef CONFIG_HAS_EARLYSUSPEND
	if (p_egalax_i2c_dev->earlysuspend_mode)
	{
		egalax_early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN;
		egalax_early_suspend.suspend = egalax_i2c_early_suspend;
		egalax_early_suspend.resume = egalax_i2c_early_resume;
		register_early_suspend(&egalax_early_suspend);
		EGALAX_DBG(DBG_MODULE, " Register early_suspend done\n");
	}
#endif
	wakeup_controller(client->irq);


	EGALAX_DBG(DBG_MODULE, " I2C probe done\n");
	return 0;

fail5:
	i2c_set_clientdata(client, NULL);
	destroy_workqueue(p_egalax_i2c_dev->ktouch_wq);
	free_irq(client->irq, p_egalax_i2c_dev);
	input_unregister_device(input_dev);
	input_dev = NULL;
fail4:
fail3:
	touch_gpio_free(p_egalax_i2c_dev->gpios);
fail1:
	devm_kfree(dev, p_egalax_i2c_dev);
	EGALAX_DBG(DBG_MODULE, " I2C probe failed\n");
	return ret;
}

static int __devexit egalax_i2c_remove(struct i2c_client *client)
{
	struct _egalax_i2c *egalax_i2c = i2c_get_clientdata(client);

	remove_special_devices();

	egalax_i2c->work_state = MODE_STOP;

	cancel_delayed_work_sync(&p_egalax_i2c_dev->delay_work_loopback);

	if (egalax_i2c->idle_support_mode)
		cancel_delayed_work_sync(&egalax_i2c->delay_work_idle);
	cancel_work_sync(&egalax_i2c->work_irq);
	if(client->irq)
	{
		disable_irq(client->irq);
		free_irq(client->irq, egalax_i2c);
	}

	if(egalax_i2c->ktouch_wq)
		destroy_workqueue(egalax_i2c->ktouch_wq);

#ifdef CONFIG_HAS_EARLYSUSPEND
	if (p_egalax_i2c_dev->earlysuspend_mode)
		unregister_early_suspend(&egalax_early_suspend);
#endif
	if(input_dev)
	{
		EGALAX_DBG(DBG_MODULE,  " Unregister input device\n");
		input_unregister_device(input_dev);
		input_dev = NULL;
	}

	touch_gpio_reset_out(p_egalax_i2c_dev->gpios, true);
	touch_gpio_free(p_egalax_i2c_dev->gpios);

	i2c_set_clientdata(client, NULL);
	return 0;
}

static struct i2c_device_id egalax_i2c_idtable[] = {
	{ "eetii2c_ts", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, egalax_i2c_idtable);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,15)
static const struct dev_pm_ops egalax_i2c_pm_ops = {
        .suspend = egalax_i2c_pm_suspend,
        .resume = egalax_i2c_pm_resume,
};
#endif

static struct i2c_driver egalax_i2c_driver = {
	.driver = {
		.name 	= "eetii2c_ts",
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,1,15)
		.pm		= &egalax_i2c_pm_ops,
#endif
#ifdef CONFIG_OF
		.of_match_table = egalax_i2c_dt_idtable,
#endif
	},
	.id_table	= egalax_i2c_idtable,
	.probe		= egalax_i2c_probe,
	.remove		= egalax_i2c_remove,
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,1,15)
#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend 	= egalax_i2c_pm_suspend,
	.resume 	= egalax_i2c_pm_resume,
#endif
#endif
};

static const struct file_operations egalax_cdev_fops = {
	.owner	= THIS_MODULE,
	.read	= egalax_cdev_read,
	.write	= egalax_cdev_write,
	.unlocked_ioctl = egalax_cdev_ioctl,
	.open	= egalax_cdev_open,
	.release= egalax_cdev_release,
	.poll	= egalax_cdev_poll,
};

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,6,0)
static const struct file_operations egalax_proc_fops = {
	.owner		= THIS_MODULE,
        .open           = egalax_proc_open,
        .read           = seq_read,
        .write          = egalax_proc_write,
        .llseek         = seq_lseek,
	.release	= single_release,
};
#else
static const struct proc_ops egalax_proc_fops = {
        .proc_open           = egalax_proc_open,
        .proc_read           = seq_read,
        .proc_write          = egalax_proc_write,
        .proc_lseek         = seq_lseek,
	.proc_release	= single_release,
};
#endif

static struct miscdevice egalax_misc_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "egalax_i2c",
	.fops = &egalax_cdev_fops,
};

static void egalax_i2c_ts_exit(void)
{

	i2c_del_driver(&egalax_i2c_driver);

	EGALAX_DBG(DBG_MODULE, " Exit driver done!\n");
}

static struct egalax_char_dev* setup_chardev(void)
{
	struct egalax_char_dev *pCharDev;

	pCharDev = kzalloc(1*sizeof(struct egalax_char_dev), GFP_KERNEL);
	if(!pCharDev)
		goto fail_cdev;

	spin_lock_init( &pCharDev->FiFoLock );
	pCharDev->pFiFoBuf = kzalloc(sizeof(unsigned char)*FIFO_SIZE, GFP_KERNEL);
	if(!pCharDev->pFiFoBuf)
		goto fail_fifobuf;

	kfifo_init(&pCharDev->DataKFiFo, pCharDev->pFiFoBuf, FIFO_SIZE);
	if( !kfifo_initialized(&pCharDev->DataKFiFo) )
		goto fail_kfifo;

	pCharDev->OpenCnts = 0;
	sema_init(&pCharDev->sem, 1);
	init_waitqueue_head(&pCharDev->fifo_inq);

	return pCharDev;

fail_kfifo:
	kfree(pCharDev->pFiFoBuf);
fail_fifobuf:
	kfree(pCharDev);
fail_cdev:
	return NULL;
}

static void remove_special_devices(void)
{
	if(p_char_dev)
	{
		if( p_char_dev->pFiFoBuf )
			kfree(p_char_dev->pFiFoBuf);

		kfree(p_char_dev);
		p_char_dev = NULL;
	}

	misc_deregister(&egalax_misc_dev);

	remove_proc_entry(PROC_FS_NAME, NULL);
	EGALAX_DBG(DBG_MODULE, " Remove special devices done!\n");
}

static void create_special_devices(void)
{
	int result;

	result = misc_register(&egalax_misc_dev);
	if(result)
	{
		EGALAX_DBG(DBG_MODULE, " misc device register failed\n");
	}

	p_char_dev = setup_chardev(); // allocate the character device
	if(!p_char_dev)
	{
		EGALAX_DBG(DBG_MODULE, " chardev device register failed\n");
	}

	dbgProcFile = proc_create(PROC_FS_NAME, S_IRUGO|S_IWUGO, NULL, &egalax_proc_fops);
	if (dbgProcFile == NULL)
	{
		remove_proc_entry(PROC_FS_NAME, NULL);
		EGALAX_DBG(DBG_MODULE, " Could not initialize /proc/%s\n", PROC_FS_NAME);
	}
	EGALAX_DBG(DBG_MODULE, " Create special devices done!\n");
}

static int egalax_i2c_ts_init(void)
{
	EGALAX_DBG(DBG_MODULE, " Driver init done!\n");
	return i2c_add_driver(&egalax_i2c_driver);
}

module_init(egalax_i2c_ts_init);
module_exit(egalax_i2c_ts_exit);

MODULE_AUTHOR("SECO Northern Europe <grimm@garz-fricke.com>");
MODULE_DESCRIPTION("egalax touch screen i2c driver");
MODULE_LICENSE("GPL");
