SUMMARY = "${PN} touch driver."

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

require conf/seconorth-kernel-modules.inc

SRCREV = "${AUTOREV}"
SRC_URI = " \
    file://Makefile \
    git://git.seco.com/seco-ne/kernel/modules/touchgpio.git;protocol=https;branch=main;nobranch=1;destsuffix=src/ \
"

do_install:append() {
    install -Dm0644 ${S}/touch_gpio.h ${D}${includedir}/
}

# Workarround, the PN has to start with kernel-module- to make the dependencies work
# But the packages split class adds another kernel-module- 
# FILES:${PN} = " /etc/* /lib/modules/* "
