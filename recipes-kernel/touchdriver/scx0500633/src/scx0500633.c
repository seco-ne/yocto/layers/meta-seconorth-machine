/* drivers/input/touchscreen/scx0500633.c
 *
 * Copyright (C) 2011 Simon Budig, <simon.budig@kernelconcepts.de>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <asm/uaccess.h>
#include <linux/delay.h>
#include <linux/slab.h>

#include <linux/gpio.h>
#include <linux/input/mt.h>
#include <linux/regulator/consumer.h>

#include "touch_gpio.h"
#include "scx0500633.h"

#ifdef CONFIG_OF
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#endif

#define DRIVER_VERSION "v0.1"

#define EXT_FIRMWARE_REGS 4
#define DBG_VERBOSE		0x00000001
#define DBG_SAMPLE 		0x00000002

#define MAX_CONTACTS 5	// There are up to 10 registers but I have only up to 5 fingers to be reported, should be enough on a 5" display anyway.
#define MAX_TRACKING_ID 0xFFFF

static struct i2c_driver scx0500633_i2c_ts_driver;
struct scx0500633_i2c_xy_data {
	u8 valid;
	unsigned int x;
	unsigned int y;
};

struct scx0500633_i2c_ts_data {
	struct i2c_client *client;
	struct input_dev *input;
	struct regulator *regulator;

	struct touch_gpio * gpios;

	int irq;

	struct mutex mutex;

	int x_resolution;
	int y_resolution;
	int page_mode;
	// sensing counter
	int timeout_idle;
	int device_control;
	int debug;

	u8 firmware_rev[EXT_FIRMWARE_REGS+1];
	unsigned int tracking_id;
	struct scx0500633_i2c_xy_data data[MAX_CONTACTS];
};


struct  scx0500633_i2c_touch_data {
	u8 fingers;
	u8 gesture;
	u8 keys;
	struct scx0500633_i2c_xy_data data[MAX_CONTACTS];
};
static bool single_touch_mode = false;
module_param(single_touch_mode, bool, 0644);
MODULE_PARM_DESC(single_touch_mode, "Simulated a single touch on a multi touch device.");


static int scx0500633_ts_readwrite (struct i2c_client *client,
                                    u16 wr_len, u8 *wr_buf,
                                    u16 rd_len, u8 *rd_buf)
{
	struct i2c_msg wrmsg[2];
	int i, ret;

	i = 0;
	if (wr_len) {
		wrmsg[i].addr  = client->addr;
		wrmsg[i].flags = 0;
		wrmsg[i].len = wr_len;
		wrmsg[i].buf = wr_buf;
		i++;
	}
	if (rd_len) {
		wrmsg[i].addr  = client->addr;
		wrmsg[i].flags = I2C_M_RD;
		wrmsg[i].len = rd_len;
		wrmsg[i].buf = rd_buf;
		i++;
	}

	ret = i2c_transfer (client->adapter, wrmsg, i);
	if (ret < 0) {
		dev_info (&client->dev, "i2c_transfer failed: %d\n", ret);
		return ret;
	}

	return ret;
}

static irqreturn_t scx0500633_ts_isr (int irq, void *dev_id)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_id;
	unsigned char wrbuf[1];
	unsigned char read_buf[2 + SCN_TOUCHPOINT_OFFSET * MAX_CONTACTS];
	struct scx0500633_i2c_touch_data touch_data;
	int ret, i;
	int x, y;
	bool reported = false;

	wrbuf[0] = SCN_REG_FINGER_GESTURE;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_ts_readwrite (tsdata->client,
	                               1, wrbuf,
	                               sizeof(read_buf), read_buf);
	mutex_unlock (&tsdata->mutex);
	if (ret < 0) {
		dev_err (&tsdata->client->dev, "Unable to communicate via i2c with touchscreen!\n");
		goto out;
	}
	if ((tsdata->debug & (DBG_SAMPLE | DBG_VERBOSE)) == (DBG_SAMPLE | DBG_VERBOSE)) 
	{
		int c = 0;
		dev_info( &tsdata->client->dev, "0x%02x 0x%02x \n",
			read_buf[c+0],read_buf[c+1]);
		c+=2;
		dev_info( &tsdata->client->dev, "0x%02x 0x%02x 0x%02x 0x%02x      0x%02x 0x%02x 0x%02x 0x%02x \n",
			read_buf[c+0],read_buf[c+1],read_buf[c+2],read_buf[c+3],
			read_buf[c+4],read_buf[c+5],read_buf[c+6],read_buf[c+7]);
		c+=8;
		dev_info( &tsdata->client->dev, "0x%02x 0x%02x 0x%02x 0x%02x      0x%02x 0x%02x 0x%02x 0x%02x \n",
			read_buf[c+0],read_buf[c+1],read_buf[c+2],read_buf[c+3],
			read_buf[c+4],read_buf[c+5],read_buf[c+6],read_buf[c+7]);
		c+=8;
		dev_info( &tsdata->client->dev, "0x%02x 0x%02x 0x%02x 0x%02x  \n",
			read_buf[c+0],read_buf[c+1],read_buf[c+2],read_buf[c+3]);
	}

	// Decode data
	touch_data.fingers = read_buf[0]       & 0x0F;
	touch_data.gesture = read_buf[0] >> 4  & 0x0F;
	touch_data.keys    = read_buf[1];
    for( i = 0; i < MAX_CONTACTS; i++)
    {
        touch_data.data[i].valid    =  read_buf[2 + i * 4] >> 7 & 0x01;
        touch_data.data[i].x        = (read_buf[2 + i * 4] >> 4 & 0x07) << 8 | read_buf[3 + i * 4];
        touch_data.data[i].y        = (read_buf[2 + i * 4] >> 0 & 0x07) << 8 | read_buf[4 + i * 4];
    }

	if (tsdata->debug & DBG_SAMPLE) 
		dev_info (&tsdata->client->dev, "scx0500633_ts_isr: f %d, g %d, k %d, |1:% 3d % 3d  % 3d |2: % 3d % 3d % 3d |3: % 3d % 3d % 3d |4: % 3d % 3d % 3d |5: % 3d % 3d % 3d \n",
		touch_data.fingers, touch_data.gesture, touch_data.keys,
		touch_data.data[0].valid, touch_data.data[0].x, touch_data.data[0].y,
		touch_data.data[1].valid, touch_data.data[1].x, touch_data.data[1].y,
		touch_data.data[2].valid, touch_data.data[2].x, touch_data.data[2].y,
		touch_data.data[3].valid, touch_data.data[3].x, touch_data.data[3].y,
		touch_data.data[4].valid, touch_data.data[4].x, touch_data.data[4].y
	   	);


	for (i = 0; i < MAX_CONTACTS; i++) {
		if (touch_data.data[i].valid)  // ignore all not valid events
		{
			bool touchdown = false;
			if( ! tsdata->data[i].valid)
			{
				tsdata->data[i].valid = true;
				touchdown = true;
			}

			x = touch_data.data[i].x;
			y = touch_data.data[i].y;

			if( ! touchdown
			 	&& x == tsdata->data[i].x 
				&& y == tsdata->data[i].y )
				continue;

			if ( 0 == i) {
				if(touchdown)
					input_report_key (tsdata->input, BTN_TOUCH, 1);

				input_report_abs (tsdata->input, ABS_X, x);
				input_report_abs (tsdata->input, ABS_Y, y);
				reported = true;
			}

			if(!single_touch_mode){

				input_mt_slot(tsdata->input, i);

				if(touchdown){
					input_report_abs (tsdata->input, ABS_MT_TRACKING_ID, tsdata->tracking_id );
					if( ++ tsdata->tracking_id > MAX_TRACKING_ID )
						tsdata->tracking_id = 0;
				}

				input_report_abs (tsdata->input, ABS_MT_POSITION_X, x);
				input_report_abs (tsdata->input, ABS_MT_POSITION_Y, y);
				reported = true;
			}
			tsdata->data[i].x = x; 
			tsdata->data[i].y = y;
			
		}
		else
		{
			if( tsdata->data[i].valid)
			{
				tsdata->data[i].valid = false;	
				tsdata->data[i].x = -1; 
				tsdata->data[i].y = -1;
				
				if ( 0 == i)
				{
					input_report_key (tsdata->input, BTN_TOUCH,    0);
					reported = true;
				}

				if(!single_touch_mode){
					input_mt_slot(tsdata->input, i);
					input_report_abs (tsdata->input, ABS_MT_TRACKING_ID, -1);
					reported = true;
				}
			}
		}
	}

	if(reported)
		input_sync (tsdata->input);

out:
	return IRQ_HANDLED;
}


static int scx0500633_i2c_register_write (struct scx0500633_i2c_ts_data *tsdata,
                                          u8 addr, u8 value)
{
	u8 wrbuf[2];
	int ret;

	wrbuf[0]  = addr;
	wrbuf[1]  = value;

	disable_irq (tsdata->irq);

	ret = scx0500633_ts_readwrite (tsdata->client,
	                               2, wrbuf,
	                               0, NULL);

	enable_irq (tsdata->irq);

	return ret;
}

static int scx0500633_i2c_register_read (struct scx0500633_i2c_ts_data *tsdata,
                                         u8 addr)
{
	u8 wrbuf[1], rdbuf[1];
	int ret;

	wrbuf[0]  = addr;

	disable_irq (tsdata->irq);

	ret = scx0500633_ts_readwrite (tsdata->client,
	                               1, wrbuf,
	                               1, rdbuf);

	enable_irq (tsdata->irq);

	dev_dbg (&tsdata->client->dev, "wr: %02x -> rd: %02x\n",
	          wrbuf[0], rdbuf[0]);

	return ret < 0 ? ret : rdbuf[0];
}

static ssize_t scn_debug_show (struct device *dev,
                                  struct device_attribute *attr,
                                  char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d\n", tsdata->debug);
}

static ssize_t scn_debug_store (struct device *dev,
                                    struct device_attribute *attr,
                                    const char *buf, size_t count)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	unsigned int debug;

	if (sscanf(buf, "%u", &debug) != 1) {
		dev_err (dev, "Invalid value for debug\n");
		return -EINVAL;
	}
	tsdata->debug = debug;
	return count;
}

static ssize_t scn_device_control_show (struct device *dev,
                                  		 struct device_attribute *attr,
                                  		 char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d\n", tsdata->timeout_idle);
}

static ssize_t scn_device_control_store (struct device *dev,
                                    	  struct device_attribute *attr,
                                    	  const char *buf, size_t count)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	int ret = 0;
	unsigned int device_control;

	if (sscanf(buf, "%u", &device_control) != 1 || (device_control > 0xff)) {
		dev_err (dev, "Invalid value for device control\n");
		return -EINVAL;
	}

	// no change, return without doing anything
	if (device_control == tsdata->device_control)
		return count;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_DEVICE_CONTROL, (u8)tsdata->device_control);
	if (ret < 0) {
		dev_err (dev, "failed to write device control (%d)\n", ret);
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	tsdata->device_control = device_control;
	mutex_unlock (&tsdata->mutex);
	return count;
}

static ssize_t scn_timeout_idle_show (struct device *dev,
                                  		 struct device_attribute *attr,
                                  		 char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d\n", tsdata->timeout_idle);
}

static ssize_t scn_timeout_idle_store (struct device *dev,
                                    	  struct device_attribute *attr,
                                    	  const char *buf, size_t count)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	int ret = 0;
	unsigned int timeout_idle;

	if (sscanf(buf, "%u", &timeout_idle) != 1 || (timeout_idle > 0xff)) {
		dev_err (dev, "Invalid value for timeout idle\n");
		return -EINVAL;
	}

	// no change, return without doing anything
	if (timeout_idle == tsdata->timeout_idle)
		return count;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_TIMEOUT_TO_IDLE, (u8)tsdata->timeout_idle);
	if (ret < 0) {
		dev_err (dev, "failed to write timeout idle (%d)\n", ret);
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	tsdata->timeout_idle = timeout_idle;
	mutex_unlock (&tsdata->mutex);
	return count;
}

static ssize_t scn_y_resolution_show (struct device *dev,
                                  		 struct device_attribute *attr,
                                  		 char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d\n", tsdata->x_resolution);
}

static ssize_t scn_y_resolution_store (struct device *dev,
                                    	  struct device_attribute *attr,
                                    	  const char *buf, size_t count)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	int ret = 0;
	unsigned int resolution;

	if (sscanf(buf, "%u", &resolution) != 1 || (resolution > 0x7ff)) {
		dev_err (dev, "Invalid value for x resolution\n");
		return -EINVAL;
	}

	// no change, return without doing anything
	if (resolution == tsdata->y_resolution)
		return count;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_XY_RESOLUTION_H, (u8)((tsdata->x_resolution & 0x700) >> 4) | (u8)(resolution >> 8));
	if (ret < 0) {
		dev_err (dev, "failed to write high x resolution (%d)\n", ret);
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_Y_RESOLUTION_L, (u8)resolution);
	if (ret < 0) {
		dev_err (dev, "failed to write low x resolution (%d)\n", ret);
		ret = scx0500633_i2c_register_write (tsdata, SCN_REG_XY_RESOLUTION_H, (u8)((tsdata->x_resolution & 0x700) >> 4) | (u8)(tsdata->y_resolution >> 8));
		if (ret < 0) {
			dev_err (dev, "failed to correcting the high x resolution (%d)\n", ret);
		}
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	tsdata->y_resolution = resolution;
	mutex_unlock (&tsdata->mutex);
	return count;
}

static ssize_t scn_x_resolution_show (struct device *dev,
                                  		 struct device_attribute *attr,
                                  		 char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d\n", tsdata->y_resolution);
}

static ssize_t scn_x_resolution_store (struct device *dev,
                                    	  struct device_attribute *attr,
                                    	  const char *buf, size_t count)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	int ret = 0;
	unsigned int resolution;

	if (sscanf(buf, "%u", &resolution) != 1 || (resolution > 0x7ff)) {
		dev_err (dev, "Invalid value for x resolution\n");
		return -EINVAL;
	}

	// no change, return without doing anything
	if (resolution == tsdata->x_resolution)
		return count;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_XY_RESOLUTION_H, (u8)((resolution & 0x700) >> 4) | (u8)(tsdata->y_resolution >> 8));
	if (ret < 0) {
		dev_err (dev, "failed to write high x resolution (%d)\n", ret);
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_X_RESOLUTION_L, (u8)resolution);
	if (ret < 0) {
		dev_err (dev, "failed to write low x resolution (%d)\n", ret);
		ret = scx0500633_i2c_register_write (tsdata, SCN_REG_XY_RESOLUTION_H, (u8)((tsdata->x_resolution & 0x700) >> 4) | (u8)(tsdata->y_resolution >> 8));
		if (ret < 0) {
			dev_err (dev, "failed to correcting the high x resolution (%d)\n", ret);
		}
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	tsdata->x_resolution = resolution;
	mutex_unlock (&tsdata->mutex);
	return count;
}

static ssize_t scn_page_mode_show (struct device *dev,
                                  		 struct device_attribute *attr,
                                  		 char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d\n", tsdata->page_mode);
}

static ssize_t scn_page_mode_store (struct device *dev,
                                    	  struct device_attribute *attr,
                                    	  const char *buf, size_t count)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	int ret = 0;
	unsigned int page_mode;

	if (sscanf(buf, "%u", &page_mode) != 1 || (page_mode > 0x1)) {
		dev_err (dev, "Invalid value for page mode\n");
		return -EINVAL;
	}

	// no change, return without doing anything
	if (page_mode == tsdata->page_mode)
		return count;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_i2c_register_write (tsdata, SCN_REG_PAGE, page_mode);
	if (ret < 0) {
		dev_err (dev, "failed to write page mode (%d)\n", ret);
		mutex_unlock (&tsdata->mutex);
		return -EIO;
	}
	tsdata->page_mode = page_mode;
	mutex_unlock (&tsdata->mutex);
	return count;
}

static ssize_t scn_device_status_show (struct device *dev,
                                  struct device_attribute *attr,
                                  char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	int ret;

	mutex_lock (&tsdata->mutex);
	ret = scx0500633_i2c_register_read(tsdata, SCN_REG_STATUS);
	if (ret < 0) {
		mutex_unlock (&tsdata->mutex);
		return ret;
	}
	mutex_unlock (&tsdata->mutex);
	return sprintf (buf, "%x\n", ret);
}

static ssize_t scn_firmware_show (struct device *dev,
                                  struct device_attribute *attr,
                                  char *buf)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (dev);
	return sprintf (buf, "%d.%d.%d.%d.%d\n",
			  tsdata->firmware_rev[0], tsdata->firmware_rev[1],
			  tsdata->firmware_rev[2], tsdata->firmware_rev[3],
			  tsdata->firmware_rev[4]);
}

static DEVICE_ATTR(firmware_rev,    0444, scn_firmware_show, NULL);
static DEVICE_ATTR(device_status,	0444, scn_device_status_show, NULL);
static DEVICE_ATTR(page_mode,  		0664, scn_page_mode_show, scn_page_mode_store);
static DEVICE_ATTR(x_resolution,  	0664, scn_x_resolution_show, scn_x_resolution_store);
static DEVICE_ATTR(y_resolution,  	0664, scn_y_resolution_show, scn_y_resolution_store);
static DEVICE_ATTR(timeout_idle,  	0664, scn_timeout_idle_show, scn_timeout_idle_store);
static DEVICE_ATTR(device_control,	0664, scn_device_control_show, scn_device_control_store);
static DEVICE_ATTR(debug,			0664, scn_debug_show, scn_debug_store);

static struct attribute *scx0500633_i2c_attrs[] = {
	&dev_attr_firmware_rev.attr,
	&dev_attr_page_mode.attr,
	&dev_attr_x_resolution.attr,
	&dev_attr_y_resolution.attr,
	&dev_attr_timeout_idle.attr,
	&dev_attr_device_control.attr,
	&dev_attr_device_status.attr,
	&dev_attr_debug.attr,
	NULL
};

static const struct attribute_group scx0500633_i2c_group = {
	.attrs = scx0500633_i2c_attrs,
};

#ifdef CONFIG_OF
static struct of_device_id scx0500633_i2c_dt_idtable[] = {
	{ .compatible = "di,scx0500633", .data = 0, },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, scx0500633_i2c_dt_idtable);

#endif

static int scx0500633_i2c_ts_probe (struct i2c_client *client,
                                    const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
	struct scx0500633_i2c_ts_data *tsdata;
	struct input_dev *input;
	int error;
	int counter;
	struct scx0500633_platform_data * pdata = dev->platform_data;
	int irq_flags = 0;

	dev_info (dev, "probing for Dataimage I2C touch\n");

	if (!client->irq) {
		dev_dbg (dev, "no IRQ?\n");
		return -ENODEV;
	}

	tsdata = devm_kzalloc (dev, sizeof (*tsdata), GFP_KERNEL);
	if (!tsdata) {
		dev_err (dev, "failed to allocate driver data!\n");
		return -ENOMEM;
	}

	tsdata->gpios = touch_gpio_read_from_dt(dev, reset_optional | wake_optional | irq_optional );
	if (IS_ERR(tsdata->gpios) && pdata) {
			tsdata->gpios = touch_gpio_set_from_pdata(dev, reset_mandatory | wake_mandatory | irq_mandatory ,
					pdata->reset_pin, ACTIVE_LOW, pdata->wake_pin, ACTIVE_HIGH, pdata->irq_pin, ACTIVE_LOW);
			irq_flags = IRQF_TRIGGER_FALLING;
	}
	if (IS_ERR(tsdata->gpios)){
			dev_err(dev, "Probe gpios from dt and from platform data failed\n");
			error = PTR_ERR(tsdata->gpios);
			goto err_free_tsdata;
	}


	tsdata->irq = client->irq;
	tsdata->client = client;
	tsdata->regulator = devm_regulator_get(dev, "vin");
	if(IS_ERR(tsdata->regulator))
	{
		dev_info(dev, "Regulator \"vin\" not found. Try to go on");
		tsdata->regulator = NULL;
	} else {
		error = regulator_enable(tsdata->regulator);
		if (error)
			goto err_free_tsdata;
	}

	mutex_init (&tsdata->mutex);

	/* In ST1633 a reset of min 5ms is required. Be conservative  */
	touch_gpio_do_reset(tsdata->gpios, 6000); // do a reset of 6 ms anyway

	/* this pulls wake high, enabling the controller */
	touch_gpio_wake_out(tsdata->gpios, true);

	/* From TST421GGU-01C data sheet: max 65ms, be conservative */	
	msleep_interruptible(70);

	mutex_lock (&tsdata->mutex);

	error = scx0500633_i2c_register_read (tsdata,  SCN_REG_FIRMWARE_VERSION);
	if (error < 0) {
		dev_err (dev, "failed reading firmware version\n");
		error = -ENODEV;
		goto err_free_gpios;
	}
	tsdata->firmware_rev[0] = (u8) error;
	for (counter = 0; counter < EXT_FIRMWARE_REGS; counter++) {
		error = scx0500633_i2c_register_read (tsdata,  SCN_REG_FIRMWARE_REV_3+counter);
		if (error < 0) {
			dev_err (dev, "failed reading extended firmware version\n");
			error = -ENODEV;
			goto err_free_gpios;
		} else {
			tsdata->firmware_rev[counter+1] = (u8) error;
		}
	}

	error = scx0500633_i2c_register_read (tsdata, SCN_REG_XY_RESOLUTION_H);
	if (error < 0) {
		dev_err (dev, "failed reading high XY resolution.\n");
		goto err_free_gpios;
	}
	tsdata->x_resolution = (((error & SCN_MASK_XY_RES_X_RES_H) >> SCN_SHIFT_XY_RES_X_RES_H) << 8);
	tsdata->y_resolution = ((error & SCN_MASK_XY_RES_Y_RES_H) << 8);
	error = scx0500633_i2c_register_read (tsdata, SCN_REG_X_RESOLUTION_L);
	if (error < 0) {
		dev_err (dev, "failed reading low X resolution.\n");
		goto err_free_gpios;
	}
	tsdata->x_resolution |= error;
	error = scx0500633_i2c_register_read (tsdata, SCN_REG_Y_RESOLUTION_L);
	if (error < 0) {
		dev_err (dev, "failed reading low Y resolution.\n");
		goto err_free_gpios;
	}
	tsdata->y_resolution |= error;

	tsdata->page_mode = scx0500633_i2c_register_read (tsdata, SCN_REG_PAGE);
	if (tsdata->page_mode < 0) {
		dev_err (dev, "failed reading page mode.\n");
		goto err_free_gpios;
	}
	tsdata->timeout_idle = scx0500633_i2c_register_read (tsdata, SCN_REG_TIMEOUT_TO_IDLE);
	if (tsdata->timeout_idle < 0) {
		dev_err (dev, "failed reading timeout idle.\n");
		goto err_free_gpios;
	}
	error = scx0500633_i2c_register_write (tsdata, SCN_REG_DEVICE_CONTROL, 0xC0);
	if (error < 0) {
		dev_err (dev, "failed to write control reg (%d)\n", error);
		goto err_free_gpios;
	}
	tsdata->device_control = scx0500633_i2c_register_read (tsdata, SCN_REG_DEVICE_CONTROL);
	if (tsdata->device_control < 0) {
		dev_err (dev, "failed reading device control.\n");
		goto err_free_gpios;
	}
	mutex_unlock (&tsdata->mutex);

	dev_info (dev, "Model Dataimage SCX0500633, rev %d.%d.%d.%d.%d\n",
			  tsdata->firmware_rev[0], tsdata->firmware_rev[1],
			  tsdata->firmware_rev[2], tsdata->firmware_rev[3],
			  tsdata->firmware_rev[4]);


	input = devm_input_allocate_device (dev);
	if (!input) {
		dev_err (dev, "failed to allocate input device!\n");
		error = -ENOMEM;
		goto err_free_gpios;
	}

	set_bit (EV_SYN, input->evbit);
	set_bit (EV_KEY, input->evbit);
	set_bit (EV_ABS, input->evbit);
	set_bit (BTN_TOUCH, input->keybit);
	input_set_abs_params (input, ABS_X, 0, tsdata->x_resolution - 1, 0, 0);
	input_set_abs_params (input, ABS_Y, 0, tsdata->y_resolution - 1, 0, 0);
	if(!single_touch_mode){
		input_set_abs_params (input, ABS_MT_POSITION_X, 0, tsdata->x_resolution - 1, 0, 0);
		input_set_abs_params (input, ABS_MT_POSITION_Y, 0, tsdata->y_resolution - 1, 0, 0);
		input_mt_init_slots(input, MAX_CONTACTS, INPUT_MT_DIRECT);
	}
	dev_info (dev, "ABS_X = %d, ABS_Y = %d\n",  tsdata->x_resolution - 1, tsdata->y_resolution - 1);

	input->name = client->name;
	input->id.bustype = BUS_I2C;
	
	input_set_drvdata (input, tsdata);
	tsdata->input = input;

	if (devm_request_threaded_irq (dev, tsdata->irq, NULL, scx0500633_ts_isr,
	                          irq_flags | IRQF_ONESHOT,
	                          client->name, tsdata)) {
		dev_err (dev, "Unable to request touchscreen IRQ.\n");
		input = NULL;
		error = -ENOMEM;
		goto err_unregister_device;
	}


	error = sysfs_create_group (&dev->kobj, &scx0500633_i2c_group);
	if (error)
		goto err_free_input_device;

	if ((error = input_register_device (input)))
		goto err_free_input_device;
	
	i2c_set_clientdata (client, tsdata);
	device_init_wakeup (dev, 1);

	dev_info (dev, "Dataimage SCX0500633 initialized.\n");
	return 0;

err_free_input_device:
	sysfs_remove_group(&dev->kobj, &scx0500633_i2c_group);
err_unregister_device:
err_free_gpios:
	touch_gpio_free(tsdata->gpios);
err_free_tsdata:
	if(tsdata->regulator && !IS_ERR(tsdata->regulator))
        regulator_disable(tsdata->regulator);
	return error;
}

static int scx0500633_i2c_ts_remove (struct i2c_client *client)
{
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (&client->dev);

	input_unregister_device (tsdata->input);
	sysfs_remove_group(&client->dev.kobj, &scx0500633_i2c_group);
	touch_gpio_free(tsdata->gpios);
	if(tsdata->regulator && !IS_ERR(tsdata->regulator))
        regulator_disable(tsdata->regulator);
	return 0;
}

static int scx0500633_i2c_ts_suspend (struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (&client->dev);

	if (device_may_wakeup (&client->dev))
		enable_irq_wake (tsdata->irq);

	return 0;
}

static int scx0500633_i2c_ts_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct scx0500633_i2c_ts_data *tsdata = dev_get_drvdata (&client->dev);

	if (device_may_wakeup (&client->dev))
		disable_irq_wake (tsdata->irq);

	return 0;
}

static const struct i2c_device_id scx0500633_i2c_ts_id[] =
{
	{ "scx-0500633", 0 },
	{ }
};
MODULE_DEVICE_TABLE (i2c, scx0500633_i2c_ts_id);

static SIMPLE_DEV_PM_OPS(scx0500633_ts_pm_ops,
			 scx0500633_i2c_ts_suspend, scx0500633_i2c_ts_resume);

static struct i2c_driver scx0500633_i2c_ts_driver =
{
	.driver = {
		.owner = THIS_MODULE,
		.name = "scx0500633",
#ifdef CONFIG_OF
		.of_match_table = scx0500633_i2c_dt_idtable,
#endif
		.pm = &scx0500633_ts_pm_ops,
	},
	.id_table = scx0500633_i2c_ts_id,
	.probe    = scx0500633_i2c_ts_probe,
	.remove   = scx0500633_i2c_ts_remove,
};

static int __init scx0500633_i2c_ts_init (void)
{
	return i2c_add_driver (&scx0500633_i2c_ts_driver);
}
module_init (scx0500633_i2c_ts_init);

static void __exit scx0500633_i2c_ts_exit (void)
{
	i2c_del_driver (&scx0500633_i2c_ts_driver);
}
module_exit (scx0500633_i2c_ts_exit);


MODULE_AUTHOR ("Jonas Hoeppner <jonas.hoeppner@garz-fricke.com>");
MODULE_DESCRIPTION ("Dataimage SCX0500633 I2C Touchscreen Driver");
MODULE_LICENSE ("GPL");
