#ifndef _DATAIMAGE_SCX0500633_H
#define _DATAIMAGE_SCX0500633_H

/*
 * Copyright (c) 2011 Simon Budig, <simon.budig@kernelconcepts.de>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

#define GF_DATAIMAGE_SCT0500133_ADDRESS (0x55)

struct scx0500633_platform_data {
	unsigned int reset_pin;
	unsigned int wake_pin;
	unsigned int irq_pin;
};

#define SCN_REG_FIRMWARE_VERSION					0x00
#define SCN_REG_STATUS								0x01
#define SCN_MASK_STATUS_ERROR_CODE					0xf0
#define SCN_SHIFT_STATUS_ERROR_CODE					0x04
#define SCN_STATUS_ERR_NO_ERROR						0x00
#define SCN_STATUS_ERR_INVALID_ADDRESS				0x01
#define SCN_STATUS_ERR_INVALID_VALUE				0x02
#define SCN_STATUS_ERR_INVALID_PLATFORM				0x03
#define SCN_MASK_STATUS_DEVICE_STATUS				0x0f
#define SCN_STATUS_STAT_NORMAL						0x00
#define SCN_STATUS_STAT_INIT						0x01
#define SCN_STATUS_STAT_ERROR						0x02
#define SCN_STATUS_STAT_AUTO_TUNING					0x03
#define SCN_STATUS_STAT_IDLE						0x04
#define SCN_STATUS_STAT_POWER_DOWN					0x05

#define SCN_REG_DEVICE_CONTROL						0x02
#define SCN_DEV_CTRL_AUTO_TUNE						0x80
#define SCN_DEV_CTRL_FLASH_UPDATE_DISABLED			0x40
#define SCN_DEV_CTRL_GESTURE_ENABLE					0x08
#define SCN_DEV_CTRL_PROXIMITY_ENABLE				0x04
#define SCN_DEV_CTRL_POWER_DOWN						0x02
#define SCN_DEV_CTRL_RESET							0x01

#define SCN_REG_TIMEOUT_TO_IDLE						0x03
#define SCN_REG_XY_RESOLUTION_H						0x04
#define SCN_MASK_XY_RES_X_RES_H						0xf0
#define SCN_SHIFT_XY_RES_X_RES_H					0x04
#define SCN_MASK_XY_RES_Y_RES_H						0x0f

#define SCN_REG_X_RESOLUTION_L						0x05
#define SCN_REG_Y_RESOLUTION_L						0x06
#define SCN_REG_FIRMWARE_REV_3						0x0c
#define SCN_REG_FIRMWARE_REV_2						0x0d
#define SCN_REG_FIRMWARE_REV_1						0x0e
#define SCN_REG_FIRMWARE_REV_0						0x0f
#define SCN_REG_FINGER_GESTURE						0x10
#define SCN_MASK_F_GEST_GESTURE_CODE				0xf8
#define SCN_SHIFT_F_GEST_GESTURE_CODE				0x03
#define SCN_F_GEST_NO_DETECTED						0x00
#define SCN_F_GEST_SINGLE_TOUCH_TAP					0x01
#define SCN_F_GEST_SINGLE_TOUCH_DOUBLE_TAP			0x02
#define SCN_F_GEST_SINGLE_TOUCH_SLIDE_UP			0x03
#define SCN_F_GEST_SINGLE_TOUCH_SLIDE_DOWN			0x04
#define SCN_F_GEST_SINGLE_TOUCH_SLIDE_LEFT			0x05
#define SCN_F_GEST_SINGLE_TOUCH_SLIDE_RIGHT			0x06
#define SCN_F_GEST_TWO_TOUCH_SLIDE_UP				0x07
#define SCN_F_GEST_TWO_TOUCH_SLIDE_DOWN				0x08
#define SCN_F_GEST_TWO_TOUCH_SLIDE_LEFT				0x09
#define SCN_F_GEST_TWO_TOUCH_SLIDE_RIGHT			0x0a
#define SCN_F_GEST_PINCH_IN							0x0b
#define SCN_F_GEST_PINCH_OUT						0x0c
#define SCN_F_GEST_ROTATE_CW						0x0d
#define SCN_F_GEST_ROTATE_CCW						0x0e
#define SCN_F_GEST_OBJECT_APPROACHING				0x0f
#define SCN_F_GEST_OBJECT_LEAVING					0x10
#define SCN_MASK_F_GEST_FINGERS						0x07

#define SCN_REG_KEYS								0x11

#define SCN_REG_FIRST_TOUCHPOINT					0x12
#define SCN_TOUCHPOINT_OFFSET						0x04
#define SCN_MAX_TOUCHPOINT							10

#define SCN_OFFSET_XY0_H							0x0
#define SCN_MASK_XY0_H_X0_H							0x70
#define SCN_SHIFT_XY0_H_X0_H						0x04
#define SCN_MASK_XY0_H_Y0_H							0x07
#define SCN_MASK_XY0_H_VALID_0						0x80
#define SCN_OFFSET_X0_L								0x1
#define SCN_OFFSET_Y0_L								0x2
// rserved 0x1A ... 0x3e
#define SCN_REG_MAX_SUPPORTED_CONTACTS				0x40
// rserved 0x40 ... 0xFE
#define SCN_REG_PAGE								0xff
#define SCN_PAGE_REPORT_PAGE						0x00
#define SCN_PAGE_AUTO_TUNE_PAGE						0x01


#endif /* _DATAIMAGE_SCX0500633_H */
