/*
 * Touch Screen driver for Himax HX85XX controllers
 *
 * Copyright (C) 2017 SECO Northern Europe GmbH. All Rights Reserved.
 *
 * This driver is based on sis_i2c.c
 *
 * Copyright (C) 2015 SiS, Inc.
 *
 * This driver is also based on pixcir_i2c_ts.c
 *
 * Copyright (C) 2010-2011 Pixcir, Inc.
 *
 * This driver is also based on Himax_android_driver_sample.c
 *
 * Copyright (C) 2012 Himax Corporation.
 *
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/crc-itu-t.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/input/mt.h>
#include <linux/interrupt.h>
#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <asm/unaligned.h>
#include <linux/of_gpio.h>
#include "touch_gpio.h"

#define HIMAX_I2C_NAME        "himax_i2c_ts"

#define HIMAX_MAX_MSG_SIZE        2

/* Touchscreen parameters */
#define HIMAX_MAX_FINGERS            5
#define HIMAX_MAX_PRESSURE        0xff

//------------------------------------------// Himax TP COMMANDS -> Do not modify the below definition
#define HX_CMD_NOP                   0x00   /* no operation */
#define HX_CMD_SETMICROOFF           0x35   /* set micro on */
#define HX_CMD_SETROMRDY             0x36   /* set flash ready */
#define HX_CMD_TSSLPIN               0x80   /* set sleep in */
#define HX_CMD_TSSLPOUT              0x81   /* set sleep out */
#define HX_CMD_TSSOFF                0x82   /* sense off */
#define HX_CMD_TSSON                 0x83   /* sense on */
#define HX_CMD_ROE                   0x85   /* read one event */
#define HX_CMD_RAE                   0x86   /* read all events */
#define HX_CMD_RLE                   0x87   /* read latest event */
#define HX_CMD_CLRES                 0x88   /* clear event stack */
#define HX_CMD_TSSWRESET             0x9E   /* TS software reset */
#define HX_CMD_SETDEEPSTB            0xD7   /* set deep sleep mode */
#define HX_CMD_SET_CACHE_FUN         0xDD   /* set cache function */
#define HX_CMD_SETIDLE               0xF2   /* set idle mode */
#define HX_CMD_SETIDLEDELAY          0xF3   /* set idle delay */
#define HX_CMD_SELFTEST_BUFFER       0x8D   /* Self-test return buffer */
#define HX_CMD_MANUALMODE            0x42
#define HX_CMD_FLASH_ENABLE          0x43
#define HX_CMD_FLASH_SET_ADDRESS     0x44
#define HX_CMD_FLASH_WRITE_REGISTER  0x45
#define HX_CMD_FLASH_SET_COMMAND     0x47
#define HX_CMD_FLASH_WRITE_BUFFER    0x48
#define HX_CMD_FLASH_PAGE_ERASE      0x4D
#define HX_CMD_FLASH_SECTOR_ERASE    0x4E
#define HX_CMD_CB                    0xCB
#define HX_CMD_EA                    0xEA
#define HX_CMD_4A                    0x4A
#define HX_CMD_4F                    0x4F
#define HX_CMD_B9                    0xB9
#define HX_CMD_76                    0x76


static int                HX_TOUCH_INFO_POINT_CNT                        = 0;


#define DEFAULT_RETRY_CNT            3            // For I2C Retry count

struct himax_touch{
    int x;
    int y;

    int id;

    int pressure;
    int area;

    bool touch_down;
};

struct himax_ts_platform_data {
    unsigned int x_max;
    unsigned int y_max;

    struct touch_gpio * gpios;
};

struct himax_ts_data {
    struct i2c_client *client;
    struct input_dev *input;

    const struct himax_ts_platform_data *pdata;

    struct i2c_msg msg[HIMAX_MAX_MSG_SIZE];

    struct mutex mutex_lock;
};


static struct himax_ts_platform_data *himax_parse_dt(struct device *dev)
{
    struct himax_ts_platform_data *pdata;
    struct device_node *np = dev->of_node;
    int error;

    pdata = devm_kzalloc(dev, sizeof(*pdata), GFP_KERNEL);
    if (!pdata){
        return ERR_PTR(-ENOMEM);
    }

    pdata->gpios = touch_gpio_read_from_dt(dev, reset_mandatory | wake_optional | irq_mandatory );
    if (IS_ERR(pdata->gpios)){
            dev_err(dev, "Probe gpios from dt and from platform data failed\n");
            error = PTR_ERR(pdata->gpios);
            return ERR_PTR(error);
    }

    if (of_property_read_u32(np, "touchscreen-size-x", &pdata->x_max)) {
        dev_err(dev, "Failed to get touchscreen-size-x property\n");
        return ERR_PTR(-EINVAL);
    }
    pdata->x_max -= 1;

    if (of_property_read_u32(np, "touchscreen-size-y", &pdata->y_max)) {
        dev_err(dev, "Failed to get touchscreen-size-y property\n");
        return ERR_PTR(-EINVAL);
    }
    pdata->y_max -= 1;

    return pdata;
}

void calculate_touch_info_point_calc_address(void)
{
    HX_TOUCH_INFO_POINT_CNT = HIMAX_MAX_FINGERS * 4 ;

    if( (HIMAX_MAX_FINGERS % 4) == 0)
    {
        HX_TOUCH_INFO_POINT_CNT += (HIMAX_MAX_FINGERS / 4) * 4 ;
    }
    else
    {
        HX_TOUCH_INFO_POINT_CNT += ((HIMAX_MAX_FINGERS / 4) +1) * 4 ;
    }
}    

static void himax_ts_reset(struct himax_ts_data *ts)
{
    mutex_lock(&ts->mutex_lock);
    /* Get out of reset */
    msleep(100);
    touch_gpio_reset_out(ts->pdata->gpios, true);
    msleep(100);
    touch_gpio_reset_out(ts->pdata->gpios, false);
    msleep(100);

    mutex_unlock(&ts->mutex_lock);
}

static int i2c_himax_read(struct i2c_client *client, uint8_t command, uint8_t *data, uint8_t length, uint8_t toRetry)
{
    int retry;
    struct i2c_msg msg[] = 
    {
        {
            .addr = client->addr,
            .flags = 0,
            .len = 1,
            .buf = &command,
        },
        {
            .addr = client->addr,
            .flags = I2C_M_RD,
            .len = length,
            .buf = data,
        }
    };

    struct himax_ts_data* ts = i2c_get_clientdata(client); 

    mutex_lock(&ts->mutex_lock);

    for (retry = 0; retry < toRetry; retry++) 
    {
        if (i2c_transfer(client->adapter, msg, 2) == 2)
        {
            break;
        }
        msleep(10);
    }


    mutex_unlock(&ts->mutex_lock);

    if (retry == toRetry) 
    {
        printk(KERN_INFO "[TP] %s: i2c_read_block retry over %d\n", __func__, toRetry);
        return -EIO;
    }
    return 0;
}


static int i2c_himax_write(struct i2c_client *client, uint8_t command, uint8_t *data, uint8_t length, uint8_t toRetry)
{
    int retry, loop_i;
    uint8_t *buf = kzalloc(sizeof(uint8_t)*(length+1), GFP_KERNEL);

    struct i2c_msg msg[] = 
    {
        {
            .addr = client->addr,
            .flags = 0,
            .len = length + 1,
            .buf = buf,
        }
    };

    struct himax_ts_data* ts = i2c_get_clientdata(client); 

    mutex_lock(&ts->mutex_lock);

    buf[0] = command;
    for (loop_i = 0; loop_i < length; loop_i++)
    {
        buf[loop_i + 1] = data[loop_i];
    }
    for (retry = 0; retry < toRetry; retry++) 
    {
        if (i2c_transfer(client->adapter, msg, 1) == 1)
        {
            break;
        }
        msleep(10);
    }

    mutex_unlock(&ts->mutex_lock);

    if (retry == toRetry) 
    {
        printk(KERN_ERR "[TP] %s: i2c_write_block retry over %d\n", __func__, toRetry);
        kfree(buf);
        return -EIO;
    }
    kfree(buf);
    return 0;
}

static int __maybe_unused i2c_himax_write_command(struct i2c_client *client, uint8_t command, uint8_t toRetry)
{
    return i2c_himax_write(client, command, NULL, 0, toRetry);
}

int i2c_himax_master_write(struct i2c_client *client, uint8_t *data, uint8_t length, uint8_t toRetry)
{
    int retry, loop_i;
    uint8_t *buf = kzalloc(sizeof(uint8_t)*length, GFP_KERNEL);

    struct i2c_msg msg[] = 
    {
        {
            .addr = client->addr,
            .flags = 0,
            .len = length,
            .buf = buf,
        }
    };

    struct himax_ts_data* ts = i2c_get_clientdata(client); 

    mutex_lock(&ts->mutex_lock);

    for (loop_i = 0; loop_i < length; loop_i++)
    {
        buf[loop_i] = data[loop_i];
    }
    for (retry = 0; retry < toRetry; retry++) 
    {
        if (i2c_transfer(client->adapter, msg, 1) == 1)
        {
            break;
        }
        msleep(10);
    }

    mutex_unlock(&ts->mutex_lock);

    if (retry == toRetry) 
    {
        printk(KERN_ERR "[TP] %s: i2c_write_block retry over %d\n", __func__, toRetry);
        kfree(buf);
        return -EIO;
    }
    kfree(buf);
    return 0;
}


static int himax_read_packet(struct i2c_client *client, struct i2c_msg *msg,
        struct himax_ts_data *ts, struct himax_touch *touches)
{
    int ret;

    uint8_t buf[128] = {0};

    unsigned char check_sum_cal = 0;
    unsigned int num_contacts;

    int raw_cnt_max = HIMAX_MAX_FINGERS/4;
    int raw_cnt_rmd = HIMAX_MAX_FINGERS%4;

    int  i;

    unsigned int x=0, y=0, area=0, press=0;
    const unsigned int x_res = ts->pdata->x_max;
    const unsigned int y_res = ts->pdata->y_max;

    int hx_touch_info_size = 128;

    if(raw_cnt_rmd != 0x00) //more than 4 fingers
    {
        hx_touch_info_size = (HIMAX_MAX_FINGERS+raw_cnt_max+2)*4;
    }
    else //less than 4 fingers
    {
        hx_touch_info_size = (HIMAX_MAX_FINGERS+raw_cnt_max+1)*4;
    }

    ret = i2c_himax_read(client, HX_CMD_RAE, buf, hx_touch_info_size, 3);

    if(ret){
        dev_err(&client->dev, "i2c error");
        return -EIO;
    }    

    for(i = 0; i <msg[1].len; i++)
    {
        check_sum_cal += buf[i];
    }

    if (check_sum_cal != 0x00)
    {
        dev_err(&client->dev, "Checksum error");
        return -EIO;
    }
    if((buf[HX_TOUCH_INFO_POINT_CNT] & 0xF0 )!= 0xF0)
    {
        dev_err(&client->dev, "HX_TOUCH_INFO_POINT_CNT Error");
        return -EIO;
    }

    if(buf[HX_TOUCH_INFO_POINT_CNT] == 0xff)
    {
        num_contacts = 0;
    }
    else
    {
        num_contacts = buf[HX_TOUCH_INFO_POINT_CNT] & 0x0f;
    }

    // Touch Point information
    if(num_contacts != 0)
    {
        // parse the point information
        for(i=0; i<HIMAX_MAX_FINGERS; i++)
        {
            if(buf[4*i] != 0xFF)
            {
                // x and y axis
                x = buf[4 * i + 1] | (buf[4 * i] << 8) ;
                y = buf[4 * i + 3] | (buf[4 * i + 2] << 8);

                if(x > x_res){
                    x = x_res;
                }
                if(y > y_res)
                {
                    y = y_res;
                }
                press = buf[4*HIMAX_MAX_FINGERS+i];
                area = press;
                area = (area >> 3);

                touches[i].x = x;
                touches[i].y = y;
                touches[i].id = i;
                touches[i].touch_down = true;
                touches[i].pressure = press;
                touches[i].area = area;
            }
            else
            {
                touches[i].x = 0xFFFF;
                touches[i].y = 0xFFFF;
                touches[i].id = i;
                touches[i].touch_down = false;
                touches[i].pressure = 0;
                touches[i].area = 0;
            }
        }
    }
    else if(num_contacts == 0 )
    {
        for(i=0; i<HIMAX_MAX_FINGERS; i++)
        {
            touches[i].x = 0xFFFF;
            touches[i].y = 0xFFFF;
            touches[i].id = i;
            touches[i].touch_down = false;
            touches[i].pressure = 0;
            touches[i].area = 0;
        }
    }
    return 0;
}

static int himax_ts_report_contact(struct himax_ts_data *ts, struct himax_touch *touches)
{
    int slot;
    int i;

    for(i = 0 ; i < HIMAX_MAX_FINGERS ; i++){

        slot = input_mt_get_slot_by_key(ts->input, touches[i].id);

        if (slot < 0) {
            dev_dbg(&ts->client->dev, "no free slot for id 0x%x\n",
                    touches[i].id);
            continue;
        }

        input_mt_slot(ts->input, slot);

        if(touches[i].touch_down){

            input_mt_report_slot_state(ts->input,
                    MT_TOOL_FINGER, true);

            input_event(ts->input, EV_ABS, ABS_MT_POSITION_Y, touches[i].y);
            input_event(ts->input, EV_ABS, ABS_MT_POSITION_X, touches[i].x);
            input_event(ts->input, EV_ABS, ABS_MT_TRACKING_ID, touches[i].id); //ID of touched point
            input_event(ts->input, EV_ABS, ABS_MT_TOUCH_MAJOR, touches[i].area); //Touch Size
            input_event(ts->input, EV_ABS, ABS_MT_PRESSURE, touches[i].pressure); // Pressure
        } else{
            input_mt_report_slot_state(ts->input,
                    MT_TOOL_FINGER, false);
        }

    }
    input_mt_sync_frame(ts->input);
    input_sync(ts->input);
    return 0;
}

static void himax_ts_handle_packet(struct himax_ts_data *ts)
{
    int error;    
    struct himax_touch touches[HIMAX_MAX_FINGERS];

    error = himax_read_packet(ts->client, ts->msg,
            ts, touches);
    if (error)
    {    
        dev_err(&ts->client->dev, "Error reading packet = %d\n", error);
    }

    himax_ts_report_contact(ts, touches);
}

void __maybe_unused himaxGetVersionID(struct i2c_client* client){

    uint8_t cmd[3];
    uint8_t data[3];

    if( i2c_himax_read(client, 0xD1, cmd, 3, DEFAULT_RETRY_CNT) < 0)
    {
        return ;
    }

    if( i2c_himax_read(client, 0x31, data, 3, DEFAULT_RETRY_CNT) < 0)
    {
        return ;
    }

    dev_dbg(&client->dev,
            "VersionID cmd = %d | %d | %d \n",
            cmd[0], cmd[1], cmd[2]);

    dev_dbg(&client->dev,
            "VersionID data = %d | %d | %d \n",
            data[0], data[1], data[2]);
}


static int himax_ts_poweron(struct himax_ts_data *ts)
{
    uint8_t buf0[11];
    int ret = 0;

    buf0[0] = HX_CMD_MANUALMODE;
    buf0[1] = 0x02;
    ret = i2c_himax_master_write(ts->client, buf0, 2, DEFAULT_RETRY_CNT);//Reload Disable
    if(ret < 0) 
    {
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        goto send_i2c_msg_fail;
    }
    udelay(100);

    buf0[0] = HX_CMD_SETMICROOFF;
    buf0[1] = 0x02;
    ret = i2c_himax_master_write(ts->client, buf0, 2, DEFAULT_RETRY_CNT);//Reload Disable
    if(ret < 0) 
    {
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        goto send_i2c_msg_fail;
    }
    udelay(100);   

    buf0[0] = HX_CMD_SETROMRDY;
    buf0[1] = 0x0F;
    buf0[2] = 0x53;
    ret = i2c_himax_master_write(ts->client, buf0, 3, DEFAULT_RETRY_CNT);//enable flash
    if(ret < 0) 
    {
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        goto send_i2c_msg_fail;
    } 
    udelay(100);

    buf0[0] = HX_CMD_SET_CACHE_FUN;
    buf0[1] = 0x06;
    buf0[2] = 0x02;
    ret = i2c_himax_master_write(ts->client, buf0, 3, DEFAULT_RETRY_CNT);//prefetch
    if(ret < 0) 
    {
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        goto send_i2c_msg_fail;
    } 
    udelay(100);

    buf0[0] = HX_CMD_76;
    buf0[1] = 0x01;
    buf0[2] = 0x2D;
    ret = i2c_himax_master_write(ts->client, buf0, 3, DEFAULT_RETRY_CNT);//prefetch
    if(ret < 0) 
    {    
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        goto send_i2c_msg_fail;
    } 
    udelay(100);

    return ret;

send_i2c_msg_fail:

    printk(KERN_ERR "[Himax]:send_i2c_msg_failline: %d \n",__LINE__);

    return -1;
}


static int himax_sense_enable(struct himax_ts_data *ts, bool enable)
{
    struct device *dev = &ts->client->dev;
    uint8_t cmd;
    int ret;

    if (enable)
        cmd = HX_CMD_TSSON;
    else
        cmd = HX_CMD_TSSON;

    ret = i2c_himax_master_write(ts->client, &cmd, 1, DEFAULT_RETRY_CNT);//sense on
    if(ret < 0) 
    {
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        return ret;
    } 
    msleep(120); //120ms

    dev_info(dev, "Set sense = %d\n", enable);

    return 0;
}


static int himax_sleep_enable(struct himax_ts_data *ts, bool enable)
{
    struct device *dev = &ts->client->dev;
    uint8_t cmd;
    int ret;

    if (enable)
        cmd = HX_CMD_TSSLPIN;
    else
        cmd = HX_CMD_TSSLPOUT;

    ret = i2c_himax_master_write(ts->client, &cmd, 1, DEFAULT_RETRY_CNT);//sense on
    if(ret < 0) 
    {
        printk(KERN_ERR "i2c_master_send failed addr = 0x%x\n",ts->client->addr);
        return ret;
    } 
    msleep(120); //120ms

    dev_info(dev, "Set sleep = %d\n", enable);

    return 0;
}

static int himax_start(struct himax_ts_data *ts)
{
    himax_sense_enable(ts, true);
    himax_sleep_enable(ts, false);

    return 0;
}

static irqreturn_t himax_ts_irq_handler(int irq, void *dev_id)
{
    struct himax_ts_data *ts = dev_id;

    himax_ts_handle_packet(ts);

    return IRQ_HANDLED;
}

static int himax_ts_probe(struct i2c_client *client,
        const struct i2c_device_id *id)
{
    struct himax_ts_data *ts;
    struct input_dev *input;
    int error;

    calculate_touch_info_point_calc_address();

    ts = devm_kzalloc(&client->dev, sizeof(*ts), GFP_KERNEL);
    if (!ts){
        return -ENOMEM;
    }

    ts->client = client;

    ts->pdata = himax_parse_dt(&ts->client->dev);

    mutex_init(&ts->mutex_lock);

    ts->input = input = devm_input_allocate_device(&client->dev);
    if (!input) {
        dev_err(&client->dev, "Failed to allocate input device\n");
        return -ENOMEM;
    }

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) 
    {
        printk(KERN_ERR "[HIMAX TP ERROR] %s: i2c check functionality error\n", __func__);
        return -ENODEV;
    }

    input->name = "Himax Touchscreen";
    input->id.bustype = BUS_I2C;

    __set_bit(EV_KEY, input->evbit);
    __set_bit(EV_ABS, input->evbit);
    __set_bit(BTN_TOUCH, input->keybit);

    input_set_abs_params(input, ABS_X, 0, ts->pdata->x_max, 0, 0);
    input_set_abs_params(input, ABS_Y, 0, ts->pdata->y_max, 0, 0);
    input_set_abs_params(input, ABS_MT_TRACKING_ID, 0, HIMAX_MAX_FINGERS, 0, 0);
    input_set_abs_params(input, ABS_MT_POSITION_X, 0, ts->pdata->x_max, 0, 0);
    input_set_abs_params(input, ABS_MT_POSITION_Y, 0, ts->pdata->y_max, 0, 0);
    input_set_abs_params(input, ABS_MT_TOUCH_MAJOR, 0, 31, 0, 0); //Touch Size
    input_set_abs_params(input, ABS_MT_WIDTH_MAJOR, 0, 31, 0, 0); //Finger Size
    input_set_abs_params(input, ABS_MT_PRESSURE, 0,  HIMAX_MAX_PRESSURE, 0, 0);

    error = input_mt_init_slots(input, HIMAX_MAX_FINGERS, INPUT_MT_DIRECT);
    if (error) {
        dev_err(&client->dev,
                "Failed to initialize MT slots: %d\n", error);
        return error;
    }

    input_set_drvdata(input, ts);
    i2c_set_clientdata(client, ts);

    himax_ts_reset(ts);

    himax_ts_poweron(ts);

    error = himax_start(ts);

    if (error) {
        dev_err(&client->dev,
                "Failed to register input device: %d\n", error);
        return error;
    }

    error = devm_request_threaded_irq(&client->dev, client->irq,
            NULL, himax_ts_irq_handler,
            IRQF_ONESHOT,
            client->name, ts);
    if (error) {
        dev_err(&client->dev, "Failed to request IRQ: %d\n", error);
        return error;
    }

    dev_dbg(&ts->client->dev, "requested irq %d", client->irq);

    error = input_register_device(ts->input);
    if (error) {
        dev_err(&client->dev,
                "Failed to register input device: %d\n", error);
        return error;
    }

    device_init_wakeup(&client->dev, 1);

    return 0;
}

static int himax_i2c_ts_remove(struct i2c_client *client)
{
    device_init_wakeup(&client->dev, 0);

    return 0;
}

static const struct of_device_id himax_ts_dt_ids[] = {
    { .compatible = "himax,himax_i2c_ts" },
    { /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, himax_ts_dt_ids);

static const struct i2c_device_id himax_ts_id[] = {
    { HIMAX_I2C_NAME,    0 },
    { /* sentinel */  }
};
MODULE_DEVICE_TABLE(i2c, himax_ts_id);

static struct i2c_driver himax_ts_driver = {
    .driver = {
        .owner    = THIS_MODULE,
        .name    = HIMAX_I2C_NAME,
        .of_match_table = of_match_ptr(himax_ts_dt_ids),
    },
    .probe        = himax_ts_probe,
    .remove        = himax_i2c_ts_remove,
    .id_table    = himax_ts_id,
};
module_i2c_driver(himax_ts_driver);

MODULE_DESCRIPTION("Himax HX85xx Touchscreen Driver");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Sven Emme <sven.emme@garz-fricke.com>");
