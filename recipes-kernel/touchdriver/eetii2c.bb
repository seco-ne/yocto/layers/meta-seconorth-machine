SUMMARY = "EETI I2C driver touch driver for touchscreens based on EXC7200 controller"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

require conf/seconorth-kernel-modules.inc

DEPENDS += " kernel-module-touchgpio "
