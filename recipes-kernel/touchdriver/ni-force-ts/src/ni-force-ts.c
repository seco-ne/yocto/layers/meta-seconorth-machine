/*
 * Reference Driver for NextInput Sensor
 *
 * The GPL Deliverables are provided to Licensee under the terms
 * of the GNU General Public License version 2 (the "GPL") and
 * any use of such GPL Deliverables shall comply with the terms
 * and conditions of the GPL. A copy of the GPL is available
 * in the license txt file accompanying the Deliverables and
 * at http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright (C) NextInput, Inc.
 * All rights reserved
 *
 * 1. Redistribution in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 * 2. Neither the name of NextInput nor the names of the contributors
 *    may be used to endorse or promote products derived from
 *    the software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING BUT
 * NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE. 
 */

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <asm/uaccess.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/time.h>
#include <linux/timer.h>
#include <linux/firmware.h>
#include <linux/timer.h>
#include <linux/gpio.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/workqueue.h>
#include <linux/of_gpio.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/regulator/consumer.h>
#include <linux/ctype.h>

// Registers
#define TOEN_REG 0
#define TOEN_POS 7
#define TOEN_MSK 0b10000000

#define WAIT_REG 0
#define WAIT_POS 4
#define WAIT_MSK 0b01110000

#define ADCRAW_REG 0
#define ADCRAW_POS 3
#define ADCRAW_MSK 0b00001000

#define TEMPWAIT_REG 0
#define TEMPWAIT_POS 1
#define TEMPWAIT_MSK 0b00000110

#define EN_REG 0
#define EN_POS 0
#define EN_MSK 0b00000001

#define INAGAIN_REG 1
#define INAGAIN_POS 4
#define INAGAIN_MSK 0b01110000

#define PRECHARGE_REG 1
#define PRECHARGE_POS 0
#define PRECHARGE_MSK 0b00000111

#define BUSY_REG 2
#define BUSY_POS 7
#define BUSY_MSK 0b10000000

#define OVRINTRTH_REG 2
#define OVRINTRTH_POS 3
#define OVRINTRTH_MSK 0b00001000

#define OVRACALTH_REG 2
#define OVRACALTH_POS 2
#define OVRACALTH_MSK 0b00000100

#define SNSERR_REG 2
#define SNSERR_POS 1
#define SNSERR_MSK 0b00000010

#define INTR_REG 2
#define INTR_POS 0
#define INTR_MSK 0b00000001

#define ADCOUT_REG 3
#define ADCOUT_SHIFT 4

#define SCOUNT_REG 4
#define SCOUNT_POS 0
#define SCOUNT_MSK 0b00001111

#define TEMP_REG 5
#define TEMP_SHIFT 4

#define AUTOCAL_REG 7
#define AUTOCAL_SHIFT 4

#define AUTOPRELDADJ_REG 8
#define AUTOPRELDADJ_POS 1
#define AUTOPRELDADJ_MSK 0b00000010

#define ENNEGZONE_REG 8
#define ENNEGZONE_POS 0
#define ENNEGZONE_MSK 0b00000001

#define ENCALMODE_REG 9
#define ENCALMODE_POS 7
#define ENCALMODE_MSK 0b10000000

#define CALRESET_REG 9
#define CALRESET_POS 4
#define CALRESET_MSK 0b01110000

#define INTRPOL_REG 9
#define INTRPOL_POS 3
#define INTRPOL_MSK 0b00001000

#define CALPERIOD_REG 9
#define CALPERIOD_POS 0
#define CALPERIOD_MSK 0b00000111

#define RISEBLWGT_REG 10
#define RISEBLWGT_POS 4
#define RISEBLWGT_MSK 0b01110000

#define LIFTDELAY_REG 10
#define LIFTDELAY_POS 0
#define LIFTDELAY_MSK 0b00000111

#define PRELDADJ_REG 11
#define PRELDADJ_POS 3
#define PRELDADJ_MSK 0b11111000

#define FALLBLWGT_REG 11
#define FALLBLWGT_POS 0
#define FALLBLWGT_MSK 0b00000111

#define INTRTHR_REG 12
#define INTRTHR_SHIFT 4

#define FALLTHRSEL_REG 13
#define FALLTHRSEL_POS 0
#define FALLTHRSEL_MSK 0b00000111

#define INTREN_REG 14
#define INTREN_POS 7
#define INTREN_MSK 0b10000000

#define INTRMODE_REG 14
#define INTRMODE_POS 6
#define INTRMODE_MSK 0b01000000

#define INTRPERSIST_REG 14
#define INTRPERSIST_POS 5
#define INTRPERSIST_MSK 0b00100000

#define BTNMODE_REG 14
#define BTNMODE_POS 4
#define BTNMODE_MSK 0b00010000

#define INTRSAMPLES_REG 14
#define INTRSAMPLES_POS 0
#define INTRSAMPLES_MSK 0b00000111

#define PWRMODE_REG 15
#define PWRMODE_POS 4
#define PWRMODE_MSK 0b11110000

#define FORCEBL_REG 15
#define FORCEBL_POS 0
#define FORCEBL_MSK 0b00000001

#define ADCMAX_REG 16
#define ADCMAX_SHIFT 4

#define BASELINE_REG 18
#define BASELINE_SHIFT 4

#define ADCFIFO_REG 20
#define ADCFIFO_SHIFT 4

#define FCOUNT_REG 21
#define FCOUNT_POS 0
#define FCOUNT_MSK 0b00001111

#define FALLTHR_REG 22
#define FALLTHR_SHIFT 4

#define DEVID_REG 0x80
#define DEVID_POS 3
#define DEVID_MSK 0b11111000

#define REV_REG 0x80
#define REV_POS 0
#define REV_MSK 0b00000111

#define TEMPSNSBL_REG 0xB3
#define TEMPSNSBL_POS 0
#define TEMPSNSBL_MSK 0b11111111

#define FIRST_REG TOEN_REG
#define LAST_REG (FALLTHR_REG + 1)

// Wait
#define WAIT_0MS 0
#define WAIT_1MS 1
#define WAIT_4MS 2
#define WAIT_8MS 3
#define WAIT_16MS 4
#define WAIT_32MS 5
#define WAIT_64MS 6
#define WAIT_256MS 7

// Adcraw
#define ADCRAW_BL 0
#define ADCRAW_RAW 1

// Temperature
#define TEMPWAIT_DISABLED 0
#define TEMPWAIT_32 1
#define TEMPWAIT_64 2
#define TEMPWAIT_128 3

#define TEMP_READ_TIME_MS 1

// Inagain
#define INAGAIN_1X 0
#define INAGAIN_2X 1
#define INAGAIN_4X 2
#define INAGAIN_8X 3
#define INAGAIN_16X 4
#define INAGAIN_32X 5
#define INAGAIN_64X 6
#define INAGAIN_128X 7

// Precharge
#define PRECHARGE_50US 0
#define PRECHARGE_100US 1
#define PRECHARGE_200US 2
#define PRECHARGE_400US 3
#define PRECHARGE_800US 4
#define PRECHARGE_1600US 5
#define PRECHARGE_3200US 6
#define PRECHARGE_6400US 7

// Calreset
#define CALRESET_500MS 0
#define CALRESET_1000MS 1
#define CALRESET_2000MS 2
#define CALRESET_4000MS 3
#define CALRESET_8000MS 4
#define CALRESET_16000MS 5
#define CALRESET_32000MS 6
#define CALRESET_DISABLED 7

#define CALRESET_TIMER_STOPPED 0
#define CALRESET_TIMER_STARTED 1
#define CALRESET_TIMER_FINISHED 2

// Calperiod
#define CALPERIOD_100MS 0
#define CALPERIOD_200MS 1
#define CALPERIOD_400MS 2
#define CALPERIOD_800MS 3
#define CALPERIOD_1600MS 4
#define CALPERIOD_3200MS 5
#define CALPERIOD_6400MS 6
#define CALPERIOD_DISABLED 7

#define CALPERIOD_TIMER_STOPPED 0
#define CALPERIOD_TIMER_STARTED 1
#define CALPERIOD_TIMER_FINISHED 2

// Riseblwgt
#define RISEBLWGT_0X 0
#define RISEBLWGT_1X 1
#define RISEBLWGT_3X 2
#define RISEBLWGT_7X 3
#define RISEBLWGT_15X 4
#define RISEBLWGT_31X 5
#define RISEBLWGT_63X 6
#define RISEBLWGT_127X 7

// Liftdelay
#define LIFTDELAY_DISABLED 0
#define LIFTDELAY_20MS 1
#define LIFTDELAY_40MS 2
#define LIFTDELAY_80MS 3
#define LIFTDELAY_160MS 4
#define LIFTDELAY_320MS 5
#define LIFTDELAY_640MS 6
#define LIFTDELAY_1280MS 7

#define LIFTDELAY_TIMER_DISABLED 0
#define LIFTDELAY_TIMER_ENABLED 1
#define LIFTDELAY_TIMER_RUNNING 2

// Fallblwgt
#define FALLBLWGT_0X 0
#define FALLBLWGT_1X 1
#define FALLBLWGT_3X 2
#define FALLBLWGT_7X 3
#define FALLBLWGT_15X 4
#define FALLBLWGT_31X 5
#define FALLBLWGT_63X 6
#define FALLBLWGT_127X 7

// Calmode
#define CALMODE_COUPLED 0
#define CALMODE_DECOUPLED 1

// Intrmode
#define INTRMODE_THRESH 0
#define INTRMODE_DRDY 1

// Intrpersist
#define INTRPERSIST_PULSE 0
#define INTRPERSIST_INF 1

#define INT_DURATION 100

// Intrsamples
#define INTRSAMPLES_1 0
#define INTRSAMPLES_2 1
#define INTRSAMPLES_3 2
#define INTRSAMPLES_4 3
#define INTRSAMPLES_5 4
#define INTRSAMPLES_6 5
#define INTRSAMPLES_7 6
#define INTRSAMPLES_8 7

// Target
#define TARGET_ADCOUT_LEN 2

#define TARGET_ADC_DATA_MASK 0xFFF
#define TARGET_ADC_SIGN_MASK 0x800
#define TARGET_MAX_AUTOCAL TARGET_ADC_DATA_MASK
#define TARGET_MAX_INTERRUPT TARGET_ADC_DATA_MASK

#define TARGET_ADC_FIELD_READ(base,shift) ((((u16) data_buffer_out[(base) + 0]) << (shift)) + (data_buffer_out[(base) + 1] >> (shift)))

#define TARGET_ADC_FIELD_WRITE(base,value,shift) do { data_buffer_out[(base) + 0] = (u8) ((value) >> (shift)); data_buffer_out[(base) + 1] = (u8) (data_buffer_out[(base) + 1] & 0x0f) | (u8) ((value) << (shift)); } while (0)

#define TARGET_SCOUNT(base,shift,mask) ((data_buffer_out[(base) + 1] & (mask)) >> (shift))

#define ADC_SCALER 1

#define DEVICE_NAME "ni_force"
#define DEVICE_TREE_NAME "nif,ni_force"

#define NI_WRITE_PERMISSIONS (S_IWUSR | S_IWGRP)

#define OEM_ID_VENDOR 0x0101
#define OEM_ID_PRODUCT 0x0101
#define OEM_ID_VERSION 0x0101

#define OEM_DATE "MM DD YYYY"
#define OEM_TIME "HH:MM:SS"

#define OEM_I2C_ATTEMPTS 1

/* Pixel phone IRQ is active high so trigger on rising signal */
#define OEM_IRQ_TRIGGER (IRQF_TRIGGER_RISING | IRQF_ONESHOT)

#define OEM_PRELOAD_LOW (-500 * ADC_SCALER)
#define OEM_PRELOAD_HIGH (500 * ADC_SCALER)

#define OEM_SLOPE_SCALER 1000

#define OEM_MIN_SLOPE 50
#define OEM_MAX_SLOPE 15000
#define OEM_MIN_INTERCEPT (-50 * ADC_SCALER)
#define OEM_MAX_INTERCEPT (50 * ADC_SCALER)

#define OEM_EVENT_CODE 250

#define OEM_SYSFS 

#define DRIVER_VERSION 5
#define DRIVER_REVISION 1
#define DRIVER_BUILD 1

#define MAX_RETRY_COUNT 3
#define BOOTING_DELAY 400

enum {
    CID_ADCOUT,
    CID_FORCE,
    CID_ISR,
};

#define OTP_SIZE 3
#define OTP_FIRST 0x04
#define OTP_LAST 0x0F

#define UCHAR_MAX ((u8)(-1))

#define SKIP_BLANKS(p) do { while ((*p == ' ') || (*p == '\t')) p++; } while (0)
#define SKIP_UPPERCASE(p) do { while ((*p >= 'A') && (*p <= 'Z')) p++; } while (0)
#define SKIP_SIGN(p) do { if ((*p == '+') || (*p == '-')) p++; } while (0)
#define SKIP_DIGITS(p) do { while ((*p >= '0') && (*p <= '9')) p++; } while (0)
#define SKIP_HEXDIGITS(p) do { while (isxdigit(*p)) p++; } while (0)

struct ni_force_platform_data {

    int dummy;                  // Make sure struct has at least one field
};

struct ni_force_data {
    struct i2c_client *client;                  // Pointer to i2c client

    struct ni_force_platform_data *pdata;       // Pointer to platform data

    int device_id;
    int revision_id;

    int adcraw_mode;

    int slope;
    int intercept;

    u8  otp_buffer[OTP_LAST - OTP_FIRST + 1];
};

struct ni_force_sample {
    int kadcout;
    int scount;
    int kbaseline;
};

#define PRELDADJ_MAX (PRELDADJ_MSK >> PRELDADJ_POS)

enum {
    ID_TOEN = 0,
    ID_WAIT,
    ID_ADCRAW,
    ID_TEMPWAIT,
    ID_EN,
    ID_INAGAIN,
    ID_PRECHARGE,
    ID_AUTOCAL,
    ID_AUTOPRELDADJ,
    ID_CALRESET,
    ID_CALPERIOD,
    ID_RISEBLWGT,
    ID_LIFTDELAY,
    ID_PRELDADJ,
    ID_FALLBLWGT,
    ID_FORCEBL,
    ID_SLOPE,
    ID_INTERCEPT,
};

struct ni_force_register {
    char name[16];              // Register name
    int id;                     // Register ID
    int address;                // Register address
    int mask;                   // Register mask
    int position;               // Register position
    int minimum;                // Minimum valid register value
    int maximum;                // Maximum valid register value
    int value;                  // Register value
    bool specified;             // Whether register specified or not

} registers[] = {
    // name           id                 address           mask              position         min  max                   value  specified

    { "TOEN",         ID_TOEN,           TOEN_REG,         TOEN_MSK,         TOEN_POS,          0, 1,                        0, false },
    { "WAIT",         ID_WAIT,           WAIT_REG,         WAIT_MSK,         WAIT_POS,          0, 7,                        0, false },
    { "ADCRAW",       ID_ADCRAW,         ADCRAW_REG,       ADCRAW_MSK,       ADCRAW_POS,        0, 1,                        0, false },
    { "TEMPWAIT",     ID_TEMPWAIT,       TEMPWAIT_REG,     TEMPWAIT_MSK,     TEMPWAIT_POS,      0, 3,                        0, false },
    { "EN",           ID_EN,             EN_REG,           EN_MSK,           EN_POS,            0, 1,                        0, false },
    { "INAGAIN",      ID_INAGAIN,        INAGAIN_REG,      INAGAIN_MSK,      INAGAIN_POS,       0, 7,                        0, false },
    { "PRECHARGE",    ID_PRECHARGE,      PRECHARGE_REG,    PRECHARGE_MSK,    PRECHARGE_POS,     0, 7,                        0, false },
    { "AUTOCAL",      ID_AUTOCAL,        AUTOCAL_REG,      0,                AUTOCAL_SHIFT,     0, TARGET_MAX_AUTOCAL,       0, false },
    { "AUTOPRELDADJ", ID_AUTOPRELDADJ,   AUTOPRELDADJ_REG, AUTOPRELDADJ_MSK, AUTOPRELDADJ_POS,  0, 1,                        0, false },
    { "CALRESET",     ID_CALRESET,       CALRESET_REG,     CALRESET_MSK,     CALRESET_POS,      0, 7,                        0, false },
    { "CALPERIOD",    ID_CALPERIOD,      CALPERIOD_REG,    CALPERIOD_MSK,    CALPERIOD_POS,     0, 7,                        0, false },
    { "RISEBLWGT",    ID_RISEBLWGT,      RISEBLWGT_REG,    RISEBLWGT_MSK,    RISEBLWGT_POS,     0, 7,                        0, false },
    { "LIFTDELAY",    ID_LIFTDELAY,      LIFTDELAY_REG,    LIFTDELAY_MSK,    LIFTDELAY_POS,     0, 7,                        0, false },
    { "PRELDADJ",     ID_PRELDADJ,       PRELDADJ_REG,     PRELDADJ_MSK,     PRELDADJ_POS,      0, PRELDADJ_MAX,             0, false },
    { "FALLBLWGT",    ID_FALLBLWGT,      FALLBLWGT_REG,    FALLBLWGT_MSK,    FALLBLWGT_POS,     0, 7,                        0, false },
    { "FORCEBL",      ID_FORCEBL,        FORCEBL_REG,      FORCEBL_MSK,      FORCEBL_POS,       0, 1,                        0, false },
    { "SLOPE",        ID_SLOPE,          0,                0,                0,     OEM_MIN_SLOPE, OEM_MAX_SLOPE,            0, false },
    { "INTERCEPT",    ID_INTERCEPT,      0,                0,                0, OEM_MIN_INTERCEPT, OEM_MAX_INTERCEPT,        0, false },
    { "",             0,                 0,                0,                0,                 0, 0,                        0, 0     }
};

enum {
    DEBUG_NONE          = 0,
    DEBUG_BASE_INFO     = (1U << 0),
    DEBUG_COMMAND       = (1U << 1),
    DEBUG_VERBOSE       = (1U << 2),
    DEBUG_DATA          = (1U << 3),
    DEBUG_HARDWARE      = (1U << 4),
};

/* Debug mask value
 * usage: echo [debug_mask] > /sys/module/ni_force_ts/parameters/debug_mask
 */
static u32 ni_debug_mask = DEBUG_BASE_INFO;
module_param_named(debug_mask, ni_debug_mask, int, S_IRUGO|S_IWUSR|S_IWGRP);

// DRIVER FUNCTION PROTOTYPES

static int ni_force_get_kadcout_and_kbaseline(struct ni_force_data *fdata,
                                              struct ni_force_sample *sample);
static int ni_force_get_sample(struct ni_force_data *fdata,
                               struct ni_force_sample *sample,
                               int caller_id);
static void ni_force_get_force(struct ni_force_data *fdata,
                               int *force,
                               int caller_id);
static int  ni_force_get_ic_info(struct ni_force_data *fdata);
static int  ni_force_i2c_read(struct i2c_client *client, u8 reg, int len,
                              u8 *buf);
static int ni_force_i2c_write(struct i2c_client *client, u8 reg, int len,
                              u8 *buf);
static int ni_force_i2c_write_byte(struct i2c_client *client, u8 reg,
                                   u8 data);
static int ni_force_i2c_modify_byte(struct i2c_client *client, u8 reg, u8 data,
                                    u8 mask);

// Info Log
#define LOGI(fmt,args...) do { if (ni_debug_mask & DEBUG_BASE_INFO) pr_info("[NextInput] " fmt, ##args); } while (0)

// Error Log
#define LOGE(fmt,args...) pr_err("[NextInput E] [%s %d] " fmt, __func__, __LINE__, ##args)

// Verbose log
#define LOGV(fmt,args...) do { if (ni_debug_mask & DEBUG_VERBOSE) pr_err("[NextInput V] [%s %d] " fmt, __func__, __LINE__, ##args); } while (0)

// Error Log - rate limited
#define LOGE_LMT(fmt,args...) pr_err_ratelimited("[NextInput E] [%s %d] " fmt, __func__, __LINE__, ##args)

static int num_clients;

static int oem_force_ic_init(struct ni_force_data *fdata)
{
    /* LOGI("%s\n", __func__); */

    return 0;
}

/* System functions accessible via ADB Shell or similar Linux shell */

/* ni_force_numsensors_show
 *
 * Show number of sensors
 * ADB Console Usage: cat numsensors
 *
 */
static ssize_t ni_force_numsensors_show(struct device *dev,
                                        struct device_attribute *attr,
                                        char *buf)
{
    return snprintf(buf, PAGE_SIZE, "1\n");
}

/* ni_force_adcout_show
 *
 * Show adcout data from AFE
 * ADB Console Usage: cat adcout
 *
 */
static ssize_t ni_force_adcout_show(struct device *dev,
                                    struct device_attribute *attr,
                                    char *buf)
{
    struct ni_force_sample sample;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    if (ni_force_get_sample(fdata, &sample, CID_ADCOUT) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    return snprintf(buf, PAGE_SIZE, "%d %d\n", sample.kadcout, sample.scount);
}

/* ni_force_baseline_show
 *
 * Show baseline data
 * ADB Console Usage: cat baseline
 *
 */
static ssize_t ni_force_baseline_show(struct device *dev,
                                      struct device_attribute *attr,
                                      char *buf)
{
    struct ni_force_sample sample;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    if (ni_force_get_kadcout_and_kbaseline(fdata, &sample) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    return snprintf(buf, PAGE_SIZE, "%d\n", sample.kbaseline);
}

/* ni_force_status_show
 *
 * Show status data from AFE
 * ADB Console Usage: cat status
 *
 */
static ssize_t ni_force_status_show(struct device *dev,
                                    struct device_attribute *attr,
                                    char *buf)
{
    u8 data_buffer_out[1];

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    if (ni_force_i2c_read(fdata->client, INTR_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    return snprintf(buf, PAGE_SIZE, "%d\n", data_buffer_out[0]);
}

/* ni_force_temperature_show
 *
 * Show temperature data from AFE
 * ADB Console Usage: cat temperature
 *
 */
static ssize_t ni_force_temperature_show(struct device *dev,
                                         struct device_attribute *attr,
                                         char *buf)
{
    u8 data_buffer_out[2];
    int temperature;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    if (ni_force_i2c_read(fdata->client, TEMP_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    temperature = TARGET_ADC_FIELD_READ(0, TEMP_SHIFT);

    if (ni_force_i2c_read(fdata->client, TEMPSNSBL_REG,
                          1, data_buffer_out) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    /* discard unused upper nibble and subtract baseline */
    temperature &= 0xFF;
    temperature -= ((data_buffer_out[0] & TEMPSNSBL_MSK) >> TEMPSNSBL_POS);

    return snprintf(buf, PAGE_SIZE, "%d\n", temperature);
}

/* ni_force_register_show
 *
 * Show register data from AFE
 * ADB Console Usage: cat register
 *
 */
static ssize_t ni_force_register_show(struct device *dev,
                                      struct device_attribute *attr,
                                      char *buf)
{
    u8 i;
    u8 data_buffer_out[LAST_REG - FIRST_REG + 1];
    int value;
    int ret = 0;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    if (ni_force_i2c_read(fdata->client, FIRST_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    for (i = 0; registers[i].name[0]; i++) {

        /* show register name */
        ret += snprintf(buf + ret, PAGE_SIZE, "%s ", registers[i].name);

        /* show register value */

        switch (registers[i].id) {
        case ID_AUTOCAL:
            value = TARGET_ADC_FIELD_READ(registers[i].address, registers[i].position);
            break;

        case ID_SLOPE:
            value = fdata->slope;
            break;

        case ID_INTERCEPT:
            value = fdata->intercept;
            break;

        default:
            value = (data_buffer_out[registers[i].address] & registers[i].mask) >> registers[i].position;
            break;
        }
        ret += snprintf(buf + ret, PAGE_SIZE, "%d\n", value);
    }

    return ret;
}

/* ni_force_register_store
 *
 * Store register data
 * ADB Console Usage: echo [reg value] ... > register
 *
 */
static ssize_t ni_force_register_store(struct device *dev,
                                       struct device_attribute *attr,
                                       const char *buf, size_t count)
{
    int i;
    int value;
    const char *pbuf = buf;
    u8 data_buffer_out[LAST_REG - FIRST_REG + 1];

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    /* initially, no registers specified */
    for (i = 0; registers[i].name[0]; i++) {
        registers[i].specified = false;
    }

    /* get all specified registers and their values */
    for (;;) {
        /* point to register name or end of line */
        SKIP_BLANKS(pbuf);

        if ((*pbuf == '\n') || (*pbuf == 0)) {
            break;
        }

        /* identify register */
        for (i = 0; registers[i].name[0]; i++) {
            if (!strncmp(pbuf, registers[i].name, strlen(registers[i].name))) {
                break;
            }
        }
        if (!registers[i].name[0]) {
            LOGE("Invalid register name\n");
            return count;
        }

        SKIP_UPPERCASE(pbuf);

        /* get register value */
        SKIP_BLANKS(pbuf);

        if ((sscanf(pbuf, "%d", &value) != 1) || (value < registers[i].minimum) || (value > registers[i].maximum)) {
            LOGE("Invalid register value\n");
            return count;
        }
        registers[i].value = value;

        SKIP_SIGN(pbuf);
        SKIP_DIGITS(pbuf);

        registers[i].specified = true;
    }

    /* read, modify, write entire register set */
    if (ni_force_i2c_read(fdata->client, FIRST_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("Error reading registers\n");
        return count;
    }

    for (i = 0; registers[i].name[0]; i++) {
        if (registers[i].specified) {

            value = registers[i].value;
            switch (registers[i].id) {
            case ID_AUTOCAL:
                TARGET_ADC_FIELD_WRITE(registers[i].address, value, registers[i].position);
                break;

            case ID_SLOPE:
                fdata->slope = value;
                break;

            case ID_INTERCEPT:
                fdata->intercept = value;
                break;

            case ID_ADCRAW:
                fdata->adcraw_mode = value;
                fallthrough;
            default:
                data_buffer_out[registers[i].address] = (u8) (data_buffer_out[registers[i].address] & ~registers[i].mask) | (u8) (value << registers[i].position);
                break;
            }
        }
    }

    if (ni_force_i2c_write(fdata->client, FIRST_REG,
                           sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("Error writing registers\n");
        return count;
    }

    return count;
}

/* ni_force_raw_show
 *
 * Show raw register data from AFE
 * ADB Console Usage: cat raw
 *
 */
static ssize_t ni_force_raw_show(struct device *dev,
                                 struct device_attribute *attr,
                                 char *buf)
{
    u8 i;
    u8 data_buffer_out[LAST_REG - FIRST_REG + 1];
    int ret = 0;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    if (ni_force_i2c_read(fdata->client, FIRST_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        return snprintf(buf, PAGE_SIZE, "i2c_read error\n");
    }

    ret = snprintf(buf + ret, PAGE_SIZE, "     0  1  2  3\n");

    for (i = 0; i < sizeof(data_buffer_out); i++) {
        if ((i & 0x03) == 0) {
            ret += snprintf(buf + ret, PAGE_SIZE, "%02x: ", i);
        }
        ret += snprintf(buf + ret, PAGE_SIZE, "%02x ", data_buffer_out[i]);
        if ((i & 0x03) == 0x03) {
            ret += snprintf(buf + ret, PAGE_SIZE, "\n");
        }
    }

    if (i & 0x03) {
        ret += snprintf(buf + ret, PAGE_SIZE, "\n");
    }

    return ret;
}

/* ni_force_raw_store
 *
 * Store raw register data
 * ADB Console Usage: echo [addr] [data ...] > raw
 *
 */
static ssize_t ni_force_raw_store(struct device *dev,
                                  struct device_attribute *attr,
                                  const char *buf, size_t count)
{
    int i;
    u32 u;
    const char *pbuf = buf;
    u8 reg;
    u8 data_buffer_out[LAST_REG - FIRST_REG + 1];

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    /* get register address */
    SKIP_BLANKS(pbuf);

    if ((sscanf(pbuf, "%x", &u) != 1) || (u > UCHAR_MAX)) {
        LOGE("Invalid register address\n");
        return count;
    }
    reg = (u8)u;

    SKIP_HEXDIGITS(pbuf);

    /* get register data */
    for (i = 0; i <= LAST_REG; i++) {
        SKIP_BLANKS(pbuf);

        if ((sscanf(pbuf, "%x", &u) != 1) || (u > UCHAR_MAX)) {
            /* check if at least one valid byte found */
            if (i) {
                break;
            }

            LOGE("Invalid register data\n");
            return count;
        }
        data_buffer_out[i] = (u8)u;

        SKIP_HEXDIGITS(pbuf);
    }

    /* make sure registers to be written are within valid range */
    if ((reg < FIRST_REG) || (reg + i - 1 > LAST_REG)) {
        LOGE("Invalid register range\n");
        return count;
    }

    if (ni_force_i2c_write(fdata->client, reg, i, data_buffer_out) < 0) {
        LOGE("Error writing registers\n");
        return count;
    }

    return count;
}

/* ni_force_otp_show
 *
 * Show otp register data from AFE
 * ADB Console Usage: cat otp
 *
 */
static ssize_t ni_force_otp_show(struct device *dev,
                                 struct device_attribute *attr,
                                 char *buf)
{
    int ret = 0;

    u8 i;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    for (i = OTP_FIRST; i <= OTP_LAST; i++) {

        ret += snprintf(buf + ret, PAGE_SIZE, "%02x ", fdata->otp_buffer[i - OTP_FIRST]);

        fdata->otp_buffer[i - OTP_FIRST] = 0;
    }

    ret += snprintf(buf + ret, PAGE_SIZE, "\n");

    return ret;
}

/* ni_force_otp_store
 *
 * Store otp register data
 * ADB Console Usage: echo [data ...] > otp
 *
 */
static ssize_t ni_force_otp_store(struct device *dev,
                                  struct device_attribute *attr,
                                  const char *buf, size_t count)
{
    int i;
    u32 u;
    const char *pbuf = buf;
    u8 data_buffer_out[OTP_SIZE];

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    for (i = 0; i < OTP_SIZE; i++) {
        SKIP_BLANKS(pbuf);

        if ((sscanf(pbuf, "%x", &u) != 1) || (u > UCHAR_MAX)) {
            LOGE("Invalid register data\n");
            return count;
        }
        data_buffer_out[i] = (u8)u;

        SKIP_HEXDIGITS(pbuf);
    }

    if (ni_force_i2c_write(fdata->client, 0x90,
                           sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("Error writing registers\n");
        return count;
    }

    for (i = OTP_FIRST; i <= OTP_LAST; i++) {

        if (ni_force_i2c_write_byte(fdata->client, 0xAD, i) < 0) {
            LOGE("Error writing register\n");
            break;
        }

        if (ni_force_i2c_write_byte(fdata->client, 0xAC, 0xA0) < 0) {
            LOGE("Error writing register\n");
            break;
        }

        if (ni_force_i2c_read(fdata->client, 0xB4,
                              1, data_buffer_out) < 0) {
            LOGE("Error reading register\n");
            break;
        }

        fdata->otp_buffer[i - OTP_FIRST] = data_buffer_out[0];
    }

    for (i = 0; i < OTP_SIZE; i++) {
        data_buffer_out[i] = 0;
    }

    if (ni_force_i2c_write(fdata->client, 0x90,
                           sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("Error writing registers\n");
        return count;
    }

    return count;
}

/* ni_force_force_show
 *
 * Show force value
 * ADB Console Usage: cat force
 *
 */
static ssize_t ni_force_force_show(struct device *dev,
                                   struct device_attribute *attr,
                                   char *buf)
{
    int force;

    struct ni_force_data *fdata = dev_get_drvdata(dev);

    ni_force_get_force(fdata, &force, CID_FORCE);

    return snprintf(buf, PAGE_SIZE, "%d\n", force);
}

/* ni_force_version_show
 *
 * Show driver and AFE version
 * ADB Console Usage: cat version
 *
 */
static ssize_t ni_force_version_show(struct device *dev,
                                     struct device_attribute *attr,
                                     char *buf)
{
    struct ni_force_data *fdata = dev_get_drvdata(dev);

    return snprintf(buf, PAGE_SIZE, "Driver %d.%d.%d\nAFE %d.%d\n",
                    DRIVER_VERSION, DRIVER_REVISION, DRIVER_BUILD,
                    fdata->device_id, fdata->revision_id);
}

static struct device_attribute ni_force_device_attrs[] = {

  __ATTR(numsensors,    S_IRUGO,                        ni_force_numsensors_show,  NULL),
  __ATTR(adcout,        S_IRUGO,                        ni_force_adcout_show,      NULL),
  __ATTR(baseline,      S_IRUGO,                        ni_force_baseline_show,    NULL),
  __ATTR(status,        S_IRUGO,                        ni_force_status_show,      NULL),
  __ATTR(temperature,   S_IRUGO,                        ni_force_temperature_show, NULL),
  __ATTR(register,      S_IRUGO | NI_WRITE_PERMISSIONS, ni_force_register_show,    ni_force_register_store),
  __ATTR(raw,           S_IRUGO | NI_WRITE_PERMISSIONS, ni_force_raw_show,         ni_force_raw_store),
  __ATTR(otp,           S_IRUGO | NI_WRITE_PERMISSIONS, ni_force_otp_show,         ni_force_otp_store),
  __ATTR(force,         S_IRUGO,                        ni_force_force_show,       NULL),
  __ATTR(version,       S_IRUGO,                        ni_force_version_show,     NULL),

  OEM_SYSFS
};

static int ni_force_i2c_read(struct i2c_client *client, u8 reg, int len,
                             u8 *buf)
{

    struct i2c_msg msgs[] = {
        {
            .addr = client->addr,
            .flags = 0,
            .len = 1,
            .buf = &reg,
        },
        {
            .addr = client->addr,
            .flags = I2C_M_RD,
            .len = len,
            .buf = buf,
        },
    };
    int res;
    int ret = 0;

    res = i2c_transfer(client->adapter, msgs, 2);

    if (res < 0) {
        LOGE_LMT("transfer error\n");
        ret = -EIO;
    }

    return ret;
}

static int ni_force_i2c_write(struct i2c_client *client, u8 reg, int len,
                              u8 *buf)
{

    u8 send_buf[max(OTP_SIZE, LAST_REG+1)  + 1];
    struct i2c_msg msgs[] = {
        {
            .addr = client->addr,
            .flags = client->flags,
            .len = len + 1,
            .buf = send_buf,
        },
    };
    int i;
    int res;
    int ret = -EIO;

    for (i = 0; i < OEM_I2C_ATTEMPTS; i++) {

        send_buf[0] = (u8)reg;
        memcpy(&send_buf[1], buf, len);

        res = i2c_transfer(client->adapter, msgs, 1);

        if (res < 0) {
            LOGE_LMT("transfer error\n");
            continue;
        }

        ret = 0;
        break;
    }

    return ret;
}

static int ni_force_i2c_write_byte(struct i2c_client *client, u8 reg, u8 data)
{

    u8 send_buf[2];
    struct i2c_msg msgs[] = {
        {
            .addr = client->addr,
            .flags = client->flags,
            .len = 2,
            .buf = send_buf,
        },
    };
    int i;
    int res;
    int ret = -EIO;

    for (i = 0; i < OEM_I2C_ATTEMPTS; i++) {

        send_buf[0] = (u8)reg;
        send_buf[1] = (u8)data;

        res = i2c_transfer(client->adapter, msgs, 1);

        if (res < 0) {
            LOGE_LMT("transfer error\n");
            continue;
        }

        ret = 0;
        break;
    }

    return ret;
}

/* ni_force_i2c_modify_byte
 *
 * Read register, clear bits specified by 'mask', set any bits specified by 'data',
 * then write back to register
 *
 */
static int ni_force_i2c_modify_byte(struct i2c_client *client, u8 reg, u8 data,
                                    u8 mask)
{
    u8 buf;

    if (ni_force_i2c_read(client, reg, sizeof(buf), &buf) < 0) {
        return -EIO;
    }

    buf = (buf & ~mask) | data;

    if (ni_force_i2c_write_byte(client, reg, buf) < 0) {
        return -EIO;
    }

    return 0;
}

static int ni_force_read_sample_registers(struct ni_force_data *fdata,
                                          int *adcout,
                                          int *scount,
                                          int *baseline)
{
    u8 data_buffer_out[TARGET_ADCOUT_LEN];

    if (ni_force_i2c_read(fdata->client, ADCOUT_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        return -EIO;
    }

    *adcout = TARGET_ADC_FIELD_READ(0, ADCOUT_SHIFT);

    /* If ADCOUT is negative then sign extend it */
    if (*adcout & TARGET_ADC_SIGN_MASK) {
        *adcout |= ((-1) & ~TARGET_ADC_DATA_MASK);
    }

    *adcout *= ADC_SCALER;

    *scount = TARGET_SCOUNT(0, SCOUNT_POS, SCOUNT_MSK);

    if (ni_force_i2c_read(fdata->client, BASELINE_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        return -EIO;
    }

    *baseline = TARGET_ADC_FIELD_READ(0, ADCOUT_SHIFT);

    /* If baseline is negative then sign extend it */
    if (*baseline & TARGET_ADC_SIGN_MASK) {
        *baseline |= ((-1) & ~TARGET_ADC_DATA_MASK);
    }

    *baseline *= ADC_SCALER;

    return 0;
}

static int ni_force_get_kadcout_and_kbaseline(struct ni_force_data *fdata,
                                              struct ni_force_sample *sample)
{
    int adcout;
    int scount;
    int baseline;
    int ret;

    ret = ni_force_read_sample_registers(fdata, &adcout, &scount, &baseline);
    if (ret < 0) {
        return ret;
    }

    {
        sample->kadcout   = adcout;
        sample->kbaseline = baseline;
    }

    sample->scount = scount;

    return 0;
}

/* ni_force_get_sample
 *
 * Get ADCOUT and process
 * Called by workqueue, sysfs 'adcout', sysfs 'force', ISR, TP
 *
 */
static int ni_force_get_sample(struct ni_force_data *fdata,
                               struct ni_force_sample *sample,
                               int caller_id)
{
    int ret;

    ret = ni_force_get_kadcout_and_kbaseline(fdata, sample);
    if (ret < 0) {
        return ret;
    }

    if (caller_id == CID_ADCOUT) {
        return 0;
    }

    return 0;
}

/* ni_force_get_force
 *
 * Called by sysfs 'force', ISR, TP
 *
 */
static void ni_force_get_force(struct ni_force_data *fdata,
                               int *force,
                               int caller_id)
{
    struct ni_force_sample sample;

    if (
        (fdata->slope == 0) || fdata->adcraw_mode) {

        /* not touched, not calibrated or in raw mode, so return 1 */
        *force = 1;
        return;
    }

    if (ni_force_get_sample(fdata, &sample, caller_id) < 0) {
        *force = 1;
        return;
    }

    /* generate scaled value...
     *
     * grams = (adc_count x slope) + intercept
     *
     * Note: Assumes slope has been previously scaled by OEM_SLOPE_SCALER
     */
    *force = (((int)(sample.kadcout) * fdata->slope) / OEM_SLOPE_SCALER) + fdata->intercept;
    if (*force <= 0) {
        *force = 1;
    }
}

static int ni_force_get_ic_info(struct ni_force_data *fdata)
{
    u8 data_buffer_out[1];

    LOGI("%s\n", __func__);

    if (ni_force_i2c_read(fdata->client, DEVID_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("DEVID_REG read fail\n");
        return -EIO;
    }

    fdata->device_id   = (data_buffer_out[0] & DEVID_MSK) >> DEVID_POS;
    fdata->revision_id = (data_buffer_out[0] & REV_MSK)   >> REV_POS;

    return 0;
}

static int ni_force_parse_dt(struct device *dev,
                             struct ni_force_platform_data *pdata
)
{

    LOGV("%s\n", __func__);

    return 0;
}

static int ni_force_manual_preldadj(struct ni_force_data *fdata)
{
    static u8 preldadj_table[] = {
        0x0F, 0x0E, 0x0D, 0x0C, 0x0B, 0x0A, 0x09, 0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F
    };

    u8 data_buffer_out[1];
    u8 autopreldadj_save;
    u8 wait_save;
    int i;
    int iterations;
    int adcout;
    int scount;
    int baseline;
    int ret;

    LOGI("%s\n", __func__);

    /* enable raw mode */
    if (ni_force_i2c_modify_byte(fdata->client, ADCRAW_REG,
                                 1 << ADCRAW_POS, ADCRAW_MSK) < 0) {
        LOGE("ADCRAW_REG modify fail\n");
        return -EIO;
    }

    /* save autopreload adjust setting, then disable it */
    if (ni_force_i2c_read(fdata->client, AUTOPRELDADJ_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("AUTOPRELDADJ_REG read fail\n");
        return -EIO;
    }
    autopreldadj_save = data_buffer_out[0] & AUTOPRELDADJ_MSK;

    if (ni_force_i2c_modify_byte(fdata->client, AUTOPRELDADJ_REG,
                                 0, AUTOPRELDADJ_MSK) < 0) {
        LOGE("AUTOPRELDADJ_REG modify fail\n");
        return -EIO;
    }

    /* save wait setting, then set it to zero ms */
    if (ni_force_i2c_read(fdata->client, WAIT_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("WAIT_REG read fail\n");
        return -EIO;
    }
    wait_save = data_buffer_out[0] & WAIT_MSK;

    if (ni_force_i2c_modify_byte(fdata->client, WAIT_REG,
                                 WAIT_0MS << WAIT_POS, WAIT_MSK) < 0) {
        LOGE("WAIT_REG modify fail\n");
        return -EIO;
    }

    /* convert preload adjust setting to corresponding table index */
    if (ni_force_i2c_read(fdata->client, PRELDADJ_REG,
                          sizeof(data_buffer_out), data_buffer_out) < 0) {
        LOGE("PRELDADJ_REG read fail\n");
        return -EIO;
    }

    data_buffer_out[0] = (data_buffer_out[0] & PRELDADJ_MSK) >> PRELDADJ_POS;
    for (i = 0; i < sizeof(preldadj_table); i++) {
        if (preldadj_table[i] == data_buffer_out[0]) {
            break;
        }
    }
    if (i >= sizeof(preldadj_table)) {  /* sanity check */
        return -EIO;
    }

    /* read and discard sample, wait for new sample */
    ni_force_read_sample_registers(fdata, &adcout, &scount, &baseline);
    usleep_range(2000, 4000); /* wait at least 2ms */

    /* do preload adjustment, if needed */
    iterations = 0;
    while (iterations++ < sizeof(preldadj_table)) {
        ret = ni_force_read_sample_registers(fdata, &adcout, &scount, &baseline);
        if (ret < 0) {
            return ret;
        }

        LOGI("%s setting=0x%02x adcout=%d\n", __func__, preldadj_table[i], adcout);

        if (adcout > OEM_PRELOAD_HIGH) {
            if (++i >= sizeof(preldadj_table)) {
                LOGE("Sensor preload too high\n");
                break;
            }
        } else
        if (adcout < OEM_PRELOAD_LOW) {
            if (--i < 0) {
                LOGE("Sensor preload too low\n");
                break;
            }
        } else {
            LOGI("Sensor preload sufficient\n");
            break;
        }

        /* program new preload adjust setting */
        if (ni_force_i2c_modify_byte(fdata->client, PRELDADJ_REG,
                                     preldadj_table[i] << PRELDADJ_POS, PRELDADJ_MSK) < 0) {
            LOGE("PRELDADJ_REG modify fail\n");
            return -EIO;
        }
        usleep_range(10000, 20000); /* wait at least 10ms */
    }

    /* restore settings */
    if (ni_force_i2c_modify_byte(fdata->client, WAIT_REG,
                                 wait_save, WAIT_MSK) < 0) {
        LOGE("WAIT_REG modify fail\n");
        return -EIO;
    }

    if (ni_force_i2c_modify_byte(fdata->client, AUTOPRELDADJ_REG,
                                 autopreldadj_save, AUTOPRELDADJ_MSK) < 0) {
        LOGE("AUTOPRELDADJ_REG modify fail\n");
        return -EIO;
    }

    /* disable raw mode */
    if (ni_force_i2c_modify_byte(fdata->client, ADCRAW_REG,
                                 0, ADCRAW_MSK) < 0) {
        LOGE("ADCRAW_REG modify fail\n");
        return -EIO;
    }

    return 0;
}

/* ni_force_ic_init
 *
 * Initialize AFE
 */
static int ni_force_ic_init(struct ni_force_data *fdata)
{
    int ret;

    LOGI("%s\n", __func__);

    ret = oem_force_ic_init(fdata);
    if (ret < 0) {
        return ret;
    }

    /* enable AFE */
    if (ni_force_i2c_modify_byte(fdata->client, EN_REG,
                                 1 << EN_POS, EN_MSK) < 0) {
        LOGE("EN_REG modify fail\n");
        return -EIO;
    }

    ni_force_manual_preldadj(fdata);

    /* force baseline */
    if (ni_force_i2c_modify_byte(fdata->client, FORCEBL_REG,
                                 1 << FORCEBL_POS, FORCEBL_MSK) < 0) {
        LOGE("FORCEBL_REG modify fail\n");
        return -EIO;
    }

    return 0;
}

static int ni_force_free(void)
{
    if (--num_clients <= 0) {
    }

    return 0;
}

static int ni_force_probe(struct i2c_client *client,
                          const struct i2c_device_id *id)
{
    u8 data_buffer_out[1];
    struct ni_force_data *fdata;
    struct ni_force_platform_data *pdata;
    int ret = 0;
    int i;

    LOGI("%s\n", __func__);

    if (client->dev.of_node) {
        LOGV("%s: Allocating Memory...\n", __func__);
        pdata = devm_kzalloc(&client->dev,
                             sizeof(struct ni_force_platform_data),
                             GFP_KERNEL);
        if (!pdata) {
            LOGE("%s: Failed to allocate memory\n", __func__);
            dev_err(&client->dev, "Failed to allocate memory\n");
            return -ENOMEM;
        }
        ret = ni_force_parse_dt(&client->dev, pdata);
        if (ret) {
            LOGE("%s: Failed with error %d\n", __func__, ret);
            return ret;
        }
    } else {
        pdata = client->dev.platform_data;
        if (!pdata) {
            LOGV("%s: No platform_data available\n", __func__);
            pdata = devm_kzalloc(&client->dev,
			    sizeof(struct ni_force_platform_data),
			    GFP_KERNEL);
        }
        if (!pdata) {
            LOGE("%s: Failed with error %d\n", __func__, -ENODEV);
            return -ENODEV;
        }
    }

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
        LOGE("I2C functionality check error\n");
        return -EPERM;
    }

	fdata = kzalloc(sizeof(struct ni_force_data), GFP_KERNEL);

	if (!fdata) {
		LOGE("Cannot allocate memory\n");
		ret = -ENOMEM;
		goto err_kzalloc_failed;
	}

	fdata->pdata = pdata;

	fdata->client = client;
	i2c_set_clientdata(client, fdata);

	msleep(BOOTING_DELAY);

	LOGI("%s: Attempting I2C read from device at 0x%x\n", __func__, fdata->client->addr);

	for (i = 0; i < MAX_RETRY_COUNT; i++) {
		if (ni_force_i2c_read(fdata->client, WAIT_REG,
							  sizeof(data_buffer_out), data_buffer_out) < 0) {
			LOGE("I2C read fail\n");
			if (i == MAX_RETRY_COUNT - 1) {
				LOGE("No I2C device\n");
				ret = -EIO;
				goto err_ni_force_i2c_read_failed;
			}
		} else {
			LOGI("%s: I2C read success\n", __func__);
			break;
		}
	}

	ret = ni_force_get_ic_info(fdata);
	if (ret < 0) {
		LOGE("ic_info failed\n");
		goto err_ni_force_i2c_read_failed;
	}

	/* AFE initialization */
	ret = ni_force_ic_init(fdata);
	if (ret < 0) {
		LOGE("ic_init failed\n");
		goto err_ni_force_i2c_read_failed;
	}

	for (i = 0; i < ARRAY_SIZE(ni_force_device_attrs); i++) {
		ret = device_create_file(&client->dev, &ni_force_device_attrs[i]);
		if (ret) {
			LOGE("create_file failed\n");
			goto err_dev_create_file;
		}
	}

	num_clients++;

	LOGI("%s OK (%d)\n", __func__, ret);
    return ret;

err_dev_create_file:
	for (i = i - 1; i >= 0; i--) {
		device_remove_file(&fdata->client->dev, &ni_force_device_attrs[i]);
	}

err_ni_force_i2c_read_failed:
err_kzalloc_failed:
	kfree(fdata);
	ni_force_free();

	LOGI("%s error (%d)\n", __func__, ret);
    return ret;
}

static int ni_force_remove(struct i2c_client *client)
{
    struct ni_force_data *fdata = i2c_get_clientdata(client);
    int i;

    LOGI("%s\n", __func__);

    for (i = 0; i < ARRAY_SIZE(ni_force_device_attrs); i++) {
        device_remove_file(&client->dev, &ni_force_device_attrs[i]);
    }

    kfree(fdata);
    ni_force_free();

    return 0;
}

static const struct of_device_id ni_force_match_table[] = {
    {.compatible = DEVICE_TREE_NAME,},
    {},
};

static struct i2c_device_id ni_force_id[] = {
    {DEVICE_NAME, 0},
    {},
};

static struct i2c_driver ni_force_driver = {
    .probe    = ni_force_probe,
    .remove   = ni_force_remove,
    .id_table = ni_force_id,
    .driver = {
        .name  = DEVICE_NAME,
        .owner = THIS_MODULE,
        .of_match_table = ni_force_match_table,
    },
};

static int __init ni_force_init(void)
{
    LOGI("***NextInput driver __init!\n");
    return i2c_add_driver(&ni_force_driver);
}

static void __exit ni_force_exit(void)
{
    LOGI("***NextInput driver __exit!\n");
    i2c_del_driver(&ni_force_driver);
    ni_force_free();
}

module_init(ni_force_init);
module_exit(ni_force_exit);

MODULE_AUTHOR("NextInput Corporation");
MODULE_DESCRIPTION("NextInput ForceTouch Driver");
MODULE_LICENSE("GPL");
