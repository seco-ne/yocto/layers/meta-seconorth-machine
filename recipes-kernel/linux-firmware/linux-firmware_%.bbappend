FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	file://vpu/vpu_fw_imx6d.bin;subdir=${BPN}-${PV} \
	file://vpu/vpu_fw_imx6q.bin;subdir=${BPN}-${PV} \
	file://vpu/v4l-coda960-imx6dl.bin;subdir=${BPN}-${PV} \
	file://vpu/v4l-coda960-imx6q.bin;subdir=${BPN}-${PV} \
	file://LICENSE.vpu_firmware;subdir=${BPN}-${PV} \
"

PACKAGES =+ "${PN}-imx-vpu-license ${PN}-imx-vpu ${PN}-rtl8822b"

LICENSE =+ "Firmware-imx-vpu_firmware &"
LIC_FILES_CHKSUM =+ "file://LICENSE.vpu_firmware;md5=8cf95184c220e247b9917e7244124c5a"

NO_GENERIC_LICENSE[Firmware-imx-vpu_firmware] = "LICENSE.vpu_firmware"

LICENSE:${PN}-imx-vpu = "Firmware-imx-vpu_firmware"
LICENSE:${PN}-imx-vpu-license = "Firmware-imx-vpu_firmware"

FILES:${PN}-imx-vpu = " \
    ${nonarch_base_libdir}/firmware/vpu_fw_imx6d.bin \
    ${nonarch_base_libdir}/firmware/vpu_fw_imx6q.bin \
    ${nonarch_base_libdir}/firmware/v4l-coda960-imx6dl.bin \
    ${nonarch_base_libdir}/firmware/v4l-coda960-imx6q.bin \
"

RPROVIDES:${PN}-imx-vpu = "firmware-imx-vpu"
RREPLACES:${PN}-imx-vpu = "firmware-imx-vpu"
RCONFLICTS:${PN}-imx-vpu = "firmware-imx-vpu"

FILES:${PN}-imx-vpu-license = "${nonarch_base_libdir}/firmware/LICENSE.vpu_firmware"

RDEPENDS:${PN}-imx-vpu += "${PN}-imx-vpu-license"
RDEPENDS:${PN}-rtl8822b = "${PN}-rtl-license"


LICENSE:${PN} =+ "Firmware-imx-vpu_firmware &"

LICENSE:${PN}-rtl8822b = "Firmware-rtlwifi_firmware"

do_install:prepend () {
    set -x
}

# The vpu fw is only for the mainline kernel
do_install:append:seco-mx6 () {
    install -m 644 vpu/vpu_fw_imx6d.bin ${D}/${nonarch_base_libdir}/firmware
    install -m 644 vpu/vpu_fw_imx6q.bin ${D}/${nonarch_base_libdir}/firmware
    install -m 644 vpu/v4l-coda960-imx6dl.bin ${D}/${nonarch_base_libdir}/firmware
    install -m 644 vpu/v4l-coda960-imx6q.bin ${D}/${nonarch_base_libdir}/firmware
}

FILES:${PN}-rtl8723:append = " \
  ${nonarch_base_libdir}/firmware/rtl_bt/rtl8723*.bin \
"

FILES:${PN}-rtl8822b:append = " \
  ${nonarch_base_libdir}/firmware/rtl_bt/rtl8822b*.bin \
"