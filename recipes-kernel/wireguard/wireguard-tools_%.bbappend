# Remove dependencies to wireguard-module, because wireguard is integrated in kernel versions >= 5.6
DEPENDS:remove = "wireguard-module"
RDEPENDS:${PN}:remove = "wireguard-module"
