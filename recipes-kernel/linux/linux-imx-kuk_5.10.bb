require linux-seconorth.inc

KERNEL_CONFIG_COMMAND = "oe_runmake_call -C ${S} CC="${KERNEL_CC}" O=${B} olddefconfig"

COMPATIBLE_MACHINE = "(seco-mx8mm|seco-mx8mp)"

LINUX_VERSION = "5.10"
LINUX_VERSION_EXTENSION = "-seconorth"

FILESEXTRAPATHS:prepend := "${THISDIR}/linux-seconorth-${LINUX_VERSION}:"

S = "${WORKDIR}/git"

PV = "5.10.9"
SRCREV = "${AUTOREV}"

# branch= combined with nobranch=1 looks weird, but the branch is used for the
# AUTOREV, the nobranch tells the fetcher to not check the SHA validation for the branch
# This way the SRCREV can be specified to a specific revision also outside that branch
# with SRCREV:pn-linux-guf =
SRC_URI = " \
    git://git.seco.com/seco-ne/kernel/linux-imx-kuk.git;protocol=https;branch=linux-${PV}-guf;nobranch=1 \
    file://defconfig \
"

