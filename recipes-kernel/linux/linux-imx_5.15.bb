require linux-seconorth.inc

KERNEL_CONFIG_COMMAND = "oe_runmake_call -C ${S} CC="${KERNEL_CC}" O=${B} olddefconfig"

COMPATIBLE_MACHINE = "(seco-mx8mm|seco-mx8mp|seco-mx6-fsl|seco-mx6ull-fsl)"

LINUX_VERSION = "5.15"
LINUX_VERSION_EXTENSION = "-seco"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${LINUX_VERSION}:"

S = "${WORKDIR}/git"

PV = "5.15.32"
SRCREV = "${AUTOREV}"

# branch= combined with nobranch=1 looks weird, but the branch is used for the
# AUTOREV, the nobranch tells the fetcher to not check the SHA validation for the branch
# This way the SRCREV can be specified to a specific revision also outside that branch
# with SRCREV:pn-linux-guf =
SRC_URI = " \
    git://git.seco.com/seco-ne/kernel/linux-imx-kuk.git;protocol=https;branch=linux-${PV}-seco;nobranch=1 \
    file://defconfig \
"

OVERLAY_COMBINATIONS:seco-mx8mp = " \
    seconorth/seconorth-trizeps8plus_v1r3*.dtb:seconorth/trizeps8plus_*overlay.dtbo \
"
