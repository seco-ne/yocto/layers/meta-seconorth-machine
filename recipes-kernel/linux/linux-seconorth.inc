SUMMARY = "Linux Kernel"
SECTION = "kernel"
HOMEPAGE = "https://north.seco.com/de/"
DESCRIPTION = "Kernel recipes for SECO HMIs like Tanaro, Sant* Trizeps modules..."


LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM ?= "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

inherit kernel kernel-devicetree kernel_devicetree_overlaytest

DEPENDS += "openssl-native util-linux-native lzop-native"

KERNEL_DTC_FLAGS += "-@"

# Remove ${PKGE}- from file names, as PE is always unset,
# it generates the -- in the name
KERNEL_ARTIFACT_NAME = "${PKGV}-${PKGR}-${MACHINE}${IMAGE_VERSION_SUFFIX}"

# We have a FAT boot partition. Creating links for the Kernel leads
# to errors during the installation.
# This link is needed for edgehog builds.
KERNEL_IMAGETYPE_SYMLINK = "${@bb.utils.contains_any('DISTRO', ' \
    seconorth-wayland \
', '0', '1', d)}"

do_deploy:append() {

    # This step comes from kernel-devicetree.bbclass but is disabled as
    # KERNEL_IMAGETYPE_SYMLINK is set to 0
    # but we still need the original devicetree names in the deploy folder
    # to copy them by name
    for dtbf in ${KERNEL_DEVICETREE}; do
        dtb=`normalize_dtb "$dtbf"`
        dtb_ext=${dtb##*.}
        dtb_base_name=`basename $dtb .$dtb_ext`
        ln -sf $dtb_base_name-${KERNEL_DTB_NAME}.$dtb_ext $deployDir/$dtb_base_name.$dtb_ext
    done
}
