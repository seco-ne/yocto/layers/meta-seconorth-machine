SUMMARY = "Platform detection specific to SECO North boards"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

inherit module

SRCREV = "${AUTOREV}"
SRCBRANCH = "dunfell"

SRC_URI = " \
   git://git.seco.com/seco-ne/kernel/modules/gfplatdetect.git;protocol=https;branch=${SRCBRANCH};nobranch=1; \
"

S = "${WORKDIR}/git"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.

RPROVIDES:${PN} += "kernel-module-${PN}"
RRECOMMENDS:${PN} = "kernel-module-${PN}"
KERNEL_MODULE_AUTOLOAD += "${PN}"

