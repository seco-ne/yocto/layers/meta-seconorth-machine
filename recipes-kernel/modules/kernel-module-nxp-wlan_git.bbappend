FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://0001-Makefile-Reduce-debug-level.patch;patchdir=../.. \
            file://moal.conf \
            file://mwifiex.conf \
            "

KERNEL_MODULE_AUTOLOAD  += "mlan moal"
KERNEL_MODULE_PROBECONF += "moal"

do_install:append() {

    install -d ${D}/etc/modprobe.d
    install -m 0644 ${WORKDIR}/moal.conf ${D}/etc/modprobe.d/moal.conf
    install -m 0644 ${WORKDIR}/mwifiex.conf ${D}/etc/modprobe.d/mwifiex.conf
}

FILES:${PN} += " \
        /etc/modprobe.d/moal.conf \
        /etc/modprobe.d/mwifiex.conf\
        "
