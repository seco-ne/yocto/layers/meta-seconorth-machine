SUMMARY = "Kernel module with PCF8563 RTC driver and SECO North specific changes"
HOMEPAGE = "https://www.seco.com"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

inherit module-base
inherit module
inherit kernel-module-split

SRC_URI = " \
    file://Makefile \
    file://rtc-pcf8563-guf.c \
"

S = "${WORKDIR}"

do_configure:prepend () {
    sed  -i "s/SCMVERSION/${PV}/g" ${S}/Makefile
}

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
RPROVIDES:${PN} += "kernel-module-${PN}"
RRECOMMENDS:${PN} = "kernel-module-${PN}"
KERNEL_MODULE_AUTOLOAD += "${PN}"
KERNEL_MODULE_PROBECONF += "${PN}"
module_conf_${PN} = "blacklist rtc-pcf8563"
